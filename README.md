# Config for all integrations

This repo is for developing Employsure's core library SDK.

## Installation in other projects

Install with npm:  
`npm install -S git+https://git@bitbucket.org/employsureappsteam/employsure-core.git#{version_number}`
where `version_number` is in the format `vX.X.X`.

**Note:** Don't forget the URL fragment to specify a version to ensure you don't
accidentally pull breaking changes your project. Refer to
[package.json](./package.json) for latest version.

---

## Development

### Publishing an update

After making changes to code:

-   Compile source for distribution (`npm run build`)
-   Run tests (`npm run test`)
-   Stage files (`git add [your files]`)
-   Commit changes (`git commit -m "[commit message]"`)
-   Bump package version (`npm version [ major | minor | patch ]`)
-   Push to git (`git push`)

To update `employsure-core` in projects using this library, manually change the version number in `package.json` and run `npm upgrade employsure-core`.

---

## Examples

### loadSalesforceClient

```Typescript
const sf = await loadSalesforceClient('aus', 'Full');
const rows = await sf.query2(`SELECT id, name FROM Account LIMIT 1`);
```

### GetSecretParams

```Typescript
const secrets = await GetSecretParams(['/Service/Dev/ApiKey']);
```

### getDynamoTableRow

```Typescript
const row = await getDynamoTableRow('name_of_table', 'primary_key', 'value_to_query');
```

For example, this DynamoDB is called `Fruits`.

| Fruit  | Colour | Tastiness |
| ------ | ------ | --------: |
| Banana | yellow |       8.5 |
| Apple  | red    |         7 |

To get the Apple Row, you would run `getDynamoTableRow('Fruits', 'Fruit', 'Apple')`

### SNS Notifications

See [Notifications](https://employsure.atlassian.net/wiki/spaces/EN/pages/289079405/Notifications) in confluence for details.

#### Send notification to SNS Topic

```Typescript
const topicArn = process.env.topicArn;
const response = await sendToSns(
    topicArn,
    "Subject",
    [
        { title: "Log", message: "Message 1" },
        { title: "Failed", message: "Message 2", details: "some details" }
    ]
)
```
