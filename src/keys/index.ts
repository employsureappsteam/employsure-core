import { OAuthTokenKey, SigningSecretKey } from "../models";

/* Slack. */ 
export const SlackOAuthTokens: OAuthTokenKey = {
    Provisioning: "/Slack/Provisioning/OAuthToken",
    AWSNotificationService: "/Slack/AWSNotification/OAuthToken",
    Development: "/Slack/Development/OAuthToken",
};

export const SlackSigningSecrets: SigningSecretKey = {
    Provisioning: "/Slack/Provisioning/SigningSecret",
    AWSNotificationService: "/Slack/AWSNotification/SigningSecret",
    Development: "/Slack/Development/SigningSecret",
};