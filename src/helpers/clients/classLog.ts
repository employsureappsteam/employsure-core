import { IClientOptions, VerboseLoggingLevel } from "./models";

const SEPERATOR = "::";

const ENV_CLIENT_OPTIONS: IClientOptions = {
    verboseLogging: process.env.EMPLOYSURE_VERBOSE_LOGGING === 'true',
    verboseLoggingLevel: process.env.EMPLOYSURE_VERBOSE_LOGGING_LEVEL as VerboseLoggingLevel
}

/**
 * Helper function for logging from within a class. 
 * 
 * Intended to help debugging state of functions. 
 */
export const classLog = (
    className: string,
    method: string,
    message: string,
    logLevel: VerboseLoggingLevel = 'info',
    userOptions?: IClientOptions
) => {
    /* Overwrite user options with server options. */
    const options = {
        ...userOptions,
        ...ENV_CLIENT_OPTIONS,
    }
    if (options.verboseLogging || logLevel === 'error') {
        const msg = `${className}${SEPERATOR}${method}${SEPERATOR}${message}`;
        if (logLevel === 'info' && shouldLogInfo(options)) {
            console.info(msg);
        } else if (logLevel === 'warn' && shouldLogWarn(options)) {
            console.warn(msg);
        } else if (logLevel === 'error' && shouldLogError(options)) {
            console.error(msg);
        }
    }
}

/* True if the user requested info logs. */
const shouldLogInfo = (userConfig?: IClientOptions) => {
    return userConfig?.verboseLoggingLevel === 'info';
}

/* True if the user requested info logs or warning. */
const shouldLogWarn = (userConfig?: IClientOptions) => {
    return userConfig?.verboseLoggingLevel === 'info'
        || userConfig?.verboseLoggingLevel === 'warn';
}

/* True if the user requested info logs. */
const shouldLogError = (userConfig?: IClientOptions) => {
    /* Always log errors. */
    return true;
    // return userConfig?.verboseLoggingLevel === 'info'
    //     || userConfig?.verboseLoggingLevel === 'warn'
    //     || userConfig?.verboseLoggingLevel === 'error';
}

