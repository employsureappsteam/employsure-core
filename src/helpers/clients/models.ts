import { BigQueryClient, SalesforceClient } from "../../clients";

/* The path of the SSM string. */
type EmploysureSsmPath = string;

export interface IClientCreds {
    salesforce?: ISalesforceCreds,
    bigquery?: IBigQueryClientSsmCreds,
    azure?: IAzureClientSsmCreds
}

export interface IEmploysureClients {
    salesforce?: SalesforceClient,
    bigquery?: BigQueryClient
}

export interface IAzureClientSsmCreds {
    tenantId: EmploysureSsmPath,
    clientId: EmploysureSsmPath,
    clientSecret: EmploysureSsmPath
}
export interface IBigQueryClientSsmCreds {
    clientEmail: EmploysureSsmPath,
    privateKey: EmploysureSsmPath,
    project: string
}

export interface ISalesforceCreds {
    username: EmploysureSsmPath,
    password: EmploysureSsmPath,
    clientId: EmploysureSsmPath,
    clientSecret: EmploysureSsmPath,
    environment?: 'production' | 'sandbox'
}

export function IsSalesforceOrgLabel(e: any): e is ISalesforceOrgLabel {
    return typeof e === "object"
        && typeof e.region === "string"
        && typeof e.stage === "string"
        && (e.user === undefined || typeof e.region === "string");
}
export type SalesforceRegion = 'aus' | 'nz' | 'brighthr';
export type SalesforceStage = 'Prod' | 'Full' | 'Mini' | string;
export type SalesforceUser = 'engineering-service';
export type SalesforceApp = 'employsure-api' | 'engineering-integration';
export interface ISalesforceOrgLabel {
    /* Required. Must be either aus or nz. */
    region: SalesforceRegion;
    /* Required. Can be any 'stage' that is referenced in SSM. */
    stage: SalesforceStage;
    /* Defaults to Engineering Service. */
    user?: SalesforceUser;
    /* Defaults to Employsure API. */
    app?: SalesforceApp;
}

/* info = everything. warn = warn and error. error = only error. */
export type VerboseLoggingLevel = 'info' | 'warn' | 'error';
export interface IClientOptions {
    /** Enables logging of client interals. */
    verboseLogging?: boolean;
    verboseLoggingLevel?: VerboseLoggingLevel;

    /** The client will use SSM to cache the login token as there is a Salesforce
     * API limit on how many times a particular app/user can request a new token.
     * To disable this behaviour set disableDistributedToken to true.
     * Default: false
     **/
    disableDistributedAuth?: boolean
}

export class LoadClientError extends Error {
    name = "LoadClientError";
    constructor(message: string) {
        super(message);
    }
}