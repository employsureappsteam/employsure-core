import { BigQueryClient, SalesforceClient } from "../../clients";
import { GetSecretParams } from "../../lib";
import { EmploysureError } from "../../models/errors";
import { IBigQueryClientSsmCreds, IClientCreds, IClientOptions, IEmploysureClients, ISalesforceCreds, ISalesforceOrgLabel, IsSalesforceOrgLabel, LoadClientError, SalesforceApp, SalesforceRegion } from "./models";

/**
 * 
 * @param creds SSM locations/other config for clients
 */
export async function loadClients(creds: IClientCreds, options?: IClientOptions): Promise<IEmploysureClients> {
    /* Fetch necessary secrets. */
    const ssmParams: Array<string> = [];

    if (creds.salesforce) {
        const sf = creds.salesforce;
        ssmParams.push(sf.clientId);
        ssmParams.push(sf.clientSecret);
        ssmParams.push(sf.username);
        ssmParams.push(sf.password);
    }

    if (creds.bigquery) {
        const bq = creds.bigquery;
        ssmParams.push(bq.clientEmail);
        ssmParams.push(bq.privateKey);
    }

    const secrets = await GetSecretParams(ssmParams);

    /* Prepare clients. */
    const clients: IEmploysureClients = {};

    if (creds.salesforce) {
        const sf = creds.salesforce;
        const salesforce = new SalesforceClient({
            clientId: secrets[sf.clientId],
            clientSecret: secrets[sf.clientSecret],
            username: secrets[sf.username],
            password: secrets[sf.password],
            environment: sf.environment,
            ...options
        });
        clients.salesforce = salesforce;
    }

    if (creds.bigquery) {
        const bq = creds.bigquery;
        const bigQueryClient = new BigQueryClient(
            creds.bigquery.project,
            {
                client_email: secrets[bq.clientEmail],
                private_key: secrets[bq.privateKey]
            },
            options
        );
        clients.bigquery = bigQueryClient
    }

    return clients;
}

/**
 * Loads bigquery client. Uses default creds for a project if given string.
 * 
 * @param creds 
 */
export const loadBigQueryClient = async (creds: string | IBigQueryClientSsmCreds, options?: IClientOptions) => {
    if (typeof creds === "string") {
        if (creds === 'lead-scoring-model') {
            creds = {
                clientEmail: '/BigQuery/Prod/ClientEmail',
                privateKey: '/BigQuery/Prod/PrivateKeyWithoutNewline',
                project: 'lead-scoring-model'
            }
        } else {
            throw new LoadClientError(`No BigQuery client config for ${creds}`);
        }
    }

    const { bigquery } = await loadClients({ bigquery: creds }, options);
    return bigquery as BigQueryClient;
}

/**
 * Loads Salesforce client using simple labels.
 */
export const loadSalesforceClient = async (region: SalesforceRegion, stage: string, clientOptions?: IClientOptions) => {
    /* Default app for regions. */
    let app: SalesforceApp = 'employsure-api';
    if (region === 'brighthr') {
        app = 'engineering-integration'
    }

    return loadSalesforceClientFromLabel(
        {
            region,
            stage,
            user: 'engineering-service',
            app
        },
        clientOptions
    );
}

/* Load Salesforce client using labels with options for which app and user to use. */
export const loadSalesforceClientFromLabel = async (labels: ISalesforceOrgLabel, clientOptions?: IClientOptions) => {
    const creds = evaluateSalesforceCredDefaults(labels);
    try {
        const { salesforce } = await loadClients(
            { salesforce: creds },
            clientOptions
        );
        return salesforce as SalesforceClient;
    } catch (error) {
        if (error.name === "InvalidParameterError") {
            console.error(error.message);
            throw new EmploysureError('InvalidSalesforceSsmCredentials', `Failed to load salesforce client from labels ${JSON.stringify(labels)} as the SSM references are invalid. See logs for details.`);
        } else {
            throw error;
        }
    }
}




/**
 * Using a set of business domain labels create a credential interface for 
 * the Salesforce client to use. 
 * 
 * @param label Set of business domain labels
 * @returns 
 */
function evaluateSalesforceCredDefaults(label: ISalesforceOrgLabel): ISalesforceCreds {
    /* Setup stage. */
    let ssmStage: string = label.stage;
    let environment: 'production' | 'sandbox';
    if (label.stage === 'Prod') {
        environment = 'production';
    } else {
        environment = 'sandbox';
    }

    /* Setup region. */
    let ssmOrg: string;
    if (label.region === 'aus') {
        ssmOrg = 'Salesforce'
    } else if (label.region === 'nz') {
        ssmOrg = 'SalesforceNZ'
    } else if (label.region === 'brighthr') {
        ssmOrg = 'SalesforceBright'
    } else {
        throw new LoadClientError(`No default Salesforce region for ${label.region}`);
    }

    /* Setup Org user */
    let ssmOrgUser: string;
    if (label.user === 'engineering-service') {
        ssmOrgUser = 'EngineeringService';
    } else {
        throw new LoadClientError(`No default Salesforce user for ${label.user}`);
    }

    /* Setup Org app */
    let ssmOrgApp: string;
    if (label.app === 'employsure-api') {
        ssmOrgApp = 'EmploysureApiApp';
    } else if (label.app === 'engineering-integration') {
        ssmOrgApp = 'EngineeringIntegrationApp';
    } else {
        throw new LoadClientError(`No default Salesforce app for ${label.app}`);
    }

    /* Build credentials. */
    return {
        username: `/${ssmOrg}${ssmOrgUser}/${ssmStage}/Username`,
        password: `/${ssmOrg}${ssmOrgUser}/${ssmStage}/Password`,
        clientId: `/${ssmOrg}${ssmOrgApp}/${ssmStage}/Id`,
        clientSecret: `/${ssmOrg}${ssmOrgApp}/${ssmStage}/Secret`,
        environment
    }
}
