import _ from "lodash";

export function dedupeArrayByKey<T>(array: Array<T>, key: string): Array<T> {
    const dedupedArray: Array<T> = []
    for (const c of array) {
        const dups = dedupedArray.filter(d => c[key] === d[key]);
        if (dups.length === 0) {
            dedupedArray.push(c);
        }
    }
    return dedupedArray;
}

export function chunkArray<T = any>(_arr: T[], chunkSize: number) {
    const recordChunks: T[][] = []

    for (let i = 0; i < _arr.length; i += chunkSize) {
        recordChunks.push(_arr.slice(i, i + chunkSize));
    }

    return recordChunks;
}

export const mapValuesDeep = (v, callback) => {
    if (_.isObject(v) && !_.isArray(v)) {
        return _.mapValues(v, v => mapValuesDeep(v, callback))
    } else if (_.isArray(v)) {
        return _.map(v, v => mapValuesDeep(v, callback))
    } else {
        return callback(v);
    }
}