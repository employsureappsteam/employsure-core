import { isJsonObject } from "./isJsonObject";

export const objectHasValuesOf = <T>(obj: { [x: string]: T }, tg: (e: T) => e is T): obj is { [x: string]: T } => {
    return isJsonObject(obj)
        && Object.values(obj).map(tg).reduce(areAllTrue, true);

}

const areAllTrue = (p: boolean, c: boolean) => p && c;