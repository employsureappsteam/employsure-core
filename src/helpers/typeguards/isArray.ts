export const isArray = (e: Array<any>): e is Array<any> => {
    return Array.isArray(e);
}