export const isArrayOf = <T>(element: Array<T>, elementTypeGuard: (element: T) => element is T): element is Array<T> => {
    return (
        Array.isArray(element) &&
        element.map(r => elementTypeGuard(r))
            .reduce((p, c) => p && c, true)
    );
};