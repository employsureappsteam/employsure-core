/* Checks if Json object, as opposed to any javascript object (e.g. null) */
export const isJsonObject = (e: {}): e is {} => {
    return e !== undefined && typeof e === "object";
}