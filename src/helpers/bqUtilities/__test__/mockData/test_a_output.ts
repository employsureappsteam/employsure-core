export default [
    {
        "name": "_odata_etag",
        "type": "STRING"
    },
    {
        "name": "createdDateTime",
        "type": "TIMESTAMP"
    },
    {
        "name": "eTag",
        "type": "STRING"
    },
    {
        "name": "id",
        "type": "STRING"
    },
    {
        "name": "lastModifiedDateTime",
        "type": "TIMESTAMP"
    },
    {
        "name": "webUrl",
        "type": "STRING"
    },
    {
        "name": "createdBy",
        "type": "RECORD",
        "fields": [
            {
                "name": "user",
                "type": "RECORD",
                "fields": [
                    {
                        "name": "email",
                        "type": "STRING"
                    },
                    {
                        "name": "id",
                        "type": "STRING"
                    },
                    {
                        "name": "displayName",
                        "type": "STRING"
                    }
                ]
            }
        ]
    },
    {
        "name": "lastModifiedBy",
        "type": "RECORD",
        "fields": [
            {
                "name": "user",
                "type": "RECORD",
                "fields": [
                    {
                        "name": "email",
                        "type": "STRING"
                    },
                    {
                        "name": "id",
                        "type": "STRING"
                    },
                    {
                        "name": "displayName",
                        "type": "STRING"
                    }
                ]
            }
        ]
    },
    {
        "name": "parentReference",
        "type": "RECORD",
        "fields": [
            {
                "name": "siteId",
                "type": "STRING"
            }
        ]
    },
    {
        "name": "contentType",
        "type": "RECORD",
        "fields": [
            {
                "name": "id",
                "type": "STRING"
            }
        ]
    },
    {
        "name": "fields_odata_context",
        "type": "STRING"
    },
    {
        "name": "fields",
        "type": "RECORD",
        "fields": [
            {
                "name": "_odata_etag",
                "type": "STRING"
            },
            {
                "name": "Title",
                "type": "STRING"
            },
            {
                "name": "ServiceLine",
                "type": "STRING"
            },
            {
                "name": "Role",
                "type": "STRING"
            },
            {
                "name": "Location",
                "type": "STRING"
            },
            {
                "name": "Typeofmovement",
                "type": "STRING"
            },
            {
                "name": "DateofMovement",
                "type": "TIMESTAMP"
            },
            {
                "name": "FTE",
                "type": "FLOAT"
            },
            {
                "name": "Comment",
                "type": "STRING"
            },
            {
                "name": "id",
                "type": "STRING"
            },
            {
                "name": "ContentType",
                "type": "STRING"
            },
            {
                "name": "Modified",
                "type": "TIMESTAMP"
            },
            {
                "name": "Created",
                "type": "TIMESTAMP"
            },
            {
                "name": "AuthorLookupId",
                "type": "STRING"
            },
            {
                "name": "EditorLookupId",
                "type": "STRING"
            },
            {
                "name": "_UIVersionString",
                "type": "STRING"
            },
            {
                "name": "Attachments",
                "type": "BOOLEAN"
            },
            {
                "name": "Edit",
                "type": "STRING"
            },
            {
                "name": "LinkTitleNoMenu",
                "type": "STRING"
            },
            {
                "name": "LinkTitle",
                "type": "STRING"
            },
            {
                "name": "ItemChildCount",
                "type": "STRING"
            },
            {
                "name": "FolderChildCount",
                "type": "STRING"
            },
            {
                "name": "_ComplianceFlags",
                "type": "STRING"
            },
            {
                "name": "_ComplianceTag",
                "type": "STRING"
            },
            {
                "name": "_ComplianceTagWrittenTime",
                "type": "STRING"
            },
            {
                "name": "_ComplianceTagUserId",
                "type": "STRING"
            }
        ]
    }
]