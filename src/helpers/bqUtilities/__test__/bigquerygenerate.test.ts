import chai, { expect } from "chai";
import { IBigQuerySchema } from "../../../clients";
import generate from '../bigquerySchemaGenerator';
import * as test_b_input from './mockData/test_b_input';
import * as test_b_output from './mockData/test_b_output';
import { bqSchemaUnion } from "../bqQueryUnion";

/* Chai extensions */
import deepEqualInAnyOrder from 'deep-equal-in-any-order';
chai.use(deepEqualInAnyOrder);

describe('generate bigquery schema from JSON', () => {
    it('String type', () => {
        const original = {
            test: 'hello'
        }
        const expected: Array<IBigQuerySchema> = [{
            mode: "NULLABLE",
            name: 'test',
            type: 'STRING'
        }];
        const generated = generate(original);
        expect(generated).deep.equal(expected);
    });

    it('Null type should be skipped', () => {
        const original = {
            id: 'deadbeef',
            test: null
        }
        const expected: Array<IBigQuerySchema> = [{
            mode: "NULLABLE",
            name: 'id',
            type: 'STRING'
        }];
        const generated = generate(original);
        expect(generated).deep.equal(expected);
    });
});

describe('bqSchemaUnion | Testing flat schemas (without RECORD type)', () => {
    it('Union of 2x flat schema', () => {
        const inputSchemas: Array<Array<IBigQuerySchema>> = [
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'second_field',
                    type: "FLOAT"
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'third_field',
                    type: "BOOLEAN"
                }
            ],
        ];

        const expectedSchema: Array<IBigQuerySchema> = [
            {
                name: 'id',
                type: "STRING"
            },
            {
                name: 'second_field',
                type: "FLOAT"
            },
            {
                name: 'third_field',
                type: "BOOLEAN"
            }
        ];

        const finalSchema = bqSchemaUnion(inputSchemas);
        expect(finalSchema).to.deep.equals(expectedSchema);
    });

    it('Union of 2x flat schema with conflicting types should throw error', () => {
        const inputSchemas: Array<Array<IBigQuerySchema>> = [
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'second_field',
                    type: "FLOAT"
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'second_field',
                    type: "BOOLEAN"
                }
            ],
        ];

        const finalSchemaFn = () => bqSchemaUnion(inputSchemas);
        expect(finalSchemaFn).to.throw();
    });

    it('Union of 3x flat schema', () => {
        const inputSchemas: Array<Array<IBigQuerySchema>> = [
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'second_field',
                    type: "FLOAT"
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'third_field',
                    type: "BOOLEAN"
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'fourth_field',
                    type: "DATE"
                }
            ],
        ];

        const expectedSchema: Array<IBigQuerySchema> = [
            {
                name: 'id',
                type: "STRING"
            },
            {
                name: 'second_field',
                type: "FLOAT"
            },
            {
                name: 'third_field',
                type: "BOOLEAN"
            },
            {
                name: 'fourth_field',
                type: "DATE"
            }
        ];

        const finalSchema = bqSchemaUnion(inputSchemas);
        expect(finalSchema).to.deep.equals(expectedSchema);
    });
});

describe('bqSchemaUnion | Testing nested records (with RECORD type)', () => {
    it('Union of 2x nested schemas', () => {
        const inputSchemas: Array<Array<IBigQuerySchema>> = [
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'a_record_type',
                    type: "RECORD",
                    fields: [
                        {
                            name: 'first_child',
                            type: 'STRING'
                        }
                    ]
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'misc',
                    type: "STRING"
                },
                {
                    name: 'a_record_type',
                    type: "RECORD",
                    fields: [
                        {
                            name: 'second_child',
                            type: 'BOOLEAN'
                        }
                    ]
                }
            ]
        ];

        const expectedSchema: Array<IBigQuerySchema> = [
            {
                name: 'id',
                type: "STRING"
            },
            {
                name: 'misc',
                type: "STRING"
            },
            {
                name: 'a_record_type',
                type: "RECORD",
                fields: [
                    {
                        name: 'first_child',
                        type: 'STRING'
                    },
                    {
                        name: 'second_child',
                        type: 'BOOLEAN'
                    }
                ]
            }
        ];

        const finalSchema = bqSchemaUnion(inputSchemas);
        // @ts-ignore
        expect(finalSchema).to.deep.equalInAnyOrder(expectedSchema);
    });

    it('Type conflicts within nested schemas should throw an error', () => {
        const inputSchemas: Array<Array<IBigQuerySchema>> = [
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'a_record_type',
                    type: "RECORD",
                    fields: [
                        {
                            name: 'first_child',
                            type: 'STRING'
                        }
                    ]
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'misc',
                    type: "STRING"
                },
                {
                    name: 'a_record_type',
                    type: "RECORD",
                    fields: [
                        {
                            name: 'first_child',
                            type: 'BOOLEAN'
                        },
                        {
                            name: 'second_child',
                            type: 'BOOLEAN'
                        }
                    ]
                }
            ]
        ];

        const finalSchemaFn = () => bqSchemaUnion(inputSchemas);
        expect(finalSchemaFn).to.throw();
    });

    it('Union of 2x double nested schema', () => {
        const inputSchemas: Array<Array<IBigQuerySchema>> = [
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'a_record_type',
                    type: "RECORD",
                    fields: [
                        {
                            name: 'first_child',
                            type: 'STRING'
                        },
                        {
                            name: 'deep_nested',
                            type: 'RECORD',
                            fields: [{
                                name: 'first_deep_child',
                                type: 'STRING'
                            }]
                        }
                    ]
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'misc',
                    type: "STRING"
                },
                {
                    name: 'a_record_type',
                    type: "RECORD",
                    fields: [
                        {
                            name: 'second_child',
                            type: 'BOOLEAN'
                        },
                        {
                            name: 'deep_nested',
                            type: 'RECORD',
                            fields: [
                                {
                                    name: 'first_deep_child',
                                    type: 'STRING'
                                },
                                {
                                    name: 'second_deep_child',
                                    type: 'TIMESTAMP'
                                },
                            ]
                        }
                    ]
                }
            ]
        ];

        const expectedSchema: Array<IBigQuerySchema> = [
            {
                name: 'id',
                type: "STRING"
            },
            {
                name: 'misc',
                type: "STRING"
            },
            {
                name: 'a_record_type',
                type: "RECORD",
                fields: [
                    {
                        name: 'first_child',
                        type: 'STRING'
                    },
                    {
                        name: 'second_child',
                        type: 'BOOLEAN'
                    },
                    {
                        name: 'deep_nested',
                        type: 'RECORD',
                        fields: [
                            {
                                name: 'first_deep_child',
                                type: 'STRING'
                            },
                            {
                                name: 'second_deep_child',
                                type: 'TIMESTAMP'
                            },
                        ]
                    }
                ]
            }
        ];

        const finalSchema = bqSchemaUnion(inputSchemas);
        // @ts-ignore
        expect(finalSchema).to.deep.equalInAnyOrder(expectedSchema);
    });
});

describe('bqSchemaUnion | Options: type coercion', () => {
    it('Conflicting type TIMESTAMP to STRING should be allowed when type coercion is enabled ', () => {
        const inputSchemas: Array<Array<IBigQuerySchema>> = [
            [
                {
                    name: 'test',
                    type: "STRING"
                },
            ],
            [
                {
                    name: 'test',
                    type: "TIMESTAMP"
                }
            ],
        ];

        const expectedSchema: Array<IBigQuerySchema> = [
            {
                name: 'test',
                type: "STRING"
            }
        ];

        const finalSchema = bqSchemaUnion(inputSchemas);
        expect(finalSchema).to.deep.equals(expectedSchema);
    });

    it('Conflicting type TIMESTAMP to STRING should throw error when type coercion is disabled ', () => {
        const inputSchemas: Array<Array<IBigQuerySchema>> = [
            [
                {
                    name: 'test',
                    type: "STRING"
                },
            ],
            [
                {
                    name: 'test',
                    type: "TIMESTAMP"
                }
            ],
        ];

        const finalSchemaFn = () => bqSchemaUnion(inputSchemas, { typeCoercionEnabled: false });
        expect(finalSchemaFn).to.throw();
    });
});

describe('bqSchemaUnion | Production examples', () => {
    it('example - UserInformationListSchema', () => {
        const inputSchemas = test_b_input.data;
        const expectedSchema = test_b_output.data;
        const finalSchema = bqSchemaUnion(inputSchemas);
        // @ts-ignore
        expect(finalSchema).to.deep.equalInAnyOrder(expectedSchema);
    });
})