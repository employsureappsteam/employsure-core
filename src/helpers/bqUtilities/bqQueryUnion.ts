
import _ from 'lodash';
import { IBigQuerySchema } from '../../clients';

export interface IBqSchemaUnionOptions {
    typeCoercionEnabled?: boolean
}

const defaultOptions: IBqSchemaUnionOptions = {
    typeCoercionEnabled: true
}

/* To mash two schemas together. Throw error on difference (i.e. different types) */
export const bqSchemaUnion = (
    schemas: Array<Array<IBigQuerySchema>>,
    options: IBqSchemaUnionOptions = defaultOptions): Array<IBigQuerySchema> => {
    const finalFields: Array<IBigQuerySchema> = []
    for (const fields of schemas) {
        for (const field of fields) {
            /* Find field if it exists in final schema already. */
            const existingField = _.find(finalFields, { name: field.name });

            if (existingField && existingField?.type === field.type) {
                if (field.type === "RECORD") {
                    if (existingField.fields && field.fields) {
                        /* If existing field and is of RECORD type, do a union on the children fields */
                        existingField.fields = bqSchemaUnion([existingField.fields, field.fields])
                    } else {
                        /* Catch errors if it's missing fields */
                        throw new Error(`${field.name} is "RECORD" type but missing child fields`);
                    }
                } else {
                    /* If existing field, isn't record, and same type, skip*/
                    continue;
                }
            } else if (existingField && existingField?.type !== field.type) {
                if (options.typeCoercionEnabled) {
                    if (existingField.type === "STRING" && field.type === "TIMESTAMP") {
                        continue;
                    }
                }
                /* If existing field and same type, there is a conflict. Throw error */
                throw new Error(`${field.name} exists twice in final schema with conflicting types (${existingField.type}, ${field.type})`)
            } else {
                /* Field does not exist, add to final fields */
                finalFields.push(field);
            }
        }
    }
    return finalFields;
}