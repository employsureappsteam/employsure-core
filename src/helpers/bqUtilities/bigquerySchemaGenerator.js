"use strict";

module.exports = function generate(data) {
    return traverse([], data);
};

function getMode(val) {
    if (Array.isArray(val)) return "REPEATED";
    else return "NULLABLE";
}

function isHexString(val) {
    return typeof val === "string" && val.startsWith("0x");
}

function isString(val) {
    return typeof val === "string";
}

function getType(val) {
    if (typeof val === "boolean") return "BOOLEAN";
    /* Strings starting with 0x (hex) are considered a number. */
    if (!isNaN(val) && !isString(val)) {
        return "FLOAT";
    }
    if (val instanceof Date) return "TIMESTAMP";
    if (Array.isArray(val)) return getType(val[0]);
    if (typeof val === "object") return "RECORD";

    if (typeof val === "string") {
        // TODO: make fork into original repo to fix bug: https://github.com/nw/bigquery-schema-generator/blob/master/index.js
        if (val.match(/^\d{4}-\d{2}-\d{2}$/)) return "DATE";
        if (val.length > 18 && !isNaN(new Date(val).getTime()))
            return "TIMESTAMP";
    }

    return "STRING";
}

function traverse(arr, data) {
    return Object.keys(data)
    .filter(key => data[key] !== null)
    .map(key => {
        let val = data[key];
        let meta = {
            name: key,
            type: getType(data[key]),
            mode: getMode(data[key]),
        };

        if (meta.type === "RECORD") {
            meta.fields = traverse([], meta.mode === "REPEATED" ? val[0] : val);
        }

        return meta;
    });
}
