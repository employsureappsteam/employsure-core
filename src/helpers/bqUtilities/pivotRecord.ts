import _ from 'lodash';

export interface IPivotOptions {
    srcColsRegexPattern: string,
    destColField: string,
    destColValue: string,
    /* If set, the data will be pivoted from the key instead of from the root object. */
    pivotDataOnKey?: string,
}

// TODO: further testing around these functions. Involves deep copying hacks and not sure of edge cases that might handle data unepxectedly

export const pivotRecords = <T = {}, R = {}>(records: Array<T>, opt: IPivotOptions): Array<R> => {
    return records
        .map(d => pivotRecord<T, R>(d, opt))
        .reduce((acc, rec) => [...acc, ...rec], []);
}

export const pivotRecord = <T = {}, R = {}>(record: T, opt: IPivotOptions): Array<R> => {
    const regex = new RegExp(opt.srcColsRegexPattern);

    const dataToPivot: Array<{ key: string, value: any }> = [];

    const pivotRoot = opt.pivotDataOnKey ? record[opt.pivotDataOnKey] : record;

    /* Collect all the changes*/
    for (const [key, value] of Object.entries(pivotRoot)) {
        if (key.match(regex)) {
            dataToPivot.push({ key, value })
            delete pivotRoot[key];
        }
    }

    /* Loop over the data that was pivoted and create a new record for each. */
    const newRecords = dataToPivot.map(data => {
        const obj = _.cloneDeep(record);
        const pivotRoot = opt.pivotDataOnKey ? obj[opt.pivotDataOnKey] : obj;
        pivotRoot[opt.destColField] = data.key;
        pivotRoot[opt.destColValue] = data.value;

        return obj as unknown as R;
    });

    /* If there was nothing to be pivoted just return the original record. */
    if (newRecords.length === 0) {
        return [record as unknown as R];
    } else {
        return newRecords;
    }
}