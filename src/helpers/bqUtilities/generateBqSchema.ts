import { IBigQuerySchema } from '../../clients';
import schemaGenerate from './bigquerySchemaGenerator';
import { bqSchemaUnion } from "./bqQueryUnion";

export function generateBqSchema(data: Array<{}>, schemaOverride: Array<IBigQuerySchema> = []) {
    /* Generate schemas for every record and union them */
    const schemasGenerated = data.map(d => schemaGenerate(d) as Array<IBigQuerySchema>);
    const schema = bqSchemaUnion([schemaOverride, ...schemasGenerated]);

    return schema;
}