import { IBigQuerySchema } from "../../clients";


export const addSchema = (curr: Array<IBigQuerySchema>, newSchema: IBigQuerySchema) => {
    for (const schema of curr) {
        if (schema.name === newSchema.name) {
            if (schema.type === newSchema.type) {
                return;
            } else {
                throw new Error(`Duplicate schema ${JSON.stringify(newSchema)}`);
            }
        }
    }
    curr.push(newSchema);
    return;
}