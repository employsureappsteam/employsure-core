import { DynamoDB } from "aws-sdk";
import { AWS_REGION_DEFAULT } from "../constants";

const dynamodb = new DynamoDB({
    region: AWS_REGION_DEFAULT
});


export async function getDynamoTableRow(TableName: string, PrimaryKey: string, IndexValue: string) {
    const params: DynamoDB.QueryInput = {
        TableName,
        KeyConditionExpression: `#primaryKey = :indexValue`,
        ExpressionAttributeValues: {
            ":indexValue": {
                S: IndexValue
            }
        },
        ExpressionAttributeNames: {
            "#primaryKey": PrimaryKey
        },
    }

    const table = await dynamodb.query(params).promise()

    if (table.Items && table.Items.length > 0) {
        return table.Items[0];
    } else {
        return null;
    }
}