import { Options } from './models';

export interface IEmpLoggerOptions {
    /* Includes timestamp in log. */
    date?: boolean,
    /* Disables printing to output, but retains log internally. */
    disablePrinting?: boolean,
    /* Prefix the message with this. */
    prefix?: string
}

/**
 * Little logging class to help me out. 
 */
export class EmpLogger {
    private history: string[] = [];
    private options: IEmpLoggerOptions = {
        date: true,
        disablePrinting: false
    }

    constructor(options?: IEmpLoggerOptions) {
        /* Overwrite obj defaults with provided options. */
        this.options = { ... this.options, ...options };
    }

    log(s: string, options?: IEmpLoggerOptions) {
        const dateStr = (new Date).toISOString();

        /* Function options overwrite global. */
        options = { ... this.options, ...options };

        let _s = s;
        if (options.prefix !== undefined) {
            _s = `${options.prefix}${_s}`;
        }

        if (options.date ?? true) {
            _s = `[${dateStr}] ${_s}`;
        }

        if (!options.disablePrinting) {
            console.log(_s);
        }

        this.history.push(_s);
    }

    getLogHistory() {
        return this.history;
    }

    clearLogHistory() {
        this.history = [];
    }
}
