import chai, { expect } from "chai";
import { EmpLogger } from "..";

describe('Emplogger', () => {
    it('Log history should keep log entries', () => {
        const log = new EmpLogger({ disablePrinting: true });

        log.log('hello')

        const hist = log.getLogHistory();
        expect(hist).to.have.length(1);
    });

    it('false date option should disable timestamp', () => {
        const log = new EmpLogger({ disablePrinting: true, date: false });

        log.log('hello')

        const hist = log.getLogHistory();
        expect(hist[0]).to.equal("hello");
    });

    it('Should display prefix', () => {
        const log = new EmpLogger({ disablePrinting: true, date: false, prefix: 'test: ' });

        log.log('hello')

        const hist = log.getLogHistory();
        expect(hist[0]).to.equal("test: hello");
    });

    it('Should display prefix and timestamp', () => {
        const log = new EmpLogger({ disablePrinting: true, prefix: 'test: ' });

        log.log('hello')

        const hist = log.getLogHistory();
        expect(hist[0]).to.match(new RegExp(/\[\S+\] test: hello/));
    });

    it('Function option should overwrite global', () => {
        const log = new EmpLogger({ disablePrinting: true, prefix: 'test: ' });

        log.log('hello', { prefix: 'world-' })

        const hist = log.getLogHistory();
        expect(hist[0]).to.match(new RegExp(/\[\S+\] world-hello/));
    });
});