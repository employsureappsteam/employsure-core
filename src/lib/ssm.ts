import { SSM } from "aws-sdk";
import { AWS_REGION_DEFAULT, AWS_SSM_API_VERSION } from "../constants";
import { classLog } from "../helpers/clients/classLog";
import { InvalidParameterError, ParametersMap } from "../models";

const MAX_RETRIES = 7;
const ssm = new SSM({
    region: AWS_REGION_DEFAULT,
    apiVersion: AWS_SSM_API_VERSION,
    maxRetries: MAX_RETRIES,
    retryDelayOptions: {
        base: 250,
        customBackoff: (count, error) => {
            const backOff = 120 * (2 ** count) + Math.floor(Math.random() * 600);
            classLog('EmploysureSsm', 'GetSecretParams', `Retry ${count + 1}/${MAX_RETRIES}. Backoff: ${backOff}ms. Error: ${error}`, 'warn');
            return backOff;
        }
    }
});

/**
 * Simple wrapper over SSM's get-parameters to return the result in a key-value
 * pairing of the parameter's Name to the parameter's Value. For encrypted values.
 * 
 * E.g.: GetSecretParams(['/Service/Dev']) would get you:
 * {
 *  "/Service/Dev": "SecretValue"
 * }
 * 
 * @param Names 
 */
export async function GetSecretParams(Names: Array<string>): Promise<ParametersMap> {
    try {
        if (Names.length === 0) {
            throw new Error('Names is empty');
        }

        let SsmParams = { Names, WithDecryption: true };

        const data = await ssm.getParameters(SsmParams).promise();

        const InvalidParameters = data.InvalidParameters ? data.InvalidParameters : [];
        const Parameters = data.Parameters ? data.Parameters : [];

        if (InvalidParameters.length > 0) {
            throw new InvalidParameterError(InvalidParameters);
        }

        return Parameters.reduce((obj: ParametersMap, param) => {
            obj[param.Name as string] = param.Value as string;
            return obj;
        }, {});
    } catch (error) {
        classLog('EmploysureSsm', 'GetSecretParams', `${error}`, 'error');
        throw error;
    }
}
