import { SSM, DynamoDB } from "aws-sdk";
import { AWS_REGION_DEFAULT, AWS_SSM_API_VERSION } from "../constants";
import { ConfigFields, IConfig, InvalidParameterError, SlackChannel, SnsTopicSlackConfig } from "../models";
import { getDynamoTableRow } from "./dynamodb";

const ssm = new SSM({
    region: AWS_REGION_DEFAULT,
    apiVersion: AWS_SSM_API_VERSION
});

export function GetConfig(params: Array<ConfigFields>): Promise<IConfig> {
    if (params.length === 0) {
        throw new Error('params is empty')
    }

    const FieldToParam = process.env.stage === "Prod" ? ProdConfigFieldToParam : DevConfigFieldToParam;
    const Names = params.map(param => {
        if (FieldToParam[param]) {
            return FieldToParam[param]
        } else {
            throw new Error(`Field ${param} missing from mapping.`)
        }
    });


    let SsmParams = { Names, WithDecryption: true };

    return ssm.getParameters(SsmParams).promise()
        .then((data) => {
            const InvalidParameters = data.InvalidParameters ? data.InvalidParameters : [];
            const Parameters = data.Parameters ? data.Parameters : [];

            if (InvalidParameters.length > 0) {
                throw new InvalidParameterError(InvalidParameters)
            }

            const ParamToValue: {[x:string]: string} = Parameters.reduce((obj: {[x:string]: string}, param) => {
                obj[param.Name as string] = param.Value as string;
                return obj;
            }, {});
            
            return params.reduce((obj: {[x:string]: string}, field) => {
                obj[field] = ParamToValue[FieldToParam[field]];
                return obj;
            }, {});
        })
}


export async function GetSlackChannelID(name: string) {
    return new SlackChannel(
        (await getDynamoTableRow('SlackChannelsIDs', 'Name', name)) as DynamoDB.AttributeMap
    );
}

export async function GetSnsTopicSlackConfig(SnsTopicArn: string) {
    return new SnsTopicSlackConfig(
        (await getDynamoTableRow('SnsTopicSlackMappings', 'SnsTopicArn', SnsTopicArn)) as DynamoDB.AttributeMap
    );
}

const DevConfigFieldToParam: { [field in ConfigFields]: string } = {
    WorkplaceAccessToken: '/FacebookWorkplace/Dev/AccessToken',
    FirestoreCertificate: "/Firestore/EmployeeProvisioning/Dev/Certificate",
    BigQueryClientEmail: "/BigQuery/Dev/ClientEmail",
    BigQueryPrivateKey: "/BigQuery/Dev/PrivateKey",
    FlareHrKey: "/FlareHR/Dev/PrimaryKey",
    PingboardClientID: "/Pingboard/Dev/ClientID",
    PingboardClientSecret: "/Pingboard/Dev/ClientSecret",
    FlareHRPrimaryKey: "/FlareHR/Dev/PrimaryKey",
    FacebookAppId: "/FacebookLeadApp/Dev/AppID",
    FacebookAppSecret: "/FacebookLeadApp/Dev/AppSecret"
};

const ProdConfigFieldToParam: { [field in ConfigFields]: string } = {
    WorkplaceAccessToken: '/FacebookWorkplace/Prod/AccessToken',
    FirestoreCertificate: '/Firestore/EmployeeProvisioning/Prod/Certificate',
    BigQueryClientEmail: '/BigQuery/Prod/ClientEmail',
    BigQueryPrivateKey: '/BigQuery/Prod/PrivateKey',
    FlareHrKey: "/FlareHR/Prod/PrimaryKey",
    PingboardClientID: '/Pingboard/Prod/ClientID',
    PingboardClientSecret: '/Pingboard/Prod/ClientSecret',
    FlareHRPrimaryKey: '/FlareHR/Prod/PrimaryKey',
    FacebookAppId: "/FacebookLeadApp/Prod/AppID",
    FacebookAppSecret: "/FacebookLeadApp/Prod/AppSecret"
};
