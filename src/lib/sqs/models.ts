import { SQS } from "aws-sdk";

export interface ISendToSqsOptions {
    sendSequentially?: boolean;
    chunkSize?: number;
    messageIndexField?: string;
}

export interface ISendToSqsResponse {
    Success: SQS.SendMessageBatchResultEntryList;
    Failed: SQS.BatchResultErrorEntryList;
}