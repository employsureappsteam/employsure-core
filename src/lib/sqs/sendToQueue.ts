import { SQS } from "aws-sdk";
import { BatchResultErrorEntryList, SendMessageBatchResultEntryList } from "aws-sdk/clients/sqs";
import _ from "lodash";
import { AWS_REGION_DEFAULT } from "../../constants";
import { classLog } from "../../helpers";
import { ISendToSqsOptions } from "./models";

/**
 * 
 * @param QueueUrl QueueUrl to send messages to
 * @param messages Array to messages to send
 * @param options Employsure options. 
 * @returns 
 */
export const sendToQueue = async (QueueUrl: string, messages: Array<any>, options?: ISendToSqsOptions) => {
    /* Prepare data for SQS Queue */
    const sqsMessages = messages.map((message, index) => {
        const MessageBody = JSON.stringify(message);
        let Id = `${index}`;
        try {
            if (options?.messageIndexField !== undefined && typeof message[options.messageIndexField] === "string") {
                Id = message[options.messageIndexField]
            }
        } catch (error) {
            classLog(`sqs`, `sendToQueue`, `Failed to set Id using value from ${options?.messageIndexField}`, `warn`);
        }
        return { MessageBody, Id }
    });

    const chunkSize = options?.chunkSize ?? 10;
    const messagesChunked = _.chunk(sqsMessages, chunkSize);

    const sqs = new SQS({ region: AWS_REGION_DEFAULT });
    const Success: SendMessageBatchResultEntryList = [];
    const Failed: BatchResultErrorEntryList = [];

    if (options?.sendSequentially === true) {
        let count = 0;
        for (const Entries of messagesChunked) {
            count = count + 1;
            const r = await sqs.sendMessageBatch({
                QueueUrl,
                Entries
            }).promise();

            Success.push(...r.Successful);
            Failed.push(...r.Failed);
        }
    } else {
        const promises = messagesChunked.map(Entries => {
            const param = { QueueUrl, Entries };
            return sqs.sendMessageBatch(param).promise()
        });
        const results = await Promise.all(promises);
        for (const r of results) {
            Success.push(...r.Successful);
            Failed.push(...r.Failed);
        }
    }

    return {
        Success,
        Failed
    }
}