import { SQSEvent, Context, SQSRecord } from "aws-lambda";
import { EmploysureError } from "../../models/errors";
import { sendToSns } from "../sns/sendToSns";
import { buildLogStreamLink, parseAnyToString, parseSqsMessage, parseSqsMessageV2, postProcessMessageResults } from "./helpers";
import { BadRequestError, GenericAppFunction, GenericSqsHandlerOptions, HttpStatusCode, MessageResultList, MessageResult } from "./models";
import { instanceOfSqsEvent } from "./typeguards";

/** Parse and handle an SQS event. Returns status of function evaluation. */
export async function genericSqsHandlerV2<T, G>(
    event: SQSEvent,
    context: Context,
    fn: GenericAppFunction<T, G>,
    options?: GenericSqsHandlerOptions<T>
): Promise<string | void> {

    /**
     * To avoid crashing and repeating. Do not allow function to throw
     * lambda. Instead log and report the issue.
     */
    try {
        if (!instanceOfSqsEvent(event)) {
            throw new EmploysureError(`InvalidSqsEvent`, `Function only handles SQS Messages`);
        }

        /* TODO: update this to handle more. */
        if (event.Records.length > 1) {
            throw new EmploysureError(`TooManyRecords`, `Function only handles 1 message`);
        }

        const { errors, validRecords } = parseSqsMessageV2<T>(event, options);
        const processedRecordsResponses: MessageResultList = [];

        console.info(`Processing ${validRecords.length} messages, skipping ${errors.length} invalid messages`);
        for (const err of errors) {
            const message = `INVALID_MESSAGE_PAYLOAD ${err.message}`;
            console.warn(`${err.record.messageId} 400 ${message}`);
            processedRecordsResponses.push({
                source_arn: err.record.eventSourceARN,
                message_id: err.record.messageId,
                status: 400,
                status_message: message
            });
        }

        for (const validRecord of validRecords) {
            const { payload, record } = validRecord;
            processedRecordsResponses.push(await executeFunctionSqs(
                fn, payload, record, options?.appFunctionOptions
            ));
        }

        /* Post Processing of messages. */
        await postProcessMessageResults(options?.postProcessing, processedRecordsResponses, context);
    } catch (error) {
        console.error(error);

        /* Log event in case of unhandled error. */
        try {
            console.log(`event: ${JSON.stringify(event, null, 4)}`);
        } catch (error) {
            console.error(`Failed to log event. ${error}`)
        }

        /* Notify SQS in case of unhandled error. */
        try {
            const logStreamLink = buildLogStreamLink(context);
            const attachments = [
                { title: 'Error', message: error.message, details: error.stack },
                { title: 'Context', message: logStreamLink, details: JSON.stringify(context, null, 4) }
            ]
            await sendToSns(
                'arn:aws:sns:ap-southeast-2:711143483997:engineering-errors-prod'
                , `[Lambda Server Error] ${context.functionName} crashed with ${error.name}`
                , attachments);
        } catch (error) {
            console.error(`Failed to send error to engineering. ${error}`);
        }
    }
}


/* Execution function for a particular record. */
const executeFunctionSqs = async <T, G>(
    fn: GenericAppFunction<T, G>
    , payload: T
    , record: SQSRecord
    , options?: any
): Promise<MessageResult> => {
    try {
        const response = await fn(payload, options);
        const response_payload = parseAnyToString(response);

        return {
            source_arn: record.eventSourceARN,
            message_id: record.messageId,
            message_payload: payload,
            status: 200 as HttpStatusCode,
            status_message: 'Success',
            response_payload
        };
    } catch (error) {
        if ((error as BadRequestError).isBadRequestError) {
            return {
                source_arn: record.eventSourceARN,
                message_id: record.messageId,
                message_payload: payload,
                status: 400 as HttpStatusCode,
                status_message: `${error}`,
                error
            };
        } else {
            return {
                source_arn: record.eventSourceARN,
                message_id: record.messageId,
                message_payload: payload,
                status: 500 as HttpStatusCode,
                status_message: `${error}`,
                error
            };
        }
    }
}

