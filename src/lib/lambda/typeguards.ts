import { SNSEvent, SNSEventRecord, SNSMessage, SNSMessageAttribute, SNSMessageAttributes, SQSEvent, SQSRecord, SQSRecordAttributes } from 'aws-lambda';
import { isJsonObject } from '../../helpers';
import { objectHasValuesOf } from '../../helpers/typeguards/objectHasValuesOf';

export function instanceOfSqsEvent(event: SQSEvent): event is SQSEvent {
    return (
        Array.isArray(event.Records) &&
        event.Records
            .map(r => instanceOfSqsEventRecord(r))
            .reduce((p, c) => p && c, true)
    );
}

export function instanceOfSqsEventRecord(record: SQSRecord): record is SQSRecord {
    return (
        typeof record.messageId === "string" &&
        typeof record.receiptHandle === "string" &&
        typeof record.body === "string" &&
        typeof record.md5OfBody === "string" &&
        typeof record.eventSource === "string" &&
        typeof record.eventSourceARN === "string" &&
        typeof record.awsRegion === "string" &&
        instanceOfSqsRecordAttributes(record.attributes)
    )
}

export function instanceOfSqsRecordAttributes(att: SQSRecordAttributes): att is SQSRecordAttributes {
    return (
        typeof att.ApproximateFirstReceiveTimestamp === "string" &&
        typeof att.SentTimestamp === "string" &&
        typeof att.SenderId === "string" &&
        typeof att.ApproximateFirstReceiveTimestamp === "string"
    )
}

export function instanceOfSns(event: SNSEvent): event is SNSEvent {
    return (
        Array.isArray(event.Records) &&
        event.Records
            .map(r => instanceOfSnsEventRecord(r))
            .reduce((p, c) => p && c, true)
    );
}

export function instanceOfSnsEventRecord(r: SNSEventRecord): r is SNSEventRecord {
    return typeof r.EventVersion === 'string'
        && typeof r.EventSubscriptionArn === 'string'
        && typeof r.EventSource === 'string'
        && instanceOfSnsMessage(r.Sns);
}

export function instanceOfSnsMessage(m: SNSMessage): m is SNSMessage {
    return typeof m.SignatureVersion === 'string'
        && typeof m.Timestamp === 'string'
        && typeof m.Signature === 'string'
        && typeof m.SigningCertUrl === 'string'
        && typeof m.MessageId === 'string'
        && typeof m.Message === 'string'
        && instanceOfSNSMessageAttributes(m.MessageAttributes)
        && typeof m.Type === 'string'
        && typeof m.UnsubscribeUrl === 'string'
        && typeof m.TopicArn === 'string'
        && typeof m.Subject === 'string';
}

export function instanceOfSNSMessageAttributes(a: SNSMessageAttributes): a is SNSMessageAttributes {
    return isJsonObject(a)
        && objectHasValuesOf(a, instanceOfSnsMessageAttribute);
}

export function instanceOfSnsMessageAttribute(a: SNSMessageAttribute): a is SNSMessageAttribute {
    return isJsonObject(a)
        && typeof a.Type === 'string'
        && typeof a.Value === 'string';
}