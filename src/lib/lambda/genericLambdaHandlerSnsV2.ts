import { SQSEvent, Context, SQSRecord, SNSEvent, SNSEventRecord } from "aws-lambda";
import { RollbarClient } from "../../clients/rollbar";
import { classLog, loadBigQueryClient } from "../../helpers";
import { sendToSns } from "../sns/sendToSns";
import { sendToQueue } from "../sqs";
import { buildLogStreamLink, parseAnyToString, parseSnsMessage, postProcessMessageResults } from "./helpers";
import { AppFunctionOptions, BadRequestError, GenericAppFunction, GenericSnsHandlerOptions, GenericSqsHandlerOptions, HttpStatusCode, PostProcessingOptions, MessageResult, MessageResultList } from "./models";
import { instanceOfSns, instanceOfSqsEvent } from "./typeguards";

export const wrapperErrorCodes = {
    InvalidEvent: "CONFIG_ERROR-INVALID_EVENT",
    TooManyRecords: "CONFIG_ERROR-TOO_MANY_RECORDS"
}

/** Parse and handle an SNS event. Returns status of function evaluation. */
export async function genericSnsHanderV2<T, G>(
    event: SNSEvent,
    context: Context,
    fn: GenericAppFunction<T, G>,
    options?: GenericSnsHandlerOptions<T>
): Promise<string | void> {
    /**
     * To avoid crashing and repeating. Do not allow function to throw
     * lambda. Instead log and report the issue.
     */
    try {
        if (!instanceOfSns(event)) {
            const message = 'Invalid SNS Event';
            console.warn(`${wrapperErrorCodes.InvalidEvent} ${message}`);
            return wrapperErrorCodes.InvalidEvent;
        }

        const { errors, validRecords } = parseSnsMessage<T>(event, options);
        const processedRecordsResponses: MessageResultList = [];

        /* Log any invalid messages as warnings. */
        console.info(`Processing ${validRecords.length} messages, skipping ${errors.length} invalid messages`);
        for (const err of errors) {
            const message = `INVALID_MESSAGE_PAYLOAD ${err.message}`;
            console.warn(`${err.record.Sns.MessageId} 400 ${message}`);
            processedRecordsResponses.push({
                source_arn: err.record.Sns.TopicArn,
                message_id: err.record.Sns.MessageId,
                status: 400,
                status_message: message
            });
        }

        const processRecordsIn = options?.processRecordsIn ?? 'serial';
        if (processRecordsIn === 'serial') {
            for (const valid of validRecords) {
                const result = await executeFunctionSns(
                    fn,
                    valid.payload,
                    valid.record,
                    options?.appFunctionOptions
                );
                processedRecordsResponses.push(result);
            }
        } else if (processRecordsIn === 'parallel') {
            const results = await Promise.all(
                validRecords.map(async valid => executeFunctionSns(
                    fn,
                    valid.payload,
                    valid.record,
                    options?.appFunctionOptions
                ))
            );
            processedRecordsResponses.push(...results);
        }

        /* Post Processing of messages. */
        await postProcessMessageResults(options?.postProcessing, processedRecordsResponses, context);
    } catch (error) {
        console.error(error);

        /* Log event in case of unhandled error. */
        try {
            console.log(`event: ${JSON.stringify(event, null, 4)}`);
        } catch (error) {
            console.error(`Failed to log event causing above error. ${error}`);
        }

        /* Notify SQS in case of unhandled error. */
        try {
            const logStreamLink = buildLogStreamLink(context);
            const attachments = [
                { title: 'Error', message: error.message, details: error.stack },
                { title: 'Context', message: logStreamLink, details: JSON.stringify(context, null, 4) }
            ]
            await sendToSns(
                'arn:aws:sns:ap-southeast-2:711143483997:engineering-errors-prod'
                , `[Lambda Server Error] ${context.functionName} crashed with ${error.name}`
                , attachments);
        } catch (error) {
            console.error(`Failed to send error to engineering. ${error}`);
        }
    }
}

/* Execution function for a particular record. */
const executeFunctionSns = async <T, G>(
    fn: GenericAppFunction<T, G>
    , payload: T
    , record: SNSEventRecord
    , options?: any
): Promise<MessageResult> => {
    try {
        const response = await fn(payload, options);
        const response_payload = parseAnyToString(response);
        return {
            source_arn: record.Sns.TopicArn,
            message_id: record.Sns.MessageId,
            message_payload: payload,
            status: 200 as HttpStatusCode,
            status_message: 'Success',
            response_payload
        };
    } catch (error) {
        if ((error as BadRequestError).isBadRequestError) {
            return {
                source_arn: record.Sns.TopicArn,
                message_id: record.Sns.MessageId,
                message_payload: payload,
                status: 400 as HttpStatusCode,
                status_message: `${error}`,
                error
            };
        } else {
            return {
                source_arn: record.Sns.TopicArn,
                message_id: record.Sns.MessageId,
                message_payload: payload,
                status: 500 as HttpStatusCode,
                status_message: `${error}`,
                error
            };
        }
    }
}
