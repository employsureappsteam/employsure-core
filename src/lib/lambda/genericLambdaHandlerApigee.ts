import { buildLogStreamLink } from './helpers';
import { sendToSns } from "../sns/sendToSns";
import { BadRequestError, genericLambdaHandlerApigeeFn, GenericLambdaHandlerFn, InvalidPayload } from "./models";
import { ApigeeBadRequestResponse, ApigeeResponse, ApigeeServerErrorResponse } from '../../models';
import { classLog } from '../../helpers/clients/classLog';

/**
 * Generic wrapper for business logic function to handle server errors,
 * logging, and responses consistently.
 *
 * @param event The event data received by lambda
 * @param context The Context of the lambda that was invoked
 * @param appFunction The function to call and pass the event to
 * @param options Options for the appFunction
 */
export const genericLambdaHandlerApigee: genericLambdaHandlerApigeeFn = async (
    event
    , context
    , appFunction
    , options
) => {
    try {
        if (options?.isEventValid !== undefined) {
            /* If invalid, this function should throw an InvalidPayload error with a detailed message. */
            const isValid = options.isEventValid(event);
            if (!isValid) {
                classLog('lib-lambda', 'genericLambdaHandlerApigee', `event is invalid`);
                throw new InvalidPayload();
            }
        }

        const response = await appFunction(event, context, options?.appFunctionOptions);
        return new ApigeeResponse("OK", response);
    } catch (error) {

        /* Handle 4xx errors gracefully. */
        if ((error as BadRequestError).isBadRequestError) {
            const errorBadReq = error as BadRequestError;
            classLog('lib-lambda', 'genericLambdaHandlerApigee', `BAD_REQUEST - ${error.name}: ${error.message}`, 'warn');
            return new ApigeeBadRequestResponse(errorBadReq, context);
        }

        /* Maybe send Server Errors to SNS. */
        const snsTopic = options?.serverErrorSnsTopic;
        if (snsTopic !== undefined) {
            const logStreamLink = buildLogStreamLink(context);
            const attachments = [
                { title: 'Error', message: error.message, details: error.stack },
                { title: 'Context', message: logStreamLink, details: JSON.stringify(context, null, 4) }
            ]
            await sendToSns(snsTopic, `[Lambda Server Error] ${context.functionName} crashed with ${error.name}`, attachments);
        }

        classLog('lib-lambda', 'genericLambdaHandlerApigee', `SERVER_ERROR - ${error.name}: ${error.message}`, 'error');
        return new ApigeeServerErrorResponse(context);
    }
}

