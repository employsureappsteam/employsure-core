import { buildLogStreamLink } from './helpers';
import { sendToSns } from "../sns/sendToSns";
import { BadRequestError, GenericLambdaHandlerFn, InvalidPayload } from "./models";

/**
 * Generic wrapper for business logic function to handle server errors,
 * logging, and responses consistently.
 *
 * @param event The event data received by lambda
 * @param businessFunction The function to call and pass the event to
 * @param lambdaContext The Context of the lambda that was invoked
 * @param packageVersion The version of the project/code
 * @param businessFunctionConfig (Optional) options to configure the businessFunction
 */
export const genericLambdaHandler: GenericLambdaHandlerFn = async (
    event
    , context
    , appFunction
    , options
) => {
    try {
        if (options?.isEventValid !== undefined) {
            /* Check if valid. This should throw, if not throw a generic error. */
            const isValid = options.isEventValid(event);
            if (!isValid) {
                throw new InvalidPayload()
            }
        }

        const response = await appFunction(event, context, options?.appFunctionOptions);
        return response;
    } catch (error) {
        /* Handle 4xx errors gracefully. */
        if ((error as BadRequestError).isBadRequestError) {
            return {
                error: {
                    code: 400,
                    type: error.name,
                    message: error.message,
                    requestId: context.awsRequestId
                }
            }
        }

        /* Maybe send Server Errors to SNS. */
        const snsTopic = options?.serverErrorSnsTopic;
        if (snsTopic !== undefined) {
            const logStreamLink = buildLogStreamLink(context);
            const attachments = [
                { title: 'Error', message: error.message, details: error.stack },
                { title: 'Context', message: logStreamLink, details: JSON.stringify(context, null, 4) }
            ]
            await sendToSns(snsTopic, `[Lambda Server Error] ${context.functionName} crashed with ${error.name}`, attachments);
        }

        /* Return error without details. */
        console.error(`${error.name}: ${error.message}`);
        if ((options?.serverErrorBehaviour ?? 'supress') === 'supress') {
            return {
                error: {
                    code: 500,
                    type: 'ServerError',
                    message: 'Server Error',
                    requestId: context.awsRequestId
                }
            }
        } else {
            throw error;
        }
    }
}

