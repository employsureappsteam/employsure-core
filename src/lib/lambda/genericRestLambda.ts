import { LambdaError, ApigeeResponse } from "../../models";
import { HttpStatusCodes, HttpStatusMessages, SnsTopics } from "../../constants";
import { sendToSns } from "../sns/sendToSns";
import { EmpLogger } from "../emplogger";
import { Context } from "aws-lambda";
import { RestApiFunction } from "./models";

/**
 * Generic wrapper for business logic function to handle server errors,
 * logging, and responses consistently.
 *
 * @param event The event data received by lambda
 * @param businessFunction The function to call and pass the event to
 * @param lambdaContext The Context of the lambda that was invoked
 * @param packageVersion The version of the project/code
 * @param businessFunctionConfig (Optional) options to configure the businessFunction
 */
export async function genericRestLambda<Event, Options, Response>(
    event: Event,
    businessFunction: RestApiFunction<Event, Options, Response>,
    lambdaContext: Context,
    packageVersion: string,
    businessFunctionConfig?: Options,
): Promise<ApigeeResponse<Response>> {
    const logger = new EmpLogger();
    try {
        logger.log(`Stage: ${process.env.stage}, Version: ${packageVersion}`);
        logger.log(`Event received: ${JSON.stringify(event, null, 4)}`);
        const response = await businessFunction(event, logger, businessFunctionConfig);
        logger.log(`Status: ${response.StatusCode}`);
        logger.log(`Result: ${JSON.stringify(response.Data, null, 4)}`);
        return response;
    } catch (error) {
        /* Don't log entire error with JSON.stringify(), details will be in notification */
        logger.log(`${error}`);

        // TODO: the api error notifications should live in apigee
        await sendToSns(
            SnsTopics.EngErrorsProd,
            `${lambdaContext.functionName} has failed`,
            [
                {
                    title: `${error.name}`,
                    message: `${error}`,
                    details: `${error.stack}`,
                },
                {
                    title: `Log`,
                    message: ``,
                    details: logger.getLogHistory().join("\n")
                }
            ],
        );

        throw new LambdaError({
            StatusCode: HttpStatusCodes.SERVER_ERROR,
            Data: "Please try again and contact Employsure Engineering if issue persists."
        });
    }
}
