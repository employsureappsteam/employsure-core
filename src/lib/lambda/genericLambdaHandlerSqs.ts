import { SQSEvent, Context, SQSRecord } from "aws-lambda";
import { sendToSns } from "../sns/sendToSns";
import { buildLogStreamLink, parseSqsMessage } from "./helpers";
import { GenericSqsHandlerOptions } from "./models";
import { instanceOfSqsEvent } from "./typeguards";

export const runTimeErrorCodes = {
    InvalidEvent: "EXIT-ERROR-INVALID-EVENT",
    TooManyRecords: "EXIT-ERROR-TOO-MANY-RECORDS",
    InvalidMessage: "EXIT-ERROR-INVALID-MESSAGE",
    UnhandledError: "EXIT-ERROR-UNHANDLED",
}

export const runTimeSuccessCode = {
    Ok: "EXIT-SUCCESS"
}

/** Parse and handle an SQS event. Returns status of function evaluation. */
export async function genericSqsHandler<T, G>(
    event: SQSEvent,
    context: Context,
    fn: (e: T, c: Context, options: any, r: SQSRecord) => Promise<G>,
    options?: GenericSqsHandlerOptions<T>,
    recordLimit = 1
) {
    if (!instanceOfSqsEvent(event)) {
        const message = 'Invalid SQS Event';
        console.log(`${runTimeErrorCodes.InvalidEvent} ${message}`);
        return runTimeErrorCodes.InvalidEvent;
    }

    if (event.Records.length > recordLimit) {
        const message = `Did not process: received more than the record limit for this function (${recordLimit} record/s)`;
        console.log(`${runTimeErrorCodes.TooManyRecords} ${message}`);
        return runTimeErrorCodes.TooManyRecords;
    }

    const parsedEvent = parseSqsMessage<T>(event, options);

    if (parsedEvent.errors.length !== 0) {
        const message = `Did not process due to parsing errors: ${parsedEvent.errors.map(e => `${e.message}`).join(", ")}`;
        console.log(`${runTimeErrorCodes.InvalidMessage} ${message}`);
        return runTimeErrorCodes.InvalidMessage;
    }

    /**
     * To avoid crashing and repeating. Do not allow function to throw
     * lambda. Instead log and report the issue.
     */
    try {
        // const p = parsedEvent.parsed[0].record.
        const { parsedBody, record } = parsedEvent.parsed[0];
        await fn(parsedBody, context, options?.appFunctionOptions, record);
        console.log(`${runTimeSuccessCode.Ok}`);
        return runTimeSuccessCode.Ok;
    } catch (error) {
        console.error(`${runTimeErrorCodes.UnhandledError} ${error}`);

        /* Log event in case of unhandled error. */
        try {
            console.log(`event: ${JSON.stringify(event, null, 4)}`);
        } catch (error) {
            console.error(`Failed to log event.`)
        }

        /* Notify SQS in case of unhandled error. */
        try {
            const logStreamLink = buildLogStreamLink(context);
            const attachments = [
                { title: 'Error', message: error.message, details: error.stack },
                { title: 'Context', message: logStreamLink, details: JSON.stringify(context, null, 4) }
            ]
            await sendToSns(
                'arn:aws:sns:ap-southeast-2:711143483997:engineering-errors-prod'
                , `[Lambda Server Error] ${context.functionName} crashed with ${error.name}`
                , attachments);
        } catch (error) {
            console.error(`Failed to send error to engineering.`);
        }

        return runTimeErrorCodes.UnhandledError;
    }
}
