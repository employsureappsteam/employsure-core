import { Context } from 'aws-lambda';
import { v4 as uuid4 } from 'uuid';

/**
 * @returns mock object for AWS Lambda Context
 */
export const createMockLambdaContext = (userMock?: Context): Context => {
    const functionName = 'portalDocumentService-full-processNewDocument'
    const defaultMock = {
        "callbackWaitsForEmptyEventLoop": true,
        "functionVersion": "$LATEST",
        "functionName": functionName,
        "memoryLimitInMB": "256",
        "logGroupName": `/aws/lambda/${functionName}`,
        "logStreamName": "YYYY/MM/DD/[$LATEST]XXX",
        "invokedFunctionArn": `arn:aws:lambda:REGION:XXXXXXXXX:function:${functionName}`,
        "awsRequestId": `mock_${uuid4()}`,
        getRemainingTimeInMillis: () => 0,
        done: () => null,
        fail: () => null,
        succeed: () => null
    }
    return {
        ...userMock,
        ...defaultMock
    }
}