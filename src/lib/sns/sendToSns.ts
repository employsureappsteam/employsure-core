import { SNS } from 'aws-sdk';
import { SnsMessage, TopicArns, ISnsGlobalConfig, IPublishMessage } from '../../models/sns';
import { AWS_REGION_DEFAULT, SnsTopics } from '../../constants';

const config: ISnsGlobalConfig = {
    supressAllNotifications: false
}

type SendToSnsOptions = {
    ignoreStageCheck: boolean
}

export function supressNotifications() {
    config.supressAllNotifications = true;
}

/**
 * 
 * @param snsTopic arn of topic to send notification to. Use SnsTopics const.
 * @param subject title of the notification. Used as subject in email.
 * @param messages messages to be sent. Will be appended together in email.
 */
export async function sendToSns(snsTopic: TopicArns | string, subject: string, messages: SnsMessage[], options?: SendToSnsOptions) {
    const client = new SNS({ region: AWS_REGION_DEFAULT });

    if (config.supressAllNotifications) {
        return "SNS Notifications Supressed";
    }

    if (messages.length === 0) {
        throw new Error("No messages. Cannot send empty notification.");
    }

    /* Send everything not in prod to dev channel. */
    let topicArn = snsTopic;
    if (options?.ignoreStageCheck !== true && process.env.stage !== "Prod") {
        topicArn = SnsTopics.EngErrorsDev
    }

    /* Append subject with Stage if it's not production. */
    let stagePrefix = '';
    if (options?.ignoreStageCheck !== true && process.env.stage !== "Prod") {
        // topicArn = SnsTopics.EngErrorsDev
        stagePrefix = `${process.env.stage} | `;
    }

    const defaultMessage = messages.map(e => {
        return `${e.title}\n${e.message}${e.details ? '\n\n' + e.details : ""}`;
    }).join('\n\n');

    const publishMessage: IPublishMessage = {
        default: defaultMessage,
        lambda: JSON.stringify({ Errors: [], Logs: messages }),
        email: `${stagePrefix}${subject}\n${defaultMessage}\n`
    }

    return client.publish({
        TopicArn: topicArn,
        Subject: `${stagePrefix}${subject}`,
        Message: JSON.stringify(publishMessage),
        MessageStructure: 'json'
    }).promise();
}