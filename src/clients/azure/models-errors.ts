export class AzureUserError extends Error {
    name = "AzureBadRequestError";
    constructor(message: string) {
        super(message)
    }
}

export class AzureResponseError<T = any> extends Error {
    name = "AzureResponseError";
    status: number;
    data: T;

    constructor(status: number, data: T, message: string) {
        super(message);
        this.status = status;
        this.data = data;
    }
}