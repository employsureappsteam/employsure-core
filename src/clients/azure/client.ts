import Axios, { AxiosInstance, AxiosError } from "axios";
import { IAzureClientConfig, IFieldValueSet, IGetDriveParameters, IGetOptions, ISharepointListItem, ISharepointListItemUpdateResponse, IUploadFileParamaters } from "./models";
import * as qs from 'querystring';
import { IAzureLoginResponse, IGraphQlResponse, IAzureUser, IAzureGroup, IAzureGroupMember, GetDriveResponse, DriveItem } from "./models-rest";
import { AzureResponseError, AzureUserError } from "./models-errors";

export class AzureClient {
    /* Assume _api is always not undefined. */
    private _api: AxiosInstance;
    private _config: IAzureClientConfig;

    constructor(config: IAzureClientConfig) {
        this._config = config;
    }

    /* Internal helper function to initialise the client first time a method is used. Add to all methods. */
    private async checkClientIsInitialised() {
        if (!this._api) {
            await this.init();
        }
    }

    async init() {
        const tenantId = this._config.tenantId;
        const client_secret = this._config.clientSecret;
        const client_id = this._config.clientId;

        /* Note: The scope “https://graph.microsoft.com/.default” tells the Microsoft Graph to return a token with the application permissions already configured for the Azure AD app. */
        const scope = 'https://graph.microsoft.com/.default';
        const grant_type = 'client_credentials';

        const bodyEncoded = qs.stringify({
            grant_type,
            client_id,
            client_secret,
            scope
        });

        const loginData = await Axios
            .post<IAzureLoginResponse>(
                `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
                bodyEncoded,
                {
                    headers: {
                        "Content-Type": 'application/x-www-form-urlencoded'
                    }
                }
            ).then(response => response.data);

        this._api = Axios.create(
            {
                baseURL: 'https://graph.microsoft.com/v1.0/',
                headers: {
                    'Authorization': `Bearer ${loginData.access_token}`,
                    'Content-Type': 'application/json'
                }
            }
        )
    }

    /** Fetch all users on Azure AD */
    async getUsers(): Promise<Array<IAzureUser>> {
        const graphQlFields = [
            'accountEnabled',
            'DisplayName',
            'mail',
            'id',
            'userPrincipalName'
        ];

        let query = `users?$select=${graphQlFields.join(",")}&$top=999`;

        return this.getList<IAzureUser>(query);
    }

    /**
     * This is a wrapped around endpoints that return lists. See here: https://docs.microsoft.com/en-us/rest/api/azure/#async-operations-throttling-and-paging
     * From the docs: "Some list operations return a property called nextLink in the response body. You see this property when the results are too large to return in one response. Typically, the response includes the nextLink property when the list operation returns more than 1,000 items. When nextLink isn't present in the results, the returned results are complete. When nextLink contains a URL, the returned results are just part of the total result set."
     * 
     * "OData query parameters" can be used to operate on the data before it is returned. Read more here: https://docs.microsoft.com/en-us/graph/query-parameters. Note not all endpoints support all OData query parameters. Check endpoint documentation for what specifically is supported.
     * 
     * @param endpoint endpoint to access the list (not including base url, e.g. auditLogs/signIns)
     * @param options use this to limit the number of requests made as there is no way of telling how many items there might be in the list. And thus no way to tell how many requests will be made. Use $top=999 to return the maximum number of items in one request.
     */
    async getList<T = any>(endpoint: string, options?: IGetOptions): Promise<Array<T>> {
        await this.checkClientIsInitialised();

        const maxRequests = options?.maxRequests;

        const data: Array<T> = [];
        let requestsMade = 0;

        while (endpoint) {
            const response = await this._api
                .get<IGraphQlResponse<T>>(endpoint)
                .then(response => response.data)
                .catch(error => { throw this._handleError(error) });

            if (response.value === undefined) {
                throw new Error(`Invalid response from ${endpoint} ($.value is missing).`)
            }

            data.push(...response.value);
            endpoint = response["@odata.nextLink"];

            requestsMade = requestsMade + 1;

            if (maxRequests && requestsMade >= maxRequests) {
                break;
            }
        }

        return data;
    }

    async getGroups(): Promise<Array<IAzureGroup>> {
        await this.checkClientIsInitialised();
        const endpoint = `groups/?$top=999`;
        return this.getList<IAzureGroup>(endpoint);
    }

    async getGroupMembers(groupId: string): Promise<Array<IAzureGroupMember>> {
        await this.checkClientIsInitialised();
        const endpoint = `groups/${groupId}/members?$top=999`;
        return this.getList<IAzureGroupMember>(endpoint);
    }

    async addGroupMember(groupId: string, memberId: string): Promise<null> {
        await this.checkClientIsInitialised();

        const endpoint = `groups/${groupId}/members/$ref`;
        const body = {
            "@odata.id": `https://graph.microsoft.com/v1.0/directoryObjects/${memberId}`
        }

        try {
            await this._api.post<any>(endpoint, body);
            /* 204 returned on success */
            return null;
        } catch (rtError) {
            throw this._handleError(rtError);
        }
    }

    async removeGroupMember(groupId: string, memberId: string): Promise<null> {
        await this.checkClientIsInitialised();

        const endpoint = `groups/${groupId}/members/${memberId}/$ref`;

        try {
            await this._api.delete(endpoint);
            /* 204 returned on success */
            return null;
        } catch (rtError) {
            throw this._handleError(rtError);
        }
    }

    async updateSharepointListItem<T = { [key: string]: any }>(item: ISharepointListItem, fieldValueSet: IFieldValueSet) {
        await this.checkClientIsInitialised();

        const endpoint = `sites/${item.siteId}/lists/${item.listId}/items/${item.itemId}/fields`;

        try {
            const d = await this._api.patch<ISharepointListItemUpdateResponse & T>(endpoint, fieldValueSet);
            return d.data;
        } catch (e) {
            throw this._handleError(e);
        }
    }

    /**
     * Uploads a file to a Sharepoint Drive. Will overwrite file if exists. 
     * 
     * @param param site, drive, and file details
     * 
     * https://docs.microsoft.com/en-us/graph/api/driveitem-put-content?view=graph-rest-1.0
     */
    async uploadFile(param: IUploadFileParamaters): Promise<DriveItem> {
        await this.checkClientIsInitialised();
        const { siteId, drive, file } = param;
        if (drive.name === undefined && drive.id === undefined) {
            throw new AzureUserError(`Must provide either drive name or id`);
        }

        const driveId = drive.id ?? (await this.getDrive({ siteId, driveName: drive.name as string })).id;
        const endpoint = `sites/${param.siteId}/drives/${driveId}/items/root:/${file.filename}:/content`

        try {
            const axiosConfig = { headers: { "Content-Type": file.type } };
            const d = await this._api.put<DriveItem>(endpoint, file.body, axiosConfig);
            return d.data;
        } catch (e) {
            throw this._handleError(e);
        }
    }

    async getDrive(param: IGetDriveParameters) {
        await this.checkClientIsInitialised();
        const endpoint = `sites/${param.siteId}/drives`;
        try {
            const response = await this._api.get<GetDriveResponse>(endpoint);
            for (const drive of response.data.value) {
                if (drive.name === param.driveName) {
                    return drive;
                }
            }
            throw new AzureUserError(`Cannot find drive '${param.driveName}'`);
        } catch (e) {
            throw this._handleError(e);
        }

    }

    /* Throw and wrap error if from axios, otherwise rethrow unknown error. */
    private _handleError(rtError: AxiosError): AzureResponseError | Error {
        if (rtError.isAxiosError) {
            const respData = rtError?.response?.data;
            const message = rtError?.response?.data?.error?.message;
            const status = rtError?.response?.status;
            return new AzureResponseError(
                status ?? 500,
                respData ?? {},
                `${message}`
            );
        } else {
            return rtError;
        }
    }
}