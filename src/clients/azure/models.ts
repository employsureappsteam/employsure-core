export interface IAzureClientConfig {
    clientId: string,
    clientSecret: string,
    tenantId: string
}

export interface IGetOptions {
    maxRequests?: number
}

export interface ISharepointListItem {
    siteId: string,
    listId: string,
    itemId: string
}

export type ISharepointFieldValue = string | number | boolean;
export interface IFieldValueSet {
    [key: string]: ISharepointFieldValue
}

export interface ISharepointListItemUpdateResponse {
    "@odata.context": string;
    "@odata.etag": string;
    id: string;
    ContentType: string;
    Modified: Date;
    Created: Date;
    AuthorLookupId: string;
    EditorLookupId: string;
    _UIVersionString: string;
    Attachments: boolean;
    Edit: string;
    LinkTitleNoMenu: string;
    LinkTitle: string;
    ItemChildCount: string;
    FolderChildCount: string;
    _ComplianceFlags: string;
    _ComplianceTag: string;
    _ComplianceTagWrittenTime: string;
    _ComplianceTagUserId: string;
    AppEditorLookupId: string;
}

export interface IUploadFileParamaters {
    /* Site Id to upload file to. */
    siteId: string,
    /* Drive to upload file to. Must provided either name or id. */
    drive: {
        name?: string,
        id?: string
    },
    file: {
        /* Body of file. Encoding depends on type. */
        body: any,
        /* The HTTP Content-Type for provided body. e.g. a body of type string would be "text/plain" */
        type: string,
        /* Filename including path and extension. */
        filename: string,
    }
}

export interface IGetDriveParameters {
    siteId: string,
    driveName: string
}

