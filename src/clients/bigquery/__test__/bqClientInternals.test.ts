import chai, { expect } from "chai";
import { mapValuesDeep } from "../../../helpers/array";
import { patchStringToTime, patchToIsoString } from "../helpers";

/* Chai extensions */
describe('Mapping over data to insert', () => {
    /* Testing to fix weird bug. */
    it('Map Values Deep shouldn\'t change arrays', () => {
        let data = [
            {
                name: "tires & co",
                services: [
                    {
                        name: "test1",
                        timeA: "abc"
                    },
                    {
                        name: "test2",
                        timeA: "def"
                    },
                ]
            }
        ]

        data = data.map(d => mapValuesDeep(d, patchToIsoString))

        expect(data[0].services).to.have.length(2);
    });

    /* Testing to fix weird bug. */
    it('Map Values Deep should update values in nested arrays/objects', () => {
        let data = [
            {
                "name": "John Eric",
                "service_appointments__r": [
                    {
                        "attributes": {
                            "timestamp": "2020-12-06T06:18:47.000+0000"
                        },
                        "LastModifiedDate": "2020-12-05T06:18:47.000+0000"
                    }
                ],
                "lastmodifieddate": "2020-12-03T06:18:47.000+0000",
                "__emp_meta": {
                    "timestamp": "2020-12-04T06:18:47.000+0000"
                }
            }
        ]

        data = data.map(d => mapValuesDeep(d, patchToIsoString))

        expect(data[0].lastmodifieddate).to.equal("2020-12-03T06:18:47.000Z");
        expect(data[0].__emp_meta.timestamp).to.equal("2020-12-04T06:18:47.000Z");
        expect(data[0].service_appointments__r[0].LastModifiedDate).to.equal("2020-12-05T06:18:47.000Z");
        expect(data[0].service_appointments__r[0].attributes.timestamp).to.equal("2020-12-06T06:18:47.000Z");
    });
});


/* Chai extensions */
describe('Fixing Time Strings', () => {
    /* Testing to fix weird bug. */
    it('Salesforce time strings should be fixed', () => {
        let data = [
            {
                timestamp: "10:00:00.000Z"
            },
            {
                timestamp: "hello"
            }
        ]

        data = data.map(d => mapValuesDeep(d, patchStringToTime));

        expect(data[0].timestamp).to.equal("10:00:00.000");
        expect(data[1].timestamp).to.equal("hello");
    });
});

