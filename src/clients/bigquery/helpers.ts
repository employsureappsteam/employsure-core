import { salesforceTime, salesforceTimestamp } from "./const";

/**
 * Takes any value and patches to ISO string if possible. Otherwise returns original value.
 */
export const patchToIsoString = (value) => {
    /* Convert Date/Time data types. */
    if (typeof value === "string" && value.match(salesforceTimestamp)
    ) {
        return new Date(value).toISOString();
    } else {
        return value;
    }
}

/**
 * Takes any value and patches to ISO string if possible. Otherwise returns original value.
 */
export const patchStringToTime = (value) => {
    /* Convert Date/Time data types. */
    if (typeof value === "string") {
        const matchObj = value.match(salesforceTime);
        if (matchObj) {
            return `${matchObj[1]}:${matchObj[2]}:${matchObj[3]}.${matchObj[4]}`
        }
    }

    return value;
}