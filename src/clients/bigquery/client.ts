import { BigQuery, DatasetDeleteOptions, DatasetResource, GetDatasetsOptions, TableMetadata, ViewDefinition, } from '@google-cloud/bigquery';
import { EMPLOYSURE_LOG_JOB_ID_FIELD_NAME, EMPLOYSURE_LOG_TIMESTAMP_FIELD_NAME } from '../../constants/big-query';

import { chunkArray, mapValuesDeep } from '../../helpers/array';
import { v4 as uuidv4 } from 'uuid';
import { BigQueryCredentials, IBigQuerySchema, IEmploysureInsertOpts, IGetDatasetTablesOpt, TableEnriched, TableCustomEmp, CreateViewOptions, GetDatasetOptions } from './models';
import { patchStringToTime, patchToIsoString } from './helpers';
import { BigQueryClientError } from './errors/bigQueryClientError';
import { IClientOptions, VerboseLoggingLevel } from '../../helpers/clients/models';
import { classLog } from '../../helpers/clients/classLog';
import { DEFAULT_INSERT_CHUNK_SIZE } from './const';
import { CreateViewError } from './errors/createViewError';


/**
 * Lightweight wrapper around Google's BigQuery SDK.
 * 
 * Many of the methods can and should be access directly by using Google's SDK.
 * Access these with: bigQueryClient.client.method()
 */
export class BigQueryClient {
    public client: BigQuery;
    private options?: IClientOptions;

    constructor(projectId: string, credentials: BigQueryCredentials, options?: IClientOptions) {
        this.client = new BigQuery({
            projectId,
            credentials,
        });
        this.options = options;
    }

    /**
     *
     * @param datasetId
     * Name of the dataset. Must exist already.
     *
     * @param tableId
     * Name of the table. If the table doesn't exist it will be created. This can take longer than 2 minutes.
     *
     * @param rows
     * Array of data to insert into table. Assumed all items have the same keys.
     *
     * @param options
     * Options to enable other behaviour
     *
     * For examples see: https://googleapis.dev/nodejs/bigquery/latest/Table.html#insert-examples
     */
    async insertToTable(
        datasetId: string,
        tableId: string,
        rows: Array<{}>,
        schema?: Array<IBigQuerySchema>,
        options?: IEmploysureInsertOpts
    ) {
        this.log("insertToTable", `datasetId: ${datasetId}, tableId: ${tableId}, data.length: ${rows.length}`);
        this.log("insertToTable", `options: ${JSON.stringify(options)}`);

        /* Get the keys for each of the */
        if (schema !== undefined) {
            this.validateSchema(rows, schema);
        }

        /** Convert booleans to string as BigQuery doesn't support it. (TODO: what does this mean?)
         *
         *  Error: "Conversion from bool to string is unsupported"
         */
        if (schema !== undefined) {
            const booleanFields = schema.filter((s) => s.type === 'BOOLEAN');
            for (const field of booleanFields) {
                for (const _d of rows) {
                    _d[field.name] = _d[field.name] ? 'true' : 'false';
                }
            }
        }

        /* Convert unsupported timestamp strings to expected format. */
        /* e.g. 'Could not parse '2020-04-29T02:25:16.000+0000' as a timestamp. Required format is YYYY-MM-DD HH:MM[:SS[.SSSSSS]]' */
        if (!options || !options.disableTimestampPatching) {
            rows = rows.map(d => mapValuesDeep(d, patchToIsoString));
        }
        rows = rows.map(d => mapValuesDeep(d, patchStringToTime));

        const timestamp = new Date();
        const jobId = uuidv4();

        if (options && options.useLogProfile) {
            /* TODO: deep copy this array instead (lodash) */
            rows = rows.map((d) => {
                d[EMPLOYSURE_LOG_TIMESTAMP_FIELD_NAME] = timestamp.toISOString();
                d[EMPLOYSURE_LOG_JOB_ID_FIELD_NAME] = jobId;
                return d;
            });

            if (schema !== undefined) {
                schema.push({
                    name: EMPLOYSURE_LOG_TIMESTAMP_FIELD_NAME,
                    type: 'TIMESTAMP',
                });
                schema.push({
                    name: EMPLOYSURE_LOG_JOB_ID_FIELD_NAME,
                    type: 'STRING',
                });
            }
        }

        const da = this.getDataset(datasetId)
        const dataset = this.client.dataset(datasetId)
        const table = dataset.table(tableId);

        /* Chunk the rows into updates, send to bigquery in parallel, and wait. */
        const chunkSize = options?.chunkSize ?? DEFAULT_INSERT_CHUNK_SIZE;
        this.log("insertToTable", `chunkSize: ${chunkSize}`);
        const insertJobsPending = chunkArray(rows, chunkSize)
            .map(async (row) => {
                const [insertRowResponse] = await table.insert(row, { schema })
                return insertRowResponse;
            });
        const insertJobsDone = (await Promise.all(insertJobsPending));

        this.log("insertToTable", `finished ${insertJobsDone.length} insert jobs`);

        return {
            insertJobsDone,
            employsureJob: {
                timestamp,
                jobId,
            }
        }
    }

    async query<T>(query: string) {
        this.log("query", `running`);
        const options = {
            query,
            useLegacySql: false,
        };

        const [job] = await this.client.createQueryJob(options);
        await job.promise();
        const [result] = await job.getQueryResults();
        this.log("query", `done. results.length: ${result.length}`);
        return result as Array<T>;
    }

    async createDataset(id: string, options?: DatasetResource) {
        const [dataset, apiresponse] = await this.client.createDataset(id, options);
        return dataset;
    }

    async doesDatasetExist(id: string) {
        const dataset = await this.getDataset(id);
        const [result] = await dataset.exists();
        return result;
    }

    async getDatasets(options: GetDatasetsOptions) {
        return this.client.getDatasets(options);
    }

    async getAllDatasets() {
        const params = {
            autoPaginate: true,
            all: true
        };
        return this.client.getDatasets(params);
    }

    async getDataset(datasetId: string, options?: GetDatasetOptions) {
        const dataset = this.client.dataset(datasetId);
        const [exists] = await dataset.exists();

        if (!exists && options?.createDataset === true) {
            this.log(`getDataset`, `dataset already exists, creating`);
            await this.createDataset(datasetId);
        } else if (!exists) {
            this.log(`getDataset`, `dataset already exists, throwing error`);
            throw new BigQueryClientError(`Dataset ${datasetId} does not exist.`);
        }

        return dataset;
    }

    async getDatasetTables(id: string, options?: IGetDatasetTablesOpt) {
        const dataset = this.client.dataset(id);
        const [tables] = await dataset.getTables();
        let tablesMd = tables as TableCustomEmp[];

        if (options?.fetchOnlyViews) {
            tablesMd = tablesMd.filter(t => t.metadata.type === "VIEW");
        }

        const tableEnr = await Promise.all(
            tablesMd.map(async table => ({
                table,
                tableMetadata: (await table.getMetadata())[0] as TableMetadata
            }))
        );

        return tableEnr as TableEnriched[];
    }

    /**
     * Creates a new view and returns view.
     * 
     * If view exists and `options.ignoreIfViewExists` is set to true, the existing 
     * view will be returned. If false, an error is thrown. 
     * 
     * @param datasetId 
     * @param viewId 
     * @param view 
     * @param options 
     * @returns 
     */
    async createView(
        datasetId: string,
        viewId: string,
        view: ViewDefinition,
        options?: CreateViewOptions
    ) {
        /* Fetch the dataset referece. Create if doesn't exist. */
        const dataset = await this.getDataset(datasetId, options);

        /* Create the new view. */
        try {
            const [newView] = await dataset.createTable(
                viewId,
                { view }
            );
            this.log(`createView`, `done`);
            return newView;
        } catch (error) {
            if (error.errors[0].reason === "duplicate" && options?.ignoreIfViewExists) {
                this.log(`createView`, `view ${viewId} already exists in dataset ${datasetId}`);
                return dataset.table(viewId);
            }

            throw new CreateViewError(error, datasetId, viewId, view, options);
        }
    }

    /**
     * Deletes a table/view from a dataset. 
     * 
     * @param datasetId 
     * @param tableId
     */
    async deleteTable(
        datasetId: string,
        tableId: string
    ) {
        this.log(`deleteTable`, `datasetId: ${datasetId}, tableId: ${tableId}`);
        const dataset = this.client.dataset(datasetId);
        const table = dataset.table(tableId)
        const [res] = await table.delete();
        this.log(`deleteTable`, `done`);
        return res;
    }

    /**
     * Deletes a dataset
     * 
     * @param datasetId the dataset name
     * @param options Use `force: true` to delete all tables and views as well in the dataset
     */
    async deleteDataset(datasetId: string, options?: DatasetDeleteOptions): Promise<void> {
        this.log("deleteDataset", `datasetId: ${datasetId}`);
        const dataset = this.client.dataset(datasetId);
        await dataset.delete(options);
        this.log(`deleteDataset`, `done`);
    }

    /* Internal helper functions. */

    /**
     * Helper function to validate schema before sending to BigQuery. 
     * 
     * @param rows rows that will be inserted
     * @param schema the schema for the rows that will be inserted
     */
    private validateSchema = (rows: Array<{}>, schema: Array<IBigQuerySchema>) => {
        const dataKeys = [
            ...new Set(rows.map((m) => Object.keys(m))
                .reduce((acc, keys) => [...acc, ...keys], []))];
        dataKeys.forEach((d) => {
            let isFieldDefined = false;
            for (const s of schema) {
                if (s.name === d) {
                    isFieldDefined = true;
                }
            }

            if (!isFieldDefined) {
                throw Error(`Missing schema for field ${d}`);
            }
        });
    }

    /**
     * Log if verbose logging is set to true
     * 
     * @param message string message to log.
     */
    private log = (method: string, message: string, logLevel?: VerboseLoggingLevel) => {
        classLog("BigQueryClient", method, message, logLevel, this.options);
    }
}

