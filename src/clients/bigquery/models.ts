import { Table, TableMetadata } from "@google-cloud/bigquery";

/**
 * Replace Google's interface to credentials details.
 */
export interface BigQueryCredentials {
    client_email: string;
    private_key: string;
}

export interface IEmploysureInsertOpts {
    /* Enable to add logging metadata for tables used for logging. */
    useLogProfile?: boolean;
    /* Don't look for and update timestamps. */
    disableTimestampPatching?: boolean;
    /* Chunk size of records send to BigQuery API */
    chunkSize?: number;
}
export type IBigQuerySchemaType = 'STRING' | 'INTEGER' | 'FLOAT' | 'BOOLEAN' | 'TIMESTAMP' | 'DATE' | 'TIME' | 'RECORD';

export interface IBigQuerySchema {
    name: string;
    type: IBigQuerySchemaType;
    mode?: "NULLABLE" | "REPEATED";
    fields?: Array<IBigQuerySchema>
}


/* Extension of Google's type to include metadata of a table. */
export type TableCustomEmp = Omit<Table, 'metadata'> & { metadata: TableMetadataSummary };
export type TableMetadataSummary = {
    creationTime: string;
    id: string;
    kind: 'bigquery#table' | string;
    tableReference: TableReference;
    type: 'VIEW' | 'TABLE' | string;
}
export type TableReference = {
    projectId: string;
    datasetId: string;
    tableId: string;
}

export interface IGetDatasetTablesOpt {
    fetchOnlyViews: boolean;
}

export type TableEnriched = {
    table: TableCustomEmp,
    tableMetadata: TableMetadata
}

export type CreateViewOptions = {
    /* Default: false. Creates dataset for view if it doesn't exist*/
    createDataset?: boolean;
    ignoreIfViewExists?: boolean
}

export type GetDatasetOptions = {
    createDataset?: boolean
}