import { ViewDefinition } from "@google-cloud/bigquery";
import { CreateViewOptions } from "../models";

export class CreateViewError extends Error {
    name = "CreateViewError";
    bigQueryError: any;
    datasetId: string;
    viewId: string
    viewDef: ViewDefinition;
    options?: CreateViewOptions;

    constructor(
        error: Error,
        datasetId: string,
        viewId: string,
        viewDef: ViewDefinition,
        options?: CreateViewOptions
    ) {
        super(error.message);
        this.datasetId = datasetId;
        this.viewId = viewId;
        this.viewDef = viewDef;
        this.options = options;

    }
}