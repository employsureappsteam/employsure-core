/**
 * Errors that occur in Employsure's BigQueryClient
 */
export class BigQueryClientError extends Error {
    name = "BigQueryClientError";
    constructor(message: string) {
        super(message);
    }
}