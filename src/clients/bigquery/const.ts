export const salesforceTimestamp = new RegExp(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}\+\d+$/)
export const salesforceTime = new RegExp(/^(\d{2}):(\d{2}):(\d{2})\.(\d+)Z$/)

/* How many rows to send to bigquery at a time if not specified. */
export const DEFAULT_INSERT_CHUNK_SIZE = 50;