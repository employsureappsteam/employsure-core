export const ZOOM_API_BASE_URL = 'https://api.zoom.us/v2';

/* https://github.com/zeit/ms */
export const ZOOM_JWT_EXPIRY = '1h';