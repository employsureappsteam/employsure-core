import { LogToRollbarEvent, RollbarClientOptions } from './models';
import Rollbar from 'rollbar';
import { GetSecretParams } from '../../lib';
import { EmploysureError } from '../../models/errors';

export class RollbarClient {
    options: RollbarClientOptions;
    client: Rollbar;


    constructor(options?: RollbarClientOptions) {
        /* Prod = 'production', any other valid string = 'development', not set = 'unknown'*/
        let envStage: string = 'unknown';
        if (process.env.stage === "Prod") {
            envStage = "production";
        } else if (process.env.stage) {
            envStage = "development";
        }

        const defaultOptions = {
            projectName: "AwsEmploysure",
            /* Assume 'unknown' if it's not in the stage and not specified. */
            environment: options?.environment ?? envStage,
        }
        this.options = {
            ...defaultOptions,
            ...options,
        }

        if (!this.options.projectName && !this.options.access_token) {
            throw new EmploysureError(`InvalidRollbarOptions`, `Must give project name or access token.`);
        } else if (this.options.access_token) {
            const accessToken = this.options.access_token;
            this.client = new Rollbar({ accessToken });
        }


    }

    private async initClient() {
        if (!this.client && this.options.projectName) {
            const accessToken = await this.getRollbarAccessToken(this.options.projectName);
            this.client = new Rollbar({
                accessToken,
                environment: this.options.environment
            });
        }
    }

    private async getRollbarAccessToken(projectName: string) {
        /* Fetch from SSM. */
        const accessTokenPath = `/Rollbar${projectName}/Prod/AccessToken`;
        const params = await GetSecretParams([accessTokenPath]);
        return params[accessTokenPath];
    }


    async log(event: LogToRollbarEvent): Promise<string> {
        await this.initClient();
        /* Wrap in promise as log function only has callback. */
        return new Promise((resolve, reject) => {
            /* Assume severity of info */
            const level = event?.severity ?? 'info';

            /* Note: order does not matter, however the first Object that contains at least one key from the list under request will be considered a request object. */
            const { uuid } = this.client.log(
                event.message,
                event.error,
                {
                    level,
                    emp: event.custom
                },
                (err) => err ? reject(err) : resolve(uuid)
            );
        })
    }
}