export type RollbarProjectName = 'ApiIntegrations'

export type RollbarClientOptions = {
    /* Name of the project to use. Should have an SSM parameter mapped to /Rollbar${projectName}/Prod/AccessToken*/
    projectName?: string;
    /* Optionally give the access token directly. */
    access_token?: string;
    /* Which environment for. */
    environment?: string;
}

export type RollbarSeverity = 'info' | 'debug' | 'warning' | 'error' | 'critical';
export type LogToRollbarEvent = {
    projectName?: string;
    message: string;
    severity?: RollbarSeverity;
    environment?: string;
    error?: Error;
    custom?: {}
}