import { IClientOptions } from "../../../helpers/clients/models";

export interface SalesforceConfig extends IClientOptions {
    clientId: string;
    clientSecret: string;
    username: string;
    password: string;
    environment?: 'production' | 'sandbox';
    oAuthHost?: "https://login.salesforce.com" | "https://test.salesforce.com" | string;
    redirectUri?: string;
    apiVersion?: string;
    defaultRecordTypeIds?: any;
}

export interface SalesforceObjectAttributes {
    type: 'string',
    url: 'string'
}

export interface SalesforceQueryResultRecord {
    attributes: SalesforceObjectAttributes,
    [x: string]: null | string | number | boolean | Array<SalesforceQueryResult> | SalesforceObjectAttributes
}

export interface SalesforceQueryResult<T = SalesforceQueryResultRecord> {
    done: boolean;
    nextRecordsUrl?: string;
    totalSize: number;
    records: Array<T>
}


export interface ISfQueryOptions {
    toJson?: boolean,
    /* Page through all records fetching all records. */
    fetchAll?: boolean,
    /* Page through children objects (lookups to master) fetching all records. */
    fetchAllChildren?: boolean
}

export interface SalesforceClientConnection {
    oAuthHost: string;
    environment: string;
    clientId: string;
    clientSecret: string;
    redirectUri: string;
    apiVersion: string;
    mode: string;
    authEndpoint: string;
    testAuthEndpoint: string;
    loginUri: string;
    testLoginUri: string;
    gzip: boolean;
    autoRefresh: boolean;
    timeout: null;
    oauth?: SalesforceClientOauth;
    username: string;
    password: string;
}

export interface SalesforceClientOauth {
    access_token: string;
    instance_url: string;
    id: string;
    token_type: string;
    issued_at: string;
    signature: string;
}
