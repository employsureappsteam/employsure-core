export interface SobjectDescribe {
    actionOverrides: any[];
    activateable: boolean;
    associateEntityType: null;
    associateParentEntity: null;
    childRelationships: ChildRelationship[];
    compactLayoutable: boolean;
    createable: boolean;
    custom: boolean;
    customSetting: boolean;
    deepCloneable: boolean;
    defaultImplementation: null;
    deletable: boolean;
    deprecatedAndHidden: boolean;
    extendedBy: null;
    extendsInterfaces: null;
    feedEnabled: boolean;
    fields: Field[];
    hasSubtypes: boolean;
    implementedBy: null;
    implementsInterfaces: null;
    isInterface: boolean;
    isSubtype: boolean;
    keyPrefix: string;
    label: string;
    labelPlural: string;
    layoutable: boolean;
    listviewable: null;
    lookupLayoutable: null;
    mergeable: boolean;
    mruEnabled: boolean;
    name: string;
    namedLayoutInfos: any[];
    networkScopeFieldName: null;
    queryable: boolean;
    recordTypeInfos: RecordTypeInfo[];
    replicateable: boolean;
    retrieveable: boolean;
    searchLayoutable: boolean;
    searchable: boolean;
    sobjectDescribeOption: string;
    supportedScopes: SupportedScope[];
    triggerable: boolean;
    undeletable: boolean;
    updateable: boolean;
    urls: DescribeUrls;
}

export interface ChildRelationship {
    cascadeDelete: boolean;
    childSObject: string;
    deprecatedAndHidden: boolean;
    field: string;
    junctionIdListNames: any[];
    junctionReferenceTo: any[];
    relationshipName: null | string;
    restrictedDelete: boolean;
}

export interface Field {
    aggregatable: boolean;
    aiPredictionField: boolean;
    autoNumber: boolean;
    byteLength: number;
    calculated: boolean;
    calculatedFormula: null;
    cascadeDelete: boolean;
    caseSensitive: boolean;
    compoundFieldName: null;
    controllerName: null;
    createable: boolean;
    custom: boolean;
    defaultValue: boolean | null;
    defaultValueFormula: null;
    defaultedOnCreate: boolean;
    dependentPicklist: boolean;
    deprecatedAndHidden: boolean;
    digits: number;
    displayLocationInDecimal: boolean;
    encrypted: boolean;
    externalId: boolean;
    extraTypeInfo: null;
    filterable: boolean;
    filteredLookupInfo: null;
    formulaTreatNullNumberAsZero: boolean;
    groupable: boolean;
    highScaleNumber: boolean;
    htmlFormatted: boolean;
    idLookup: boolean;
    inlineHelpText: null;
    label: string;
    length: number;
    mask: null;
    maskType: null;
    name: string;
    nameField: boolean;
    namePointing: boolean;
    nillable: boolean;
    permissionable: boolean;
    picklistValues: any[];
    polymorphicForeignKey: boolean;
    precision: number;
    queryByDistance: boolean;
    referenceTargetField: null;
    referenceTo: string[];
    relationshipName: null | string;
    relationshipOrder: null;
    restrictedDelete: boolean;
    restrictedPicklist: boolean;
    scale: number;
    searchPrefilterable: boolean;
    soapType: string;
    sortable: boolean;
    type: string;
    unique: boolean;
    updateable: boolean;
    writeRequiresMasterRead: boolean;
}

export interface RecordTypeInfo {
    active: boolean;
    available: boolean;
    defaultRecordTypeMapping: boolean;
    developerName: string;
    master: boolean;
    name: string;
    recordTypeId: string;
    urls: RecordTypeInfoUrls;
}

export interface RecordTypeInfoUrls {
    layout: string;
}

export interface SupportedScope {
    label: string;
    name: string;
}

export interface DescribeUrls {
    compactLayouts: string;
    rowTemplate: string;
    approvalLayouts: string;
    uiDetailTemplate: string;
    uiEditTemplate: string;
    defaultValues: string;
    describe: string;
    uiNewRecord: string;
    quickActions: string;
    layouts: string;
    sobject: string;
}
