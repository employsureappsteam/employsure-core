/* An error raised by enforce. */
export class NforceError extends Error {
    name: string;
    errorCode: string;
    isOperational: boolean;
    message: string;
    stack: string;
    statusCode: number;
    constructor(name: string) {
        super();
        this.name = name;
    }
}