
/* Salesforce objects */
export type SObjectType = string;
export interface ApexOptions {
    uri: string;
    method: 'GET' | 'POST' | 'PUT' | 'PATCH';
    urlParams?: {} | string;
    body?: {};
}

/**
 * Response from calling /composite/sobject
 */
export type CompositeRequestBody = {
    allOrNone: boolean;
    records: sObject[];
}
export type CompositeResponseBody = Array<CompositeResponseRecord>
export type CompositeResponseRecord = {
    id: string;
    success: boolean;
    errors: CompositeResponseErrors;
}
export type CompositeResponseErrors = Array<CompositeResponseError>;
export type CompositeResponseError = {
    statusCode?: string;
    message?: string;
    fields?: Array<string>
}
export type sObjectValue = any;

export class sObject {
    constructor(type: SObjectType) {
        this.attributes = { type };
    }
    attributes: Attributes;
    id?: string;
    [field: string]: sObjectValue;
}

export interface Attributes {
    type: SObjectType;
}

export type CompositeRecordList = Array<CompositeRecord>
export type CompositeRecord = {
    attributes: Attributes;
    [field: string]: sObjectValue;
}
export type CompositeRecordCrudOptions = { maxParallel?: number, chunkSize?: number }
