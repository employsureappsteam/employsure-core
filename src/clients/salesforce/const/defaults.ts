export const defaults = {
    query: {
        TO_JSON: true,
        FETCH_ALL: true,
        FETCH_ALL_CHILDREN: true,
    }
}