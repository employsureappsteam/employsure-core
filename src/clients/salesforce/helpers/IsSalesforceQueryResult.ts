import { SalesforceQueryResult } from "../models";

export function IsSalesforceQueryResult(value: SalesforceQueryResult): value is SalesforceQueryResult {
    return value !== undefined
        && value !== null
        && typeof value.done === "boolean"
        && typeof value.totalSize === "number"
        && Array.isArray(value.records);
}