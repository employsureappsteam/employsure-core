import { ApexOptions, SObjectType, sObject, CompositeRecordCrudOptions, CompositeRecordList, CompositeRequestBody, CompositeResponseBody } from './models/salesforce';
import { SobjectDescribe } from './models/data/describe';
import { SalesforceConfig, SalesforceClientConnection, SalesforceQueryResult, ISfQueryOptions, SalesforceClientOauth } from './models';
import { IsSalesforceQueryResult } from './helpers/IsSalesforceQueryResult'
import { ResponseStatus } from '../../constants/salesforce';
import { defaults } from './const/defaults';
import { NforceError } from './models/errors/nforce';
import Axios, { AxiosInstance } from 'axios';
import { fetchDistributedSalesforceOauth, IDistributedSalesforceOauthRecord } from './helpers/distributedOauth';
import { classLog } from '../../helpers/clients/classLog';
import { VerboseLoggingLevel } from '../../helpers/clients/models';
import { sleep } from '../../helpers/sleep';
import promiseMapLimit = require("promise-map-limit");
import _ from 'lodash';
var nforce = require('nforce');

const RETRY_ATTEMPTS = 2;
const INVALID_DATA_ERROR_CODES = [
    'STRING_TOO_LONG',
    'INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST',
    'REQUIRED_FIELD_MISSING',
    'INVALID_EMAIL_ADDRESS',
    'FIELD_CUSTOM_VALIDATION_EXCEPTION',
];

/**
 * REST API client for Salesforce.
 * 
 * See official REST API Developer guide here: 
 * https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/quickstart.htm
 * 
 * @remarks
 * Development notes:
 * This client originally used nforce (salesforce wrapper) only. To accomodate 
 * for missing features, there is an internal axios client also used. The nforce
 * client is kept for backwards compatability. The alternative methods using 
 * axios are suffixed with a 2.
 * 
 * e.g. `query()` uses nforce client while `query2()` uses axios client.
 * 
 * The nforce client is the private field `connection`.
 * The axios client is the private field `axios`.
 */
export class SalesforceClient {
    private userConfig: SalesforceConfig;
    private connection: SalesforceClientConnection & any;
    private axios: AxiosInstance;

    constructor(config: SalesforceConfig) {
        this.userConfig = config;
    }

    /**
     * Authenticates with salesforce
     */
    async authenticateClient() {
        /* Try and fetch cached oauth from SSM. */
        let distributedOauth: IDistributedSalesforceOauthRecord | undefined = undefined;
        this.log(`authenticateClient`, `username: ${this.userConfig.username}, clientId: ${this.userConfig.clientId}`);
        if (this.userConfig.disableDistributedAuth !== true) {
            this.log(`authenticateClient`, `using distributed auth`);
            distributedOauth = await fetchDistributedSalesforceOauth(this.userConfig);
        }

        /* Use settings from user config and fallback on environment stage if necessary. */
        const environment = this.userConfig.environment?.toLowerCase() ??
            (process.env.stage === 'Prod' ? 'production' : 'sandbox');
        const oAuthHost = this.userConfig.oAuthHost ??
            (process.env.stage === 'Prod' ? 'https://login.salesforce.com' : 'https://test.salesforce.com');

        /* Create the nforce client. */
        this.connection = nforce.createConnection({
            oAuthHost,
            environment,
            clientId: this.userConfig.clientId,
            clientSecret: this.userConfig.clientSecret,
            redirectUri: this.userConfig.redirectUri || 'http://localhost:3000/oauth/_callback',
            apiVersion: this.userConfig.apiVersion || 'v51.0',
            mode: 'single',
            oauth: distributedOauth?.oauth
        });

        /* Authenticate if the oauth token is not set. */
        if (!this.connection.oauth) {
            this.log(`authenticateClient`, `sending authenticate request to salesforce`);
            try {
                /* Returns oauth object, */
                const oauth = await this.connection.authenticate({
                    username: this.userConfig.username,
                    password: this.userConfig.password,
                });

                /* Note: authenticate _sometimes_ does not save this, hence doing manually outside of nforce library */
                this.connection.oauth = oauth;

                this.log(`authenticateClient`, `authenticate done`);
            } catch (error) {
                this.logError(`authenticateClient`, `authenticate failed: ${error}`);
                throw error;
            }
        } else {
            this.log(`authenticateClient`, `skipping authenticate request to salesforce`);
        }

        /* If the distributed salesforce oauth service requested an update, run the callback */
        const newOauth = this.connection.oauth;
        if (distributedOauth?.updateOauthCache && newOauth !== undefined) {
            this.log(`authenticateClient`, `updating distrbuted oauth`);
            distributedOauth?.updateOauthCache(newOauth);
        }

        /* Create the axios client using acces_token from oauth. */
        this.axios = Axios.create({
            headers: {
                'Authorization': `Bearer ${this.connection.oauth.access_token}`
            },
            baseURL: this.connection.oauth.instance_url
        });
    }

    /**
     * Confirm that the connection has been made already
     */
    async checkClientIsAuthenticated(): Promise<void> {
        if (!this.connection?.oauth) {
            await this.authenticateClient();
        }
    }

    async getInstanceUrl() {
        await this.checkClientIsAuthenticated();
        return this.connection.oauth.instance_url;
    }

    /**
     * Gets the picklist labels and values for a partocular sObject field
     * @param {string} sObjectName Salesforce Object name
     * @param {string} fieldName Salesforce Object field name
     */
    async getPicklistValues(sObjectName: string, fieldName: string) {
        if (sObjectName && fieldName && this.userConfig) {
            const apiVersion = this.userConfig.apiVersion || 'v42.0';
            const recordTypeId =
                (this.userConfig.defaultRecordTypeIds || {})[sObjectName.toLowerCase()] || '012000000000000AAA';
            if (recordTypeId) {
                try {
                    const res = await this.connection.getUrl(
                        `/services/data/${apiVersion}/ui-api/object-info/${sObjectName}/picklist-values/${recordTypeId}/${fieldName}`
                    );
                    if (res && res.values && res.values.length) {
                        return res.values;
                    }
                } catch (error) {
                    this.logError('getPicklistValues', `${error}`);
                    return null;
                }
            }
        }
        return null;
    }


    /**
     * Running into bugs (seemingly) in nforce. 
     */
    public get2 = async<T>(url: string) => {
        if (!url.startsWith('https')) {
            url = `${this.connection.oauth.instance_url}/${url}`;
        }
        const response = await this.axios.get<T>(url);
        return response.data;
    }

    /* Improved query function. */
    public query2 = async<T = {}>(queryStr: string) => {
        await this.checkClientIsAuthenticated();
        this.log('query2', queryStr);
        const queryStrEnc = encodeURIComponent(queryStr);
        const { instance_url } = this.connection.oauth;
        const { apiVersion } = this.connection;
        const url = `${instance_url}/services/data/${apiVersion}/query/?q=${queryStrEnc}`

        /* Fetch results*/
        const data = await this.get2<SalesforceQueryResult<T>>(url);
        await this.pageQueryResult(data);

        /* Fetch all other pages. */
        while (data.nextRecordsUrl !== undefined) {
            this.log(`query2`, `${data.records.length} / ${data.totalSize}`);
            const nextResponse = await this.get2<SalesforceQueryResult<T>>(data.nextRecordsUrl);
            await this.pageQueryResult(nextResponse);

            data.nextRecordsUrl = nextResponse.nextRecordsUrl;
            data.done = nextResponse.done;
            data.records.push(...nextResponse.records);
        }

        const d2 = this.cleanSoqlQueryResults(data);
        return d2;
    }

    private pageQueryResult = async<T>(data: SalesforceQueryResult<T>) => {
        for (const r of data.records) {
            for (const [att, val] of Object.entries(r)) {
                if (IsSalesforceQueryResult(val)) {
                    while (val.nextRecordsUrl !== undefined) {
                        this.log(`pageQueryResult`, `${att} - ${val.records.length} / ${val.totalSize}`);
                        const nextResponse = await this.get2<SalesforceQueryResult>(val.nextRecordsUrl);
                        val.nextRecordsUrl = nextResponse.nextRecordsUrl;
                        val.done = nextResponse.done;
                        val.records.push(...nextResponse.records);
                    }
                }
            }
        }
    }

    /**
     * Executes a SOQL query against the Salesforce connection
     * @param {string} soqlQuery SOQL Query string
     * @param {Object} options { toJson: }
     */
    async query(soqlQuery: string, options?: ISfQueryOptions): Promise<Array<sObject>> {
        await this.checkClientIsAuthenticated();
        this.log('query', soqlQuery);
        let retries = RETRY_ATTEMPTS;
        let error;
        while (retries > 0) {
            retries -= 1;
            error = null;
            try {
                const result = await this.connection.query({
                    query: soqlQuery,
                    fetchAll: options?.fetchAll ?? defaults.query.FETCH_ALL
                });
                if (!result || !result.records) {
                    throw new Error('Error querying Salesforce');
                }
                if (!result.records.length) {
                    return [];
                }

                if (options?.toJson ?? defaults.query.TO_JSON) {
                    return result.records.map(record => record.toJSON());
                } else {
                    return result.records;
                }
            } catch (err) {
                this.logError('query', `${err}`);
                error = Object.assign(err, new NforceError(`SalesforceQueryError`));
                await this.authenticateClient();
            }
        }
        throw error;
    }

    /**
     * Gets a single record from Salesforce given the object type, id and fields
     * @param {string} type Salesforce SObject name
     * @param {string} id Salesforce record id
     * @param {Array<string>} fields Fields to return
     */
    async getRecord(type: string, id: string, fields: string[]): Promise<any> {
        await this.checkClientIsAuthenticated();
        let retries = RETRY_ATTEMPTS;
        let error;
        while (retries > 0) {
            retries -= 1;
            try {
                return await this.connection.getRecord({ type, id, fields });
            } catch (err) {
                this.logError('getRecord', `${err}`);
                error = Object.assign(err, new NforceError(`SalesforceGet${type}RecordError`));
                await this.authenticateClient();
            }
        }
        throw error;
    }

    /**
     * Creates a Salesforce sobject
     *
     * POST /services/data/v44.0/sobjects/<sobjectType>/
     *
     * @param object
     * @param sobjectType
     * @returns {Promise.<string>} id or null if unsuccessful
     */
    async createRecord(
        object: {},
        sobjectType: SObjectType
    ): Promise<{
        success: boolean;
        status: string;
        message: string;
        id?: string;
    }> {
        await this.checkClientIsAuthenticated();
        let retries = RETRY_ATTEMPTS;
        let error;
        while (retries > 0) {
            retries -= 1;
            error = null;
            try {
                const sobject = nforce.createSObject(sobjectType);
                Object.keys(object).forEach((key) => {
                    sobject.set(key, object[key]);
                });
                const result = await this.connection.insert({ sobject });
                return {
                    success: true,
                    status: ResponseStatus.CREATED,
                    message: `Created ${sobjectType} record: ${result.id}` as string,
                    id: result.id as string,
                };
            } catch (err) {
                this.logError('createRecord', `${err}`);
                if (INVALID_DATA_ERROR_CODES.includes(err.errorCode)) {
                    return {
                        success: false,
                        status: ResponseStatus.INVALID,
                        message: err.message,
                    };
                } else {
                    error = Object.assign(err, new NforceError(`SalesforceCreate${sobjectType}RecordError`));
                    await this.authenticateClient();
                }
            }
            this.log('createRecord', `retries left ${retries}`);
            sleep(1000);
        }
        throw error;
    }

    /**
     * Update a Salesforce sobject
     * @param {Object} sobject object to update
     */
    async updateRecord(object: {}, sobjectType: SObjectType): Promise<string> {
        await this.checkClientIsAuthenticated();
        let retries = RETRY_ATTEMPTS;
        let error;
        while (retries > 0) {
            retries -= 1;
            error = null;
            try {
                const sobject = nforce.createSObject(sobjectType);
                Object.keys(object).forEach((key) => {
                    sobject.set(key, object[key]);
                });
                const record = await this.connection.update({ sobject });
                return record === '' ? 'Update successful' : 'Updated failed';
            } catch (err) {
                this.logError('updateRecord', `${err}`);
                error = Object.assign(err, new NforceError(`SalesforceUpdateRecordError`));
                await this.authenticateClient();
            }
        }
        throw error;
    }

    async deleteRecords(
        ids: Array<string>,
        options?: { maxParallel?: number, chunkSize?: number }
    ) {
        await this.checkClientIsAuthenticated();
        const chunkSize = Math.max(options?.chunkSize ?? 5, 200);
        const idChunks = _.chunk(ids, chunkSize);
        const allResults: CompositeResponseBody = [];
        for (const idsChunk of idChunks) {
            const uriParam = `ids=${idsChunk.join(',')}&allOrNone=false`;
            const uri = `/services/data/v46.0/composite/sobjects?${uriParam}`;
            const response = await this.axios.delete<CompositeResponseBody>(uri);
            allResults.push(...response.data)
        }
        return allResults;
    }

    /** 
     * Simple wrapper around composite rest API for inserting records. 
     */
    async insertRecords(
        records: CompositeRecordList,
        options?: { maxParallel?: number, chunkSize?: number }
    ) {
        await this.checkClientIsAuthenticated();

        const maxParallel = options?.maxParallel ?? 3;
        const chunkSize = Math.max(options?.chunkSize ?? 5, 200);

        const recordChunks = _.chunk(records, chunkSize);

        const results = await promiseMapLimit(
            recordChunks,
            maxParallel,
            (records) => {
                return this.rest<CompositeRequestBody, CompositeResponseBody>(
                    "POST",
                    "/services/data/v46.0/composite/sobjects",
                    {
                        allOrNone: false,
                        records
                    }
                )
            }
        );

        return results.reduce((acc, res) => [...acc, ...res], []);
    }


    /** 
     * Simple wrapper around composite rest API for inserting records. 
     */
    async updateRecords(
        records: CompositeRecordList,
        options?: CompositeRecordCrudOptions
    ) {
        await this.checkClientIsAuthenticated();

        const maxParallel = options?.maxParallel ?? 3;
        const chunkSize = Math.max(options?.chunkSize ?? 5, 200);

        const recordChunks = _.chunk(records, chunkSize);

        const results = await promiseMapLimit(
            recordChunks,
            maxParallel,
            (records) => {
                return this.rest<CompositeRequestBody, CompositeResponseBody>(
                    "PATCH",
                    "/services/data/v46.0/composite/sobjects",
                    {
                        allOrNone: false,
                        records
                    }
                )
            }
        );

        return results.reduce((acc, res) => [...acc, ...res], []);
    }
    /**
     * Scans an object for any subquery objects and flattens them
     * @param {Object} object Object to scan
     * @param {Object} aliases Object that contains aliases as key value pairs
     */
    cleanSubqueries(object, aliases = {}) {
        const newObject = JSON.parse(JSON.stringify(object));
        const keys = Object.keys(object);
        if (keys && keys.length) {
            keys.forEach((key) => {
                const newKey = aliases[key] || key;
                if (object[key] && object[key].totalSize) {
                    newObject[newKey] = this.flattenSubquery(object[key]);
                } else {
                    newObject[newKey] = object[key];
                }
                if (newKey !== key) {
                    delete newObject[key];
                }
            });
        }
        return newObject;
    }

    /**
     * Flattens a subquery object into its field values.
     * Lower cases keys
     * @param {Object} object Subquery Object to flatten
     */
    flattenSubquery(object) {
        let keys;
        let lowerKeys;
        if (!object || !object.records || !object.records.length) {
            return [];
        }
        return object.records.map((record) => {
            const newRecord = {};
            if (!keys) {
                keys = Object.keys(record).filter((key) => key !== 'attributes');
                lowerKeys = keys.map((key) => key.toLowerCase());
            }
            if (keys && keys.length) {
                for (let i = 0; i < keys.length; i += 1) {
                    newRecord[lowerKeys[i]] = record[keys[i]];
                }
            }
            return newRecord;
        });
    }

    /**
     * Lower cases an object's keys and removes attributes field
     * @param {Object} object Subquery Object to flatten
     */
    lowerCaseObjectKeys(object) {
        if (!object) {
            return null;
        }
        const newObject = {};
        const keys = Object.keys(object).filter((key) => key !== 'attributes');
        const lowerKeys = keys.map((key) => key.toLowerCase());
        if (keys && keys.length) {
            for (let i = 0; i < keys.length; i += 1) {
                newObject[lowerKeys[i]] = object[keys[i]];
            }
        }
        return newObject;
    }

    /**
     * Typed wrapper around nforce.
     **/
    async apexRest<T>(opts: ApexOptions): Promise<T> {
        await this.checkClientIsAuthenticated();
        return this.connection.apexRest({
            oauth: this.connection.oauth,
            uri: opts.uri,
            method: opts.method,
            // urlParams: opts.urlParams,
            body: JSON.stringify(opts.body),
        });
    }

    /* Extending nforce private functions. YMMV : ) */
    async rest<Req, Resp>(method: string, url: string, data?: Req): Promise<Resp> {
        await this.checkClientIsAuthenticated();
        const opt = {
            oauth: this.connection.oauth,
            method,
            uri: this.connection.oauth.instance_url + url
        };
        if (data !== undefined) {
            opt["body"] = JSON.stringify(data);
        }
        return this.connection._apiRequest(opt);
    }

    async sObjectDescribe(sObject: string): Promise<SobjectDescribe> {
        await this.checkClientIsAuthenticated();
        const data = await this.rest(
            "GET",
            `/services/data/v51.0/sobjects/${sObject}/describe`
        );

        return data as SobjectDescribe;
    }

    /**
     * Checks whether or not an input is sanitised to be an id
     * @param {String} input id string
     */
    isIdSanitised(input) {
        return input && input.search(/[^A-z0-9]/) === -1;
    }

    /**
     * Checks whether or not an input is sanitised to be an email
     * @param {*} input email string
     */
    isEmailSanitised(input) {
        return input && input.search(/[^A-z0-9.@!#$%&'*+\-/=?^_`{|}~]/) === -1;
    }

    /* Returns the ID of a recordType for a given sObject. */
    async getRecordTypeId(recordType: string, sObjectType: SObjectType) {
        await this.checkClientIsAuthenticated();
        const query = `SELECT Id FROM RecordType WHERE Name = '${recordType}' AND SobjectType = '${sObjectType}'`;
        const query_res = await this.query(query);
        const recordTypeId = query_res[0].get('id');
        return recordTypeId;
    }

    private cleanSoqlQueryResults = <T>(result: SalesforceQueryResult<T>): Array<T> => {
        return result.records
            .map(rec => {
                const childRecordKeys: string[] = []
                const removeKeys: string[] = []
                for (const [key, val] of Object.entries(rec)) {
                    if (IsSalesforceQueryResult(val)) {
                        childRecordKeys.push(key)
                    }
                    if (key === "attributes") {
                        removeKeys.push(key)
                    }
                }

                for (const key of childRecordKeys) {
                    rec[key] = this.cleanSoqlQueryResults(rec[key] as SalesforceQueryResult);
                }

                for (const key of removeKeys) {
                    delete rec[key];
                }
                return rec;
            });
    }

    /**
     * 
     * @param method name of method this is called from
     * @param message the message
     * @param logLevel the severity of message
     */
    private log = (method: string, message: string, logLevel?: VerboseLoggingLevel) => {
        classLog('SalesforceClient', method, message, logLevel, this.userConfig);
    }

    private logWarning = (method: string, message: string) => this.log(method, message, 'warn');
    private logError = (method: string, message: string) => this.log(method, message, 'error');
}

