export type SnsTopicNames = 'FlareHrDataErrors' | 
    'EngErrorsDev' | 
    'EngErrorsProd' |
    'ProvisioningWorkplace' | 
    'PingboardSync' |
    'EmploysureApiError' |
    'EmploysureLeadApiWarnings' |
    'EmploysureLeadApiCampaignWarnings'

export type TopicArns =
    'arn:aws:sns:ap-southeast-2:711143483997:flarehr-data-errors' |
    'arn:aws:sns:ap-southeast-2:711143483997:engineering-errors-dev' |
    'arn:aws:sns:ap-southeast-2:711143483997:engineering-errors-prod' | 
    'arn:aws:sns:ap-southeast-2:711143483997:provisioning-workplace' | 
    'arn:aws:sns:ap-southeast-2:711143483997:pingboard-sync' | 
    'arn:aws:sns:ap-southeast-2:711143483997:employsure-api-errors' | 
    'arn:aws:sns:ap-southeast-2:711143483997:employsure-lead-api-warnings' | 
    'arn:aws:sns:ap-southeast-2:711143483997:employsure-lead-api-campaign-warnings'

export type SnsMessage = { 
    title: string, 
    message: string, 
    details?: string 
}

export interface IPublishMessage {
    /* Default for all protocols. */
    default: string,
    /* To send to subscribed lambdas. */
    lambda: string,
    /* To send to subscribed emails. */
    email: string
};

export interface ISnsGlobalConfig {
    /* Flag used to supress all notifications. */
    supressAllNotifications: boolean
}