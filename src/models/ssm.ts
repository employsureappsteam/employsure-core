/* Parameters mapped to Path -> Value .*/
export type ParametersMap = { [path: string]: string }

export class InvalidParameterError extends Error {
    name = "InvalidParameterError";
    constructor(parameters: Array<string>) {
        super(`These ${parameters.length} parameters are invalid: ${parameters.join(", ")}. Please check they exist.`);
    }
}
