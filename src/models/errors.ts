/** A generic error to use for Employsure application errors. 
 * All errors using this class should be server errors. */
export class EmploysureError extends Error {
    isEmploysureError = true;
    constructor(errorName: string, message: string) {
        super(message);
        this.name = `EmploysureError_${errorName}`;
    }
}