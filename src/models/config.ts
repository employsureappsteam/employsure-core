import { DynamoDB } from "aws-sdk";

/* Matches 1-to-1 to IConfig*/
export type ConfigFields = 'WorkplaceAccessToken' |
    'FirestoreCertificate' |
    'BigQueryClientEmail' |
    'BigQueryPrivateKey' |
    'PingboardClientID' |
    'PingboardClientSecret' |
    'FlareHRPrimaryKey' | 
    'FacebookAppSecret' | 
    'FacebookAppId' | 
    'FlareHrKey'

export interface IConfig {
    readonly PingboardClientID?: string;
    readonly PingboardClientSecret?: string;
    readonly BigQueryClientEmail?: string;
    readonly BigQueryPrivateKey?: string;
    readonly FlareHrKey?: string;
    readonly WorkplaceAccessToken?: string;
    readonly FirestoreCertificate?: string;
    readonly FlareHRPrimaryKey?: string;
    readonly FacebookAppId?: string;
    readonly FacebookAppSecret?: string;
}

export interface ISlackChannel {
    Name: string;
    Id: string
}

export class SlackChannel {
    private _record: DynamoDB.AttributeMap;

    constructor(record: DynamoDB.AttributeMap) {
        this._record = record
    }

    get Name() {
        return this._record.Name.S;
    }

    get Id() {
        return this._record.Id.S;
    }
}

export interface ISnsTopicSlackConfig {
    Enabled: boolean,
    SlackChannelName: string,
    SlackAppName: string
}

export class SnsTopicSlackConfig implements ISnsTopicSlackConfig {
    private _record: DynamoDB.AttributeMap;

    constructor(record: DynamoDB.AttributeMap) {
        this._record = record;
    }

    get Enabled() {
        return this._record.Enabled.BOOL as boolean;
    }

    get SlackAppName() {
        return this._record.SlackAppName.S as string;
    }

    get SlackChannelName() {
        return this._record.SlackChannelName.S as string;
    }

    get SnsTopicArn() {
        return this._record.SnsTopicArn.S as string;
    }
}