/* Apps */
export type SlackAppNames = 'Provisioning' |
    'AWSNotificationService' |
    'Development';

export type OAuthTokenKey = { [app in SlackAppNames]: string }
export type SigningSecretKey = { [app in SlackAppNames]: string }

/* Channels */
export type SlackChannelNames = 'Provisioning' |
    'PureCloudSync' |
    'EngAlertsDev' |
    'EngAlertsProd' | 
    'Engineering';

export type SlackChannelIDs = 'GCPUFAE0P' |
    'CBYVBL4CX' |
    'GCH7GC5EC' |
    'CDNU1LH7Z' |
    'GCJ6Y1P3K'

export type ChannelIDMap = { [channel in SlackChannelNames]: SlackChannelIDs };