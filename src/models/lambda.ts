import { Context } from "aws-lambda";
import { badRequestSuffixMap, HttpStatusCodes, HttpStatusMessages, unknownBadRequestSuffix } from "../constants";
import { BadRequestError, EmploysureErrorResponse } from "../lib/lambda/models";

/**
 * Event structure for workplace-provision lambda 
 */
export interface LambdaEvent<T> {
    RetryCount?: number,
    Source?: string,
    Author?: string,
    Stage?: "Dev" | "Prod"
    options: T
}

export type LambdaNameMap = { [name: string]: LambdaFunctionNames };
export type LambdaFunctionNames =
    'slack-utilities-dev-postSlackMessage' |
    'slack-utilities-dev-updateSlackMessage' |
    'fwc-lead-scrapper-dev-run' |
    'nz-utils-dev-leadScraper' |
    'provisioning-dev-provision-workplace' |
    'provisioning-prod-provision-workplace';


export type HttpStatusSuccess = "OK" | "MULTI_STATUS";
export type HttpStatusBadRequest = "BAD_REQUEST";
export type HttpStatusServerError = "SERVER_ERROR";
export type HttpStatusErrors = HttpStatusBadRequest | HttpStatusServerError;
export type HttpStatusAll = HttpStatusSuccess | HttpStatusErrors;



/** Response to return to Apigee. */
export class ApigeeResponse<T = any> {
    /* Uses HTTP status codes. */
    StatusCode: number;
    /* Response data particular to this function. */
    Data?: T;
    /* Employsure extension to narrow down the error. Will be included in final payload. */
    StatusCodeSuffix?: string;
    /* A human readabile */
    ErrorName?: string;
    /* Message to return to the user (e.g. 4xx error details). */
    UserMessage?: string;
    /* The associated Id for this request + execution. */
    RequestId?: string;

    constructor(
        status: HttpStatusAll,
        data?: T,
        userMessage?: string
    ) {
        this.StatusCode = HttpStatusCodes[status];
        this.Data = data;
        this.UserMessage = userMessage;
    }
}

export class ApigeeBadRequestResponse extends ApigeeResponse<EmploysureErrorResponse> {
    constructor(error: BadRequestError, context: Context) {
        super("BAD_REQUEST");
        this.StatusCodeSuffix = badRequestSuffixMap[error.name] ?? unknownBadRequestSuffix;
        this.ErrorName = error.name;
        this.UserMessage = error.message;
        this.RequestId = context.awsRequestId;
    }
}

export class ApigeeServerErrorResponse extends ApigeeResponse<EmploysureErrorResponse> {
    constructor(context: Context) {
        super("SERVER_ERROR")
        this.RequestId = context.awsRequestId;
    }
}

/**
 * Error to throw if the lambda fails
 */
export class LambdaError<T> extends Error {
    private lambdaResponse: ApigeeResponse<T>;

    /**
     * @param lambdaResponse lambda response. The status message is used as the error message
     */
    constructor(lambdaResponse: ApigeeResponse<T>) {
        super(`${lambdaResponse.StatusCode}`);
        this.name = "LambdaError";
        this.lambdaResponse = lambdaResponse;
    }
}