export const ResponseStatus = {
    RECEIVED: 'received', // yet to be processed
    CREATED: 'created', // 2xx
    INVALID: 'invalid', // 4xx
    ERROR: 'error', //5xx
};
