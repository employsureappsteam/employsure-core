import { TopicArns, SnsTopicNames } from "../models/sns";

export const SnsTopics: { [topic in SnsTopicNames]: TopicArns } = {
    FlareHrDataErrors: 'arn:aws:sns:ap-southeast-2:711143483997:flarehr-data-errors', 
    EngErrorsDev: 'arn:aws:sns:ap-southeast-2:711143483997:engineering-errors-dev',
    EngErrorsProd: 'arn:aws:sns:ap-southeast-2:711143483997:engineering-errors-prod',
    ProvisioningWorkplace: 'arn:aws:sns:ap-southeast-2:711143483997:provisioning-workplace',
    PingboardSync: 'arn:aws:sns:ap-southeast-2:711143483997:pingboard-sync',
    EmploysureApiError: 'arn:aws:sns:ap-southeast-2:711143483997:employsure-api-errors',
    EmploysureLeadApiWarnings: 'arn:aws:sns:ap-southeast-2:711143483997:employsure-lead-api-warnings',
    EmploysureLeadApiCampaignWarnings: 'arn:aws:sns:ap-southeast-2:711143483997:employsure-lead-api-campaign-warnings'
}