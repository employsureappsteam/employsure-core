import { ChannelIDMap } from "../models/slack";

export const ChannelIDs: ChannelIDMap = {
    Provisioning: "GCPUFAE0P",
    PureCloudSync: "CBYVBL4CX",
    EngAlertsDev: "GCH7GC5EC",
    EngAlertsProd: "CDNU1LH7Z",
    Engineering: "GCJ6Y1P3K"
};

export const AppConfigPaths = {
    Provisioning: "/Slack/Provisioning",
    AWSNotificationService: "/Slack/AWSNotification",
    Development: "/Slack/Development",
}