export const AWS_LAMBDA_API_VERSION = "2015-03-31"
export const AWS_STEP_FUNCTIONS_API_VERSION = "2016-11-23"
export const AWS_SSM_API_VERSION = "2014-11-06"

export const AWS_REGION_DEFAULT = "ap-southeast-2"
export const AWS_REGION_SES = "us-west-2"
