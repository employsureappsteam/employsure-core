import { LambdaNameMap, HttpStatusAll } from "../models";

export const LambdaNamesMap: LambdaNameMap = {
    postSlackMessage: "slack-utilities-dev-postSlackMessage",
    updateSlackMessage: "slack-utilities-dev-updateSlackMessage",
    FWCLeadScrapper: "fwc-lead-scrapper-dev-run",
    NZLeadSrapper: "nz-utils-dev-leadScraper",
    ProvisionWorkplaceDev: "provisioning-dev-provision-workplace",
    ProvisionWorkplaceProd: "provisioning-prod-provision-workplace"
}

export const HttpStatusCodes: { [type in HttpStatusAll]: number } = {
    "OK": 200,
    "MULTI_STATUS": 207,
    "BAD_REQUEST": 400,
    "SERVER_ERROR": 500,
}

export const HttpStatusMessages: { [type in HttpStatusAll]: string } = {
    "OK": "Success",
    "BAD_REQUEST": "Bad Request",
    "SERVER_ERROR": "Server Error",
    "MULTI_STATUS": "Multi-Status"
}

export const unknownBadRequestSuffix = "02";
export const badRequestSuffixMap: { [x: string]: string } = {
    "BadRequestError": "03",
    "InvalidPayload": "04",
}