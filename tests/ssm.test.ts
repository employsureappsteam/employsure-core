import { expect } from 'chai';
import { AWS_REGION_DEFAULT, SnsTopics, SnsTopicNames } from '../src';
import { SNS } from 'aws-sdk';

var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const snsClient = new SNS({ region: AWS_REGION_DEFAULT });

snsClient.listTopics().promise().then(snsTopicList => {
    describe('All topics mapped should exist', () => {
        if (!snsTopicList.Topics) throw new Error('No Topics Found')
    
        const arns = snsTopicList.Topics.map(t => t.TopicArn);
    
        Object.keys(SnsTopics).map(name => {
            it(`Found a valid ARN for ${name}`, () => {
                return expect(arns.includes(SnsTopics[name as SnsTopicNames])).to.be.true;
            })
        })
    })

});
