import { suite, test } from 'mocha-typescript';
import { expect } from 'chai';
import { GetConfig, ConfigFields, GetSecretParams } from '../src';

var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);


@suite
export class GetConfigIntegration {

    /* Typing guarantees that every field is covered. */
    configFields: {[field in ConfigFields]:string} = {
        'WorkplaceAccessToken': "",
        'FirestoreCertificate': "",
        'BigQueryClientEmail': "",
        'BigQueryPrivateKey': "",
        'PingboardClientID': "",
        'PingboardClientSecret': "",
        'FlareHRPrimaryKey': "",
        'FacebookAppSecret': "",
        'FacebookAppId': "",
        'FlareHrKey': ""
    };

    @test
    GetConfigErrors() {
        describe('GetConfig should throw an error', () => {
            it('For empty params', () => {
                return expect(GetConfig.bind([])).to.throw;
            })
            it('For fields that aren\'t mapped', () => {
                return expect(GetConfig.bind(['FakeField' as ConfigFields])).to.throw;
            })
        })
    }

    @test
    GetConfigValidFields() {
        process.env.stage = "Dev"
        describe('GetConfig should fetch all fields mapped in Dev', () => {
            Object.keys(this.configFields).map((field) => {
                it(`Dev IConfig has field ${field}`, () => {
                    return expect(GetConfig([field as ConfigFields])).to.eventually.have.property(field);
                })
            })
        })
        
        process.env.stage = "Prod"
        describe('GetConfig should fetch all fields mapped in Prod', () => {
            Object.keys(this.configFields).map((field) => {
                it(`Dev IConfig has field ${field}`, () => {
                    return expect(GetConfig([field as ConfigFields])).to.eventually.have.property(field);
                })
            })
        })
    }

    @test
    GetSecretParams() {
        describe('Test secret param with known params', () => {
            it('Should fine John\'s Salesforce Dev Username', () => {
                const name = '/IntegrationTestPath/Test/DoNotDelete';
                return expect(GetSecretParams([name])).to.eventually.have.property(name);
            });
        })
    }
}
