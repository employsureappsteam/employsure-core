"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const src_1 = require("../src");
process.env.stage = "Dev";
(async () => {
    const whichApp = 'AzureAppEngineeringData';
    const secrets = await src_1.GetSecretParams([
        `/${whichApp}/Dev/ClientId`,
        `/${whichApp}/Dev/ClientSecret`,
        `/${whichApp}/Dev/TenantId`
    ]);
    const azureClient = new src_1.AzureClient({
        clientSecret: secrets[`/${whichApp}/Dev/ClientSecret`],
        clientId: secrets[`/${whichApp}/Dev/ClientId`],
        tenantId: secrets[`/${whichApp}/Dev/TenantId`]
    });
    // const users = await azureClient.getUsers();
    try {
        const test = await uploadFile(azureClient);
        debugger;
    }
    catch (error) {
        debugger;
    }
    debugger;
})();
async function updateItem(azureClient) {
    const data = {
        Title: 'okay : )',
        start_time_str: '16:00'
    };
    const d = await azureClient.updateSharepointListItem({
        siteId: "31d1e271-3df6-4145-b998-1dda4e83b6ba",
        listId: "bae27820-2765-4afe-9946-8db9805f5381",
        itemId: "3"
    }, data);
    console.log(`Updated item with id ${d.id}, title to: ${d.Title}`);
    return d;
}
function uploadFile(azureClient) {
    return azureClient.uploadFile({
        siteId: '31d1e271-3df6-4145-b998-1dda4e83b6ba',
        file: {
            body: 'test body is testing',
            type: 'text/plain',
            filename: 'api_test_2.txt'
        },
        drive: {
            // name: 'JOHN DOC LIB',
            id: 'b!ceLRMfY9RUG5mB3aToO2uqfc0ntzEtBLhBGdLDMm9pNRYwlzm5bVTorEYGyDEdeE'
        }
    });
}
//# sourceMappingURL=azureClientMethods.js.map