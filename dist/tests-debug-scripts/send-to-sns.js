"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sendToSns_1 = require("../src/lib/sns/sendToSns");
const constants_1 = require("../src/constants");
/* Only test in dev environment. */
process.env.stage = "Dev"; // Or "Prod"
const response = sendToSns_1.sendToSns(constants_1.SnsTopics.EmploysureApiError, "Test Notification", [
    { title: "Test", message: "Test" }
]);
response.then(data => {
    console.log(data);
}).catch(err => {
    console.log(err);
});
//# sourceMappingURL=send-to-sns.js.map