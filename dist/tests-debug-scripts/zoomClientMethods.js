"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const zoom_1 = require("../src/clients/zoom");
const lib_1 = require("../src/lib");
const moment_1 = __importDefault(require("moment"));
async function loadClients() {
    const apiKeySsmPath = "/ZoomProvisioning/Prod/ApiKey";
    const apiSecretSsmPath = "/ZoomProvisioning/Prod/ApiSecret";
    const secrets = await lib_1.GetSecretParams([
        apiKeySsmPath,
        apiSecretSsmPath
    ]);
    const zoomClient = new zoom_1.ZoomClient({
        apiKey: secrets[apiKeySsmPath],
        apiSecret: secrets[apiSecretSsmPath],
    });
    return { zoomClient };
}
(async () => {
    console.log();
    const date_from = moment_1.default().subtract(5, 'days').format("YYYY-MM-DD");
    debugger;
    const { zoomClient } = await loadClients();
    const u = await zoomClient.getUsers();
    const d = new Date();
    for (const user of u.splice(0, 5)) {
        const userMeetings = await zoomClient.getUserMeetings(user.id, { from: date_from });
        console.log(`${user.email}: ${userMeetings.length}`);
    }
})();
//# sourceMappingURL=zoomClientMethods.js.map