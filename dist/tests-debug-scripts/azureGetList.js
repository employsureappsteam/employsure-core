"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const src_1 = require("../src");
const clients_1 = require("../src/clients");
process.env.stage = "Dev";
(async () => {
    const clientEmail = `/BigQuery/${process.env.stage}/ClientEmail`;
    const privateKey = `/BigQuery/${process.env.stage}/PrivateKeyWithoutNewline`;
    const secrets = await src_1.GetSecretParams([
        '/AzureEmployeeProvisioning/Dev/ClientId',
        '/AzureEmployeeProvisioning/Dev/ClientSecret',
        '/AzureEmployeeProvisioning/Dev/TenantId',
        clientEmail,
        privateKey
    ]);
    const azureClient = new src_1.AzureClient({
        clientSecret: secrets['/AzureEmployeeProvisioning/Dev/ClientSecret'],
        clientId: secrets['/AzureEmployeeProvisioning/Dev/ClientId'],
        tenantId: secrets['/AzureEmployeeProvisioning/Dev/TenantId']
    });
    await azureClient.init();
    const bq = new clients_1.BigQueryClient('lead-scoring-model', {
        client_email: secrets[clientEmail],
        private_key: secrets[privateKey]
    });
    const a = await azureClient.getUsers();
    debugger;
    /* Fetch the max createddatetime from the synced records. */
    const queryRows = await bq.query('select FORMAT_TIMESTAMP("%Y-%m-%dT%H:%M:%E*SZ", max(createddatetime)) as max_time from `lead-scoring-model.azure_prod.sign_ins_export_*`');
    const maxTime = queryRows[0].max_time;
    const logins = await azureClient.getList(`auditLogs/signIns?$filter=createdDateTime ge ${maxTime}`, { maxRequests: 1 });
    debugger;
})();
//# sourceMappingURL=azureGetList.js.map