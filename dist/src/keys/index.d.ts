import { OAuthTokenKey, SigningSecretKey } from "../models";
export declare const SlackOAuthTokens: OAuthTokenKey;
export declare const SlackSigningSecrets: SigningSecretKey;
