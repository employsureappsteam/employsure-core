"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* Slack. */
exports.SlackOAuthTokens = {
    Provisioning: "/Slack/Provisioning/OAuthToken",
    AWSNotificationService: "/Slack/AWSNotification/OAuthToken",
    Development: "/Slack/Development/OAuthToken",
};
exports.SlackSigningSecrets = {
    Provisioning: "/Slack/Provisioning/SigningSecret",
    AWSNotificationService: "/Slack/AWSNotification/SigningSecret",
    Development: "/Slack/Development/SigningSecret",
};
//# sourceMappingURL=index.js.map