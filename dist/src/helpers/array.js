"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
function dedupeArrayByKey(array, key) {
    const dedupedArray = [];
    for (const c of array) {
        const dups = dedupedArray.filter(d => c[key] === d[key]);
        if (dups.length === 0) {
            dedupedArray.push(c);
        }
    }
    return dedupedArray;
}
exports.dedupeArrayByKey = dedupeArrayByKey;
function chunkArray(_arr, chunkSize) {
    const recordChunks = [];
    for (let i = 0; i < _arr.length; i += chunkSize) {
        recordChunks.push(_arr.slice(i, i + chunkSize));
    }
    return recordChunks;
}
exports.chunkArray = chunkArray;
exports.mapValuesDeep = (v, callback) => {
    if (lodash_1.default.isObject(v) && !lodash_1.default.isArray(v)) {
        return lodash_1.default.mapValues(v, v => exports.mapValuesDeep(v, callback));
    }
    else if (lodash_1.default.isArray(v)) {
        return lodash_1.default.map(v, v => exports.mapValuesDeep(v, callback));
    }
    else {
        return callback(v);
    }
};
//# sourceMappingURL=array.js.map