export declare function dedupeArrayByKey<T>(array: Array<T>, key: string): Array<T>;
export declare function chunkArray<T = any>(_arr: T[], chunkSize: number): T[][];
export declare const mapValuesDeep: (v: any, callback: any) => any;
