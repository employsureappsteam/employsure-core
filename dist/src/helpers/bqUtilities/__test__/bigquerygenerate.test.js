"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = __importStar(require("chai"));
const bigquerySchemaGenerator_1 = __importDefault(require("../bigquerySchemaGenerator"));
const test_b_input = __importStar(require("./mockData/test_b_input"));
const test_b_output = __importStar(require("./mockData/test_b_output"));
const bqQueryUnion_1 = require("../bqQueryUnion");
/* Chai extensions */
const deep_equal_in_any_order_1 = __importDefault(require("deep-equal-in-any-order"));
chai_1.default.use(deep_equal_in_any_order_1.default);
describe('generate bigquery schema from JSON', () => {
    it('String type', () => {
        const original = {
            test: 'hello'
        };
        const expected = [{
                mode: "NULLABLE",
                name: 'test',
                type: 'STRING'
            }];
        const generated = bigquerySchemaGenerator_1.default(original);
        chai_1.expect(generated).deep.equal(expected);
    });
    it('Null type should be skipped', () => {
        const original = {
            id: 'deadbeef',
            test: null
        };
        const expected = [{
                mode: "NULLABLE",
                name: 'id',
                type: 'STRING'
            }];
        const generated = bigquerySchemaGenerator_1.default(original);
        chai_1.expect(generated).deep.equal(expected);
    });
});
describe('bqSchemaUnion | Testing flat schemas (without RECORD type)', () => {
    it('Union of 2x flat schema', () => {
        const inputSchemas = [
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'second_field',
                    type: "FLOAT"
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'third_field',
                    type: "BOOLEAN"
                }
            ],
        ];
        const expectedSchema = [
            {
                name: 'id',
                type: "STRING"
            },
            {
                name: 'second_field',
                type: "FLOAT"
            },
            {
                name: 'third_field',
                type: "BOOLEAN"
            }
        ];
        const finalSchema = bqQueryUnion_1.bqSchemaUnion(inputSchemas);
        chai_1.expect(finalSchema).to.deep.equals(expectedSchema);
    });
    it('Union of 2x flat schema with conflicting types should throw error', () => {
        const inputSchemas = [
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'second_field',
                    type: "FLOAT"
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'second_field',
                    type: "BOOLEAN"
                }
            ],
        ];
        const finalSchemaFn = () => bqQueryUnion_1.bqSchemaUnion(inputSchemas);
        chai_1.expect(finalSchemaFn).to.throw();
    });
    it('Union of 3x flat schema', () => {
        const inputSchemas = [
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'second_field',
                    type: "FLOAT"
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'third_field',
                    type: "BOOLEAN"
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'fourth_field',
                    type: "DATE"
                }
            ],
        ];
        const expectedSchema = [
            {
                name: 'id',
                type: "STRING"
            },
            {
                name: 'second_field',
                type: "FLOAT"
            },
            {
                name: 'third_field',
                type: "BOOLEAN"
            },
            {
                name: 'fourth_field',
                type: "DATE"
            }
        ];
        const finalSchema = bqQueryUnion_1.bqSchemaUnion(inputSchemas);
        chai_1.expect(finalSchema).to.deep.equals(expectedSchema);
    });
});
describe('bqSchemaUnion | Testing nested records (with RECORD type)', () => {
    it('Union of 2x nested schemas', () => {
        const inputSchemas = [
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'a_record_type',
                    type: "RECORD",
                    fields: [
                        {
                            name: 'first_child',
                            type: 'STRING'
                        }
                    ]
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'misc',
                    type: "STRING"
                },
                {
                    name: 'a_record_type',
                    type: "RECORD",
                    fields: [
                        {
                            name: 'second_child',
                            type: 'BOOLEAN'
                        }
                    ]
                }
            ]
        ];
        const expectedSchema = [
            {
                name: 'id',
                type: "STRING"
            },
            {
                name: 'misc',
                type: "STRING"
            },
            {
                name: 'a_record_type',
                type: "RECORD",
                fields: [
                    {
                        name: 'first_child',
                        type: 'STRING'
                    },
                    {
                        name: 'second_child',
                        type: 'BOOLEAN'
                    }
                ]
            }
        ];
        const finalSchema = bqQueryUnion_1.bqSchemaUnion(inputSchemas);
        // @ts-ignore
        chai_1.expect(finalSchema).to.deep.equalInAnyOrder(expectedSchema);
    });
    it('Type conflicts within nested schemas should throw an error', () => {
        const inputSchemas = [
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'a_record_type',
                    type: "RECORD",
                    fields: [
                        {
                            name: 'first_child',
                            type: 'STRING'
                        }
                    ]
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'misc',
                    type: "STRING"
                },
                {
                    name: 'a_record_type',
                    type: "RECORD",
                    fields: [
                        {
                            name: 'first_child',
                            type: 'BOOLEAN'
                        },
                        {
                            name: 'second_child',
                            type: 'BOOLEAN'
                        }
                    ]
                }
            ]
        ];
        const finalSchemaFn = () => bqQueryUnion_1.bqSchemaUnion(inputSchemas);
        chai_1.expect(finalSchemaFn).to.throw();
    });
    it('Union of 2x double nested schema', () => {
        const inputSchemas = [
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'a_record_type',
                    type: "RECORD",
                    fields: [
                        {
                            name: 'first_child',
                            type: 'STRING'
                        },
                        {
                            name: 'deep_nested',
                            type: 'RECORD',
                            fields: [{
                                    name: 'first_deep_child',
                                    type: 'STRING'
                                }]
                        }
                    ]
                }
            ],
            [
                {
                    name: 'id',
                    type: "STRING"
                },
                {
                    name: 'misc',
                    type: "STRING"
                },
                {
                    name: 'a_record_type',
                    type: "RECORD",
                    fields: [
                        {
                            name: 'second_child',
                            type: 'BOOLEAN'
                        },
                        {
                            name: 'deep_nested',
                            type: 'RECORD',
                            fields: [
                                {
                                    name: 'first_deep_child',
                                    type: 'STRING'
                                },
                                {
                                    name: 'second_deep_child',
                                    type: 'TIMESTAMP'
                                },
                            ]
                        }
                    ]
                }
            ]
        ];
        const expectedSchema = [
            {
                name: 'id',
                type: "STRING"
            },
            {
                name: 'misc',
                type: "STRING"
            },
            {
                name: 'a_record_type',
                type: "RECORD",
                fields: [
                    {
                        name: 'first_child',
                        type: 'STRING'
                    },
                    {
                        name: 'second_child',
                        type: 'BOOLEAN'
                    },
                    {
                        name: 'deep_nested',
                        type: 'RECORD',
                        fields: [
                            {
                                name: 'first_deep_child',
                                type: 'STRING'
                            },
                            {
                                name: 'second_deep_child',
                                type: 'TIMESTAMP'
                            },
                        ]
                    }
                ]
            }
        ];
        const finalSchema = bqQueryUnion_1.bqSchemaUnion(inputSchemas);
        // @ts-ignore
        chai_1.expect(finalSchema).to.deep.equalInAnyOrder(expectedSchema);
    });
});
describe('bqSchemaUnion | Options: type coercion', () => {
    it('Conflicting type TIMESTAMP to STRING should be allowed when type coercion is enabled ', () => {
        const inputSchemas = [
            [
                {
                    name: 'test',
                    type: "STRING"
                },
            ],
            [
                {
                    name: 'test',
                    type: "TIMESTAMP"
                }
            ],
        ];
        const expectedSchema = [
            {
                name: 'test',
                type: "STRING"
            }
        ];
        const finalSchema = bqQueryUnion_1.bqSchemaUnion(inputSchemas);
        chai_1.expect(finalSchema).to.deep.equals(expectedSchema);
    });
    it('Conflicting type TIMESTAMP to STRING should throw error when type coercion is disabled ', () => {
        const inputSchemas = [
            [
                {
                    name: 'test',
                    type: "STRING"
                },
            ],
            [
                {
                    name: 'test',
                    type: "TIMESTAMP"
                }
            ],
        ];
        const finalSchemaFn = () => bqQueryUnion_1.bqSchemaUnion(inputSchemas, { typeCoercionEnabled: false });
        chai_1.expect(finalSchemaFn).to.throw();
    });
});
describe('bqSchemaUnion | Production examples', () => {
    it('example - UserInformationListSchema', () => {
        const inputSchemas = test_b_input.data;
        const expectedSchema = test_b_output.data;
        const finalSchema = bqQueryUnion_1.bqSchemaUnion(inputSchemas);
        // @ts-ignore
        chai_1.expect(finalSchema).to.deep.equalInAnyOrder(expectedSchema);
    });
});
//# sourceMappingURL=bigquerygenerate.test.js.map