"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.data = [
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Notes",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Notes",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Notes",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SipAddress",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Picture",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "Description",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "Url",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                },
                {
                    "name": "Department",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "JobTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Office",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureTimestamp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPicturePlaceholderState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureExchangeSyncState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Notes",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SipAddress",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Picture",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "Description",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "Url",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                },
                {
                    "name": "Department",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "JobTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Office",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureTimestamp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPicturePlaceholderState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureExchangeSyncState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Notes",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SipAddress",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Picture",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "Description",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "Url",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                },
                {
                    "name": "Department",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "JobTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "WorkPhone",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Office",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureTimestamp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPicturePlaceholderState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureExchangeSyncState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AppAuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AppEditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Picture",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "Description",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "Url",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Office",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureTimestamp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPicturePlaceholderState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureExchangeSyncState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AppAuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Picture",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "Description",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "Url",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                },
                {
                    "name": "Department",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "JobTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Office",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureTimestamp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPicturePlaceholderState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureExchangeSyncState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AppAuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Department",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "JobTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Office",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AppAuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SipAddress",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Picture",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "Description",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "Url",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                },
                {
                    "name": "Department",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "JobTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Office",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureTimestamp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPicturePlaceholderState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureExchangeSyncState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AppAuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SipAddress",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Picture",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "Description",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "Url",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                },
                {
                    "name": "Department",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "JobTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Office",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureTimestamp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPicturePlaceholderState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureExchangeSyncState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Picture",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "Description",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "Url",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                },
                {
                    "name": "Department",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "JobTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Office",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureTimestamp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPicturePlaceholderState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureExchangeSyncState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Picture",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "Description",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "Url",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                },
                {
                    "name": "JobTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureTimestamp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPicturePlaceholderState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureExchangeSyncState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Picture",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "Description",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "Url",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                },
                {
                    "name": "Department",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "JobTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Office",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureTimestamp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPicturePlaceholderState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureExchangeSyncState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Picture",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "Description",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "Url",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                },
                {
                    "name": "Department",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "JobTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Office",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureTimestamp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPicturePlaceholderState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureExchangeSyncState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Picture",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "Description",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "Url",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                },
                {
                    "name": "Department",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "JobTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "WorkPhone",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Office",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureTimestamp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPicturePlaceholderState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureExchangeSyncState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "email",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "id",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EMail",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Picture",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "Description",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        },
                        {
                            "name": "Url",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                },
                {
                    "name": "Department",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "JobTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FirstName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LastName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Office",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureTimestamp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPicturePlaceholderState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "SPSPictureExchangeSyncState",
                    "type": "INTEGER",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ],
    [
        {
            "name": "createdDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "eTag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "id",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "lastModifiedDateTime",
            "type": "TIMESTAMP",
            "mode": "NULLABLE"
        },
        {
            "name": "webUrl",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "createdBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "lastModifiedBy",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "user",
                    "type": "RECORD",
                    "mode": "NULLABLE",
                    "fields": [
                        {
                            "name": "displayName",
                            "type": "STRING",
                            "mode": "NULLABLE"
                        }
                    ]
                }
            ]
        },
        {
            "name": "parentReference",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "siteId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "contentType",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "fields",
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [
                {
                    "name": "Title",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Name",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "IsSiteAdmin",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Deleted",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserInfoHidden",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitle",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "id",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentType",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Modified",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Created",
                    "type": "TIMESTAMP",
                    "mode": "NULLABLE"
                },
                {
                    "name": "AuthorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditorLookupId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_UIVersionString",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Attachments",
                    "type": "BOOLEAN",
                    "mode": "NULLABLE"
                },
                {
                    "name": "Edit",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "LinkTitleNoMenu",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ItemChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "FolderChildCount",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceFlags",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagWrittenTime",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_ComplianceTagUserId",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ImnName",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_36px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_48px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "PictureOnly_Size_72px",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "NameWithPictureAndDetails",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "EditUser",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "UserSelection",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "ContentTypeDisp",
                    "type": "STRING",
                    "mode": "NULLABLE"
                },
                {
                    "name": "_odata_etag",
                    "type": "STRING",
                    "mode": "NULLABLE"
                }
            ]
        },
        {
            "name": "_odata_etag",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "fields_odata_context",
            "type": "STRING",
            "mode": "NULLABLE"
        }
    ]
];
//# sourceMappingURL=test_b_input.js.map