declare const _default: ({
    "name": string;
    "type": string;
    "fields"?: undefined;
} | {
    "name": string;
    "type": string;
    "fields": {
        "name": string;
        "type": string;
        "fields": {
            "name": string;
            "type": string;
        }[];
    }[];
} | {
    "name": string;
    "type": string;
    "fields": {
        "name": string;
        "type": string;
    }[];
})[];
export default _default;
