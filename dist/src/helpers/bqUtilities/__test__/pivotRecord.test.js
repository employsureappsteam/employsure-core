"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const pivotRecord_1 = require("../pivotRecord");
/* Chai extensions */
describe('Pivot JSON Object', () => {
    it('Simple pivot', () => {
        const record = {
            'id': 555,
            'FY 2020 Jun': 2,
            'FY 2020 Jul': 0.5,
            'FY 2020 Aug': 3,
        };
        const pivotedTest = pivotRecord_1.pivotRecord(record, {
            srcColsRegexPattern: 'FY \\d{4} \\w+',
            destColValue: 'FTE',
            destColField: 'year_month_period'
        });
        const pivotedExpected = [
            { 'id': 555, 'year_month_period': 'FY 2020 Jun', 'FTE': 2 },
            { 'id': 555, 'year_month_period': 'FY 2020 Jul', 'FTE': 0.5 },
            { 'id': 555, 'year_month_period': 'FY 2020 Aug', 'FTE': 3 },
        ];
        chai_1.expect(pivotedTest).deep.equal(pivotedExpected);
    });
    it('With 1 key/value with value as object', () => {
        const record = {
            'id': 555,
            'att': { 'hello': 'banana', 'hello2': 'apple', },
            'FY 2020 Jun': 2,
            'FY 2020 Jul': 0.5,
            'FY 2020 Aug': 3,
        };
        const pivotedTest = pivotRecord_1.pivotRecord(record, {
            srcColsRegexPattern: 'FY \\d{4} \\w+',
            destColValue: 'FTE',
            destColField: 'year_month_period'
        });
        const pivotedExpected = [
            { 'id': 555, 'att': { 'hello': 'banana', 'hello2': 'apple', }, 'year_month_period': 'FY 2020 Jun', 'FTE': 2 },
            { 'id': 555, 'att': { 'hello': 'banana', 'hello2': 'apple', }, 'year_month_period': 'FY 2020 Jul', 'FTE': 0.5 },
            { 'id': 555, 'att': { 'hello': 'banana', 'hello2': 'apple', }, 'year_month_period': 'FY 2020 Aug', 'FTE': 3 },
        ];
        chai_1.expect(pivotedTest).deep.equal(pivotedExpected);
    });
    it("Only 1 record if regex doesn't match", () => {
        const record = {
            'id': 555,
            'FY 2020 Jun': 2,
            'FY 2020 Jul': 0.5,
            'FY 2020 Aug': 3,
        };
        const pivotedTest = pivotRecord_1.pivotRecord(record, {
            srcColsRegexPattern: 'TEST\\d{4}',
            destColValue: 'FTE',
            destColField: 'year_month_period'
        });
        const pivotedExpected = [record];
        chai_1.expect(pivotedTest).deep.equal(pivotedExpected);
    });
});
describe('Pivot JSON object on a particular field', () => {
    it('Simple pivot', () => {
        const record = {
            hello: 'coffee',
            fields: {
                'id': 555,
                'FY 2020 Jun': 2,
                'FY 2020 Jul': 0.5,
                'FY 2020 Aug': 3,
            }
        };
        const pivotedTest = pivotRecord_1.pivotRecord(record, {
            srcColsRegexPattern: 'FY \\d{4} \\w+',
            destColValue: 'FTE',
            destColField: 'year_month_period',
            pivotDataOnKey: 'fields'
        });
        const pivotedExpected = [
            { hello: 'coffee', fields: { 'id': 555, 'year_month_period': 'FY 2020 Jun', 'FTE': 2 } },
            { hello: 'coffee', fields: { 'id': 555, 'year_month_period': 'FY 2020 Jul', 'FTE': 0.5 } },
            { hello: 'coffee', fields: { 'id': 555, 'year_month_period': 'FY 2020 Aug', 'FTE': 3 } },
        ];
        chai_1.expect(pivotedTest).deep.equal(pivotedExpected);
    });
});
describe('Pivot arrays of JSON objects', () => {
    it("Array of simple records", () => {
        const records = [
            {
                'id': 555,
                'FY 2020 Jun': 2,
                'FY 2020 Jul': 0.5,
                'FY 2020 Aug': 3,
            },
            {
                'id': 556,
                'FY 2020 Jun': 2.6,
                'FY 2020 Jul': 0.56,
                'FY 2020 Aug': 3.6,
            },
        ];
        const options = {
            srcColsRegexPattern: 'FY \\d{4} \\w+',
            destColValue: 'FTE',
            destColField: 'year_month_period'
        };
        const pivotedTest = pivotRecord_1.pivotRecords(records, options);
        const pivotedExpected = [
            { 'id': 555, 'year_month_period': 'FY 2020 Jun', 'FTE': 2 },
            { 'id': 555, 'year_month_period': 'FY 2020 Jul', 'FTE': 0.5 },
            { 'id': 555, 'year_month_period': 'FY 2020 Aug', 'FTE': 3 },
            { 'id': 556, 'year_month_period': 'FY 2020 Jun', 'FTE': 2.6 },
            { 'id': 556, 'year_month_period': 'FY 2020 Jul', 'FTE': 0.56 },
            { 'id': 556, 'year_month_period': 'FY 2020 Aug', 'FTE': 3.6 },
        ];
        chai_1.expect(pivotedTest).deep.equal(pivotedExpected);
    });
    it("Array of 1 record to be pivoted, and another to be ignored", () => {
        const records = [
            {
                'id': 555,
                'FY 2020 Jun': 2,
                'FY 2020 Jul': 0.5,
                'FY 2020 Aug': 3,
            },
            {
                'id': 556,
                'foo': 'bar'
            },
        ];
        const options = {
            srcColsRegexPattern: 'FY \\d{4} \\w+',
            destColValue: 'FTE',
            destColField: 'year_month_period'
        };
        const pivotedTest = pivotRecord_1.pivotRecords(records, options);
        const pivotedExpected = [
            { 'id': 555, 'year_month_period': 'FY 2020 Jun', 'FTE': 2 },
            { 'id': 555, 'year_month_period': 'FY 2020 Jul', 'FTE': 0.5 },
            { 'id': 555, 'year_month_period': 'FY 2020 Aug', 'FTE': 3 },
            { 'id': 556, 'foo': 'bar' },
        ];
        chai_1.expect(pivotedTest).deep.equal(pivotedExpected);
    });
});
//# sourceMappingURL=pivotRecord.test.js.map