"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addSchema = (curr, newSchema) => {
    for (const schema of curr) {
        if (schema.name === newSchema.name) {
            if (schema.type === newSchema.type) {
                return;
            }
            else {
                throw new Error(`Duplicate schema ${JSON.stringify(newSchema)}`);
            }
        }
    }
    curr.push(newSchema);
    return;
};
//# sourceMappingURL=addSchema.js.map