"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
// TODO: further testing around these functions. Involves deep copying hacks and not sure of edge cases that might handle data unepxectedly
exports.pivotRecords = (records, opt) => {
    return records
        .map(d => exports.pivotRecord(d, opt))
        .reduce((acc, rec) => [...acc, ...rec], []);
};
exports.pivotRecord = (record, opt) => {
    const regex = new RegExp(opt.srcColsRegexPattern);
    const dataToPivot = [];
    const pivotRoot = opt.pivotDataOnKey ? record[opt.pivotDataOnKey] : record;
    /* Collect all the changes*/
    for (const [key, value] of Object.entries(pivotRoot)) {
        if (key.match(regex)) {
            dataToPivot.push({ key, value });
            delete pivotRoot[key];
        }
    }
    /* Loop over the data that was pivoted and create a new record for each. */
    const newRecords = dataToPivot.map(data => {
        const obj = lodash_1.default.cloneDeep(record);
        const pivotRoot = opt.pivotDataOnKey ? obj[opt.pivotDataOnKey] : obj;
        pivotRoot[opt.destColField] = data.key;
        pivotRoot[opt.destColValue] = data.value;
        return obj;
    });
    /* If there was nothing to be pivoted just return the original record. */
    if (newRecords.length === 0) {
        return [record];
    }
    else {
        return newRecords;
    }
};
//# sourceMappingURL=pivotRecord.js.map