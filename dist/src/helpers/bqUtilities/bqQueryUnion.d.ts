import { IBigQuerySchema } from '../../clients';
export interface IBqSchemaUnionOptions {
    typeCoercionEnabled?: boolean;
}
export declare const bqSchemaUnion: (schemas: IBigQuerySchema[][], options?: IBqSchemaUnionOptions) => IBigQuerySchema[];
