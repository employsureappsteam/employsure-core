import { IBigQuerySchema } from '../../clients';
export declare function generateBqSchema(data: Array<{}>, schemaOverride?: Array<IBigQuerySchema>): IBigQuerySchema[];
