export interface IPivotOptions {
    srcColsRegexPattern: string;
    destColField: string;
    destColValue: string;
    pivotDataOnKey?: string;
}
export declare const pivotRecords: <T = {}, R = {}>(records: T[], opt: IPivotOptions) => R[];
export declare const pivotRecord: <T = {}, R = {}>(record: T, opt: IPivotOptions) => R[];
