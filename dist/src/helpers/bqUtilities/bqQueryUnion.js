"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
const defaultOptions = {
    typeCoercionEnabled: true
};
/* To mash two schemas together. Throw error on difference (i.e. different types) */
exports.bqSchemaUnion = (schemas, options = defaultOptions) => {
    var _a, _b;
    const finalFields = [];
    for (const fields of schemas) {
        for (const field of fields) {
            /* Find field if it exists in final schema already. */
            const existingField = lodash_1.default.find(finalFields, { name: field.name });
            if (existingField && ((_a = existingField) === null || _a === void 0 ? void 0 : _a.type) === field.type) {
                if (field.type === "RECORD") {
                    if (existingField.fields && field.fields) {
                        /* If existing field and is of RECORD type, do a union on the children fields */
                        existingField.fields = exports.bqSchemaUnion([existingField.fields, field.fields]);
                    }
                    else {
                        /* Catch errors if it's missing fields */
                        throw new Error(`${field.name} is "RECORD" type but missing child fields`);
                    }
                }
                else {
                    /* If existing field, isn't record, and same type, skip*/
                    continue;
                }
            }
            else if (existingField && ((_b = existingField) === null || _b === void 0 ? void 0 : _b.type) !== field.type) {
                if (options.typeCoercionEnabled) {
                    if (existingField.type === "STRING" && field.type === "TIMESTAMP") {
                        continue;
                    }
                }
                /* If existing field and same type, there is a conflict. Throw error */
                throw new Error(`${field.name} exists twice in final schema with conflicting types (${existingField.type}, ${field.type})`);
            }
            else {
                /* Field does not exist, add to final fields */
                finalFields.push(field);
            }
        }
    }
    return finalFields;
};
//# sourceMappingURL=bqQueryUnion.js.map