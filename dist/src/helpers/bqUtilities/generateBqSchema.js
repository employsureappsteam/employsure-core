"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bigquerySchemaGenerator_1 = __importDefault(require("./bigquerySchemaGenerator"));
const bqQueryUnion_1 = require("./bqQueryUnion");
function generateBqSchema(data, schemaOverride = []) {
    /* Generate schemas for every record and union them */
    const schemasGenerated = data.map(d => bigquerySchemaGenerator_1.default(d));
    const schema = bqQueryUnion_1.bqSchemaUnion([schemaOverride, ...schemasGenerated]);
    return schema;
}
exports.generateBqSchema = generateBqSchema;
//# sourceMappingURL=generateBqSchema.js.map