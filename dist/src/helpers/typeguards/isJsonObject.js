"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* Checks if Json object, as opposed to any javascript object (e.g. null) */
exports.isJsonObject = (e) => {
    return e !== undefined && typeof e === "object";
};
//# sourceMappingURL=isJsonObject.js.map