export declare const objectHasValuesOf: <T>(obj: {
    [x: string]: T;
}, tg: (e: T) => e is T) => obj is {
    [x: string]: T;
};
