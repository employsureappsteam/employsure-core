"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const isJsonObject_1 = require("./isJsonObject");
exports.objectHasValuesOf = (obj, tg) => {
    return isJsonObject_1.isJsonObject(obj)
        && Object.values(obj).map(tg).reduce(areAllTrue, true);
};
const areAllTrue = (p, c) => p && c;
//# sourceMappingURL=objectHasValuesOf.js.map