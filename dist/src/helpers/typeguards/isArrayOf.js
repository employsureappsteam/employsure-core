"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isArrayOf = (element, elementTypeGuard) => {
    return (Array.isArray(element) &&
        element.map(r => elementTypeGuard(r))
            .reduce((p, c) => p && c, true));
};
//# sourceMappingURL=isArrayOf.js.map