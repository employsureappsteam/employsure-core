export declare const isArrayOf: <T>(element: T[], elementTypeGuard: (element: T) => element is T) => element is T[];
