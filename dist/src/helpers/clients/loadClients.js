"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const clients_1 = require("../../clients");
const lib_1 = require("../../lib");
const errors_1 = require("../../models/errors");
const models_1 = require("./models");
/**
 *
 * @param creds SSM locations/other config for clients
 */
async function loadClients(creds, options) {
    /* Fetch necessary secrets. */
    const ssmParams = [];
    if (creds.salesforce) {
        const sf = creds.salesforce;
        ssmParams.push(sf.clientId);
        ssmParams.push(sf.clientSecret);
        ssmParams.push(sf.username);
        ssmParams.push(sf.password);
    }
    if (creds.bigquery) {
        const bq = creds.bigquery;
        ssmParams.push(bq.clientEmail);
        ssmParams.push(bq.privateKey);
    }
    const secrets = await lib_1.GetSecretParams(ssmParams);
    /* Prepare clients. */
    const clients = {};
    if (creds.salesforce) {
        const sf = creds.salesforce;
        const salesforce = new clients_1.SalesforceClient({
            clientId: secrets[sf.clientId],
            clientSecret: secrets[sf.clientSecret],
            username: secrets[sf.username],
            password: secrets[sf.password],
            environment: sf.environment,
            ...options
        });
        clients.salesforce = salesforce;
    }
    if (creds.bigquery) {
        const bq = creds.bigquery;
        const bigQueryClient = new clients_1.BigQueryClient(creds.bigquery.project, {
            client_email: secrets[bq.clientEmail],
            private_key: secrets[bq.privateKey]
        }, options);
        clients.bigquery = bigQueryClient;
    }
    return clients;
}
exports.loadClients = loadClients;
/**
 * Loads bigquery client. Uses default creds for a project if given string.
 *
 * @param creds
 */
exports.loadBigQueryClient = async (creds, options) => {
    if (typeof creds === "string") {
        if (creds === 'lead-scoring-model') {
            creds = {
                clientEmail: '/BigQuery/Prod/ClientEmail',
                privateKey: '/BigQuery/Prod/PrivateKeyWithoutNewline',
                project: 'lead-scoring-model'
            };
        }
        else {
            throw new models_1.LoadClientError(`No BigQuery client config for ${creds}`);
        }
    }
    const { bigquery } = await loadClients({ bigquery: creds }, options);
    return bigquery;
};
/**
 * Loads Salesforce client using simple labels.
 */
exports.loadSalesforceClient = async (region, stage, clientOptions) => {
    /* Default app for regions. */
    let app = 'employsure-api';
    if (region === 'brighthr') {
        app = 'engineering-integration';
    }
    return exports.loadSalesforceClientFromLabel({
        region,
        stage,
        user: 'engineering-service',
        app
    }, clientOptions);
};
/* Load Salesforce client using labels with options for which app and user to use. */
exports.loadSalesforceClientFromLabel = async (labels, clientOptions) => {
    const creds = evaluateSalesforceCredDefaults(labels);
    try {
        const { salesforce } = await loadClients({ salesforce: creds }, clientOptions);
        return salesforce;
    }
    catch (error) {
        if (error.name === "InvalidParameterError") {
            console.error(error.message);
            throw new errors_1.EmploysureError('InvalidSalesforceSsmCredentials', `Failed to load salesforce client from labels ${JSON.stringify(labels)} as the SSM references are invalid. See logs for details.`);
        }
        else {
            throw error;
        }
    }
};
/**
 * Using a set of business domain labels create a credential interface for
 * the Salesforce client to use.
 *
 * @param label Set of business domain labels
 * @returns
 */
function evaluateSalesforceCredDefaults(label) {
    /* Setup stage. */
    let ssmStage = label.stage;
    let environment;
    if (label.stage === 'Prod') {
        environment = 'production';
    }
    else {
        environment = 'sandbox';
    }
    /* Setup region. */
    let ssmOrg;
    if (label.region === 'aus') {
        ssmOrg = 'Salesforce';
    }
    else if (label.region === 'nz') {
        ssmOrg = 'SalesforceNZ';
    }
    else if (label.region === 'brighthr') {
        ssmOrg = 'SalesforceBright';
    }
    else {
        throw new models_1.LoadClientError(`No default Salesforce region for ${label.region}`);
    }
    /* Setup Org user */
    let ssmOrgUser;
    if (label.user === 'engineering-service') {
        ssmOrgUser = 'EngineeringService';
    }
    else {
        throw new models_1.LoadClientError(`No default Salesforce user for ${label.user}`);
    }
    /* Setup Org app */
    let ssmOrgApp;
    if (label.app === 'employsure-api') {
        ssmOrgApp = 'EmploysureApiApp';
    }
    else if (label.app === 'engineering-integration') {
        ssmOrgApp = 'EngineeringIntegrationApp';
    }
    else {
        throw new models_1.LoadClientError(`No default Salesforce app for ${label.app}`);
    }
    /* Build credentials. */
    return {
        username: `/${ssmOrg}${ssmOrgUser}/${ssmStage}/Username`,
        password: `/${ssmOrg}${ssmOrgUser}/${ssmStage}/Password`,
        clientId: `/${ssmOrg}${ssmOrgApp}/${ssmStage}/Id`,
        clientSecret: `/${ssmOrg}${ssmOrgApp}/${ssmStage}/Secret`,
        environment
    };
}
//# sourceMappingURL=loadClients.js.map