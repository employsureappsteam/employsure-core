import { BigQueryClient, SalesforceClient } from "../../clients";
import { IBigQueryClientSsmCreds, IClientCreds, IClientOptions, IEmploysureClients, ISalesforceOrgLabel, SalesforceRegion } from "./models";
/**
 *
 * @param creds SSM locations/other config for clients
 */
export declare function loadClients(creds: IClientCreds, options?: IClientOptions): Promise<IEmploysureClients>;
/**
 * Loads bigquery client. Uses default creds for a project if given string.
 *
 * @param creds
 */
export declare const loadBigQueryClient: (creds: string | IBigQueryClientSsmCreds, options?: IClientOptions | undefined) => Promise<BigQueryClient>;
/**
 * Loads Salesforce client using simple labels.
 */
export declare const loadSalesforceClient: (region: SalesforceRegion, stage: string, clientOptions?: IClientOptions | undefined) => Promise<SalesforceClient>;
export declare const loadSalesforceClientFromLabel: (labels: ISalesforceOrgLabel, clientOptions?: IClientOptions | undefined) => Promise<SalesforceClient>;
