"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function IsSalesforceOrgLabel(e) {
    return typeof e === "object"
        && typeof e.region === "string"
        && typeof e.stage === "string"
        && (e.user === undefined || typeof e.region === "string");
}
exports.IsSalesforceOrgLabel = IsSalesforceOrgLabel;
class LoadClientError extends Error {
    constructor(message) {
        super(message);
        this.name = "LoadClientError";
    }
}
exports.LoadClientError = LoadClientError;
//# sourceMappingURL=models.js.map