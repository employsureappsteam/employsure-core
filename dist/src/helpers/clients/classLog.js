"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SEPERATOR = "::";
const ENV_CLIENT_OPTIONS = {
    verboseLogging: process.env.EMPLOYSURE_VERBOSE_LOGGING === 'true',
    verboseLoggingLevel: process.env.EMPLOYSURE_VERBOSE_LOGGING_LEVEL
};
/**
 * Helper function for logging from within a class.
 *
 * Intended to help debugging state of functions.
 */
exports.classLog = (className, method, message, logLevel = 'info', userOptions) => {
    /* Overwrite user options with server options. */
    const options = {
        ...userOptions,
        ...ENV_CLIENT_OPTIONS,
    };
    if (options.verboseLogging || logLevel === 'error') {
        const msg = `${className}${SEPERATOR}${method}${SEPERATOR}${message}`;
        if (logLevel === 'info' && shouldLogInfo(options)) {
            console.info(msg);
        }
        else if (logLevel === 'warn' && shouldLogWarn(options)) {
            console.warn(msg);
        }
        else if (logLevel === 'error' && shouldLogError(options)) {
            console.error(msg);
        }
    }
};
/* True if the user requested info logs. */
const shouldLogInfo = (userConfig) => {
    var _a;
    return ((_a = userConfig) === null || _a === void 0 ? void 0 : _a.verboseLoggingLevel) === 'info';
};
/* True if the user requested info logs or warning. */
const shouldLogWarn = (userConfig) => {
    var _a, _b;
    return ((_a = userConfig) === null || _a === void 0 ? void 0 : _a.verboseLoggingLevel) === 'info'
        || ((_b = userConfig) === null || _b === void 0 ? void 0 : _b.verboseLoggingLevel) === 'warn';
};
/* True if the user requested info logs. */
const shouldLogError = (userConfig) => {
    /* Always log errors. */
    return true;
    // return userConfig?.verboseLoggingLevel === 'info'
    //     || userConfig?.verboseLoggingLevel === 'warn'
    //     || userConfig?.verboseLoggingLevel === 'error';
};
//# sourceMappingURL=classLog.js.map