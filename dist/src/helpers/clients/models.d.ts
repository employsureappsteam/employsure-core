import { BigQueryClient, SalesforceClient } from "../../clients";
declare type EmploysureSsmPath = string;
export interface IClientCreds {
    salesforce?: ISalesforceCreds;
    bigquery?: IBigQueryClientSsmCreds;
    azure?: IAzureClientSsmCreds;
}
export interface IEmploysureClients {
    salesforce?: SalesforceClient;
    bigquery?: BigQueryClient;
}
export interface IAzureClientSsmCreds {
    tenantId: EmploysureSsmPath;
    clientId: EmploysureSsmPath;
    clientSecret: EmploysureSsmPath;
}
export interface IBigQueryClientSsmCreds {
    clientEmail: EmploysureSsmPath;
    privateKey: EmploysureSsmPath;
    project: string;
}
export interface ISalesforceCreds {
    username: EmploysureSsmPath;
    password: EmploysureSsmPath;
    clientId: EmploysureSsmPath;
    clientSecret: EmploysureSsmPath;
    environment?: 'production' | 'sandbox';
}
export declare function IsSalesforceOrgLabel(e: any): e is ISalesforceOrgLabel;
export declare type SalesforceRegion = 'aus' | 'nz' | 'brighthr';
export declare type SalesforceStage = 'Prod' | 'Full' | 'Mini' | string;
export declare type SalesforceUser = 'engineering-service';
export declare type SalesforceApp = 'employsure-api' | 'engineering-integration';
export interface ISalesforceOrgLabel {
    region: SalesforceRegion;
    stage: SalesforceStage;
    user?: SalesforceUser;
    app?: SalesforceApp;
}
export declare type VerboseLoggingLevel = 'info' | 'warn' | 'error';
export interface IClientOptions {
    /** Enables logging of client interals. */
    verboseLogging?: boolean;
    verboseLoggingLevel?: VerboseLoggingLevel;
    /** The client will use SSM to cache the login token as there is a Salesforce
     * API limit on how many times a particular app/user can request a new token.
     * To disable this behaviour set disableDistributedToken to true.
     * Default: false
     **/
    disableDistributedAuth?: boolean;
}
export declare class LoadClientError extends Error {
    name: string;
    constructor(message: string);
}
export {};
