import { IClientOptions, VerboseLoggingLevel } from "./models";
/**
 * Helper function for logging from within a class.
 *
 * Intended to help debugging state of functions.
 */
export declare const classLog: (className: string, method: string, message: string, logLevel?: VerboseLoggingLevel, userOptions?: IClientOptions | undefined) => void;
