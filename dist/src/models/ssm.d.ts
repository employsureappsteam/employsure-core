export declare type ParametersMap = {
    [path: string]: string;
};
export declare class InvalidParameterError extends Error {
    name: string;
    constructor(parameters: Array<string>);
}
