"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class InvalidParameterError extends Error {
    constructor(parameters) {
        super(`These ${parameters.length} parameters are invalid: ${parameters.join(", ")}. Please check they exist.`);
        this.name = "InvalidParameterError";
    }
}
exports.InvalidParameterError = InvalidParameterError;
//# sourceMappingURL=ssm.js.map