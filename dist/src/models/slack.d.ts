export declare type SlackAppNames = 'Provisioning' | 'AWSNotificationService' | 'Development';
export declare type OAuthTokenKey = {
    [app in SlackAppNames]: string;
};
export declare type SigningSecretKey = {
    [app in SlackAppNames]: string;
};
export declare type SlackChannelNames = 'Provisioning' | 'PureCloudSync' | 'EngAlertsDev' | 'EngAlertsProd' | 'Engineering';
export declare type SlackChannelIDs = 'GCPUFAE0P' | 'CBYVBL4CX' | 'GCH7GC5EC' | 'CDNU1LH7Z' | 'GCJ6Y1P3K';
export declare type ChannelIDMap = {
    [channel in SlackChannelNames]: SlackChannelIDs;
};
