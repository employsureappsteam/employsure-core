"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** A generic error to use for Employsure application errors.
 * All errors using this class should be server errors. */
class EmploysureError extends Error {
    constructor(errorName, message) {
        super(message);
        this.isEmploysureError = true;
        this.name = `EmploysureError_${errorName}`;
    }
}
exports.EmploysureError = EmploysureError;
//# sourceMappingURL=errors.js.map