export declare type SfArnsMap = {
    [name: string]: SfArns;
};
export declare type SfArns = 'arn:aws:states:ap-southeast-2:711143483997:stateMachine:SyncPureCloudDataStepFunctionsStateMachine-Fp9gomS8aALC';
