"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
/** Response to return to Apigee. */
class ApigeeResponse {
    constructor(status, data, userMessage) {
        this.StatusCode = constants_1.HttpStatusCodes[status];
        this.Data = data;
        this.UserMessage = userMessage;
    }
}
exports.ApigeeResponse = ApigeeResponse;
class ApigeeBadRequestResponse extends ApigeeResponse {
    constructor(error, context) {
        var _a;
        super("BAD_REQUEST");
        this.StatusCodeSuffix = (_a = constants_1.badRequestSuffixMap[error.name], (_a !== null && _a !== void 0 ? _a : constants_1.unknownBadRequestSuffix));
        this.ErrorName = error.name;
        this.UserMessage = error.message;
        this.RequestId = context.awsRequestId;
    }
}
exports.ApigeeBadRequestResponse = ApigeeBadRequestResponse;
class ApigeeServerErrorResponse extends ApigeeResponse {
    constructor(context) {
        super("SERVER_ERROR");
        this.RequestId = context.awsRequestId;
    }
}
exports.ApigeeServerErrorResponse = ApigeeServerErrorResponse;
/**
 * Error to throw if the lambda fails
 */
class LambdaError extends Error {
    /**
     * @param lambdaResponse lambda response. The status message is used as the error message
     */
    constructor(lambdaResponse) {
        super(`${lambdaResponse.StatusCode}`);
        this.name = "LambdaError";
        this.lambdaResponse = lambdaResponse;
    }
}
exports.LambdaError = LambdaError;
//# sourceMappingURL=lambda.js.map