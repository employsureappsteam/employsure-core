/** A generic error to use for Employsure application errors.
 * All errors using this class should be server errors. */
export declare class EmploysureError extends Error {
    isEmploysureError: boolean;
    constructor(errorName: string, message: string);
}
