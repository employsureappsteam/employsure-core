import { DynamoDB } from "aws-sdk";
export declare type ConfigFields = 'WorkplaceAccessToken' | 'FirestoreCertificate' | 'BigQueryClientEmail' | 'BigQueryPrivateKey' | 'PingboardClientID' | 'PingboardClientSecret' | 'FlareHRPrimaryKey' | 'FacebookAppSecret' | 'FacebookAppId' | 'FlareHrKey';
export interface IConfig {
    readonly PingboardClientID?: string;
    readonly PingboardClientSecret?: string;
    readonly BigQueryClientEmail?: string;
    readonly BigQueryPrivateKey?: string;
    readonly FlareHrKey?: string;
    readonly WorkplaceAccessToken?: string;
    readonly FirestoreCertificate?: string;
    readonly FlareHRPrimaryKey?: string;
    readonly FacebookAppId?: string;
    readonly FacebookAppSecret?: string;
}
export interface ISlackChannel {
    Name: string;
    Id: string;
}
export declare class SlackChannel {
    private _record;
    constructor(record: DynamoDB.AttributeMap);
    get Name(): string | undefined;
    get Id(): string | undefined;
}
export interface ISnsTopicSlackConfig {
    Enabled: boolean;
    SlackChannelName: string;
    SlackAppName: string;
}
export declare class SnsTopicSlackConfig implements ISnsTopicSlackConfig {
    private _record;
    constructor(record: DynamoDB.AttributeMap);
    get Enabled(): boolean;
    get SlackAppName(): string;
    get SlackChannelName(): string;
    get SnsTopicArn(): string;
}
