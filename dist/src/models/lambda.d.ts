import { Context } from "aws-lambda";
import { BadRequestError, EmploysureErrorResponse } from "../lib/lambda/models";
/**
 * Event structure for workplace-provision lambda
 */
export interface LambdaEvent<T> {
    RetryCount?: number;
    Source?: string;
    Author?: string;
    Stage?: "Dev" | "Prod";
    options: T;
}
export declare type LambdaNameMap = {
    [name: string]: LambdaFunctionNames;
};
export declare type LambdaFunctionNames = 'slack-utilities-dev-postSlackMessage' | 'slack-utilities-dev-updateSlackMessage' | 'fwc-lead-scrapper-dev-run' | 'nz-utils-dev-leadScraper' | 'provisioning-dev-provision-workplace' | 'provisioning-prod-provision-workplace';
export declare type HttpStatusSuccess = "OK" | "MULTI_STATUS";
export declare type HttpStatusBadRequest = "BAD_REQUEST";
export declare type HttpStatusServerError = "SERVER_ERROR";
export declare type HttpStatusErrors = HttpStatusBadRequest | HttpStatusServerError;
export declare type HttpStatusAll = HttpStatusSuccess | HttpStatusErrors;
/** Response to return to Apigee. */
export declare class ApigeeResponse<T = any> {
    StatusCode: number;
    Data?: T;
    StatusCodeSuffix?: string;
    ErrorName?: string;
    UserMessage?: string;
    RequestId?: string;
    constructor(status: HttpStatusAll, data?: T, userMessage?: string);
}
export declare class ApigeeBadRequestResponse extends ApigeeResponse<EmploysureErrorResponse> {
    constructor(error: BadRequestError, context: Context);
}
export declare class ApigeeServerErrorResponse extends ApigeeResponse<EmploysureErrorResponse> {
    constructor(context: Context);
}
/**
 * Error to throw if the lambda fails
 */
export declare class LambdaError<T> extends Error {
    private lambdaResponse;
    /**
     * @param lambdaResponse lambda response. The status message is used as the error message
     */
    constructor(lambdaResponse: ApigeeResponse<T>);
}
