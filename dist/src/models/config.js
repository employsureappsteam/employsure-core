"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SlackChannel {
    constructor(record) {
        this._record = record;
    }
    get Name() {
        return this._record.Name.S;
    }
    get Id() {
        return this._record.Id.S;
    }
}
exports.SlackChannel = SlackChannel;
class SnsTopicSlackConfig {
    constructor(record) {
        this._record = record;
    }
    get Enabled() {
        return this._record.Enabled.BOOL;
    }
    get SlackAppName() {
        return this._record.SlackAppName.S;
    }
    get SlackChannelName() {
        return this._record.SlackChannelName.S;
    }
    get SnsTopicArn() {
        return this._record.SnsTopicArn.S;
    }
}
exports.SnsTopicSlackConfig = SnsTopicSlackConfig;
//# sourceMappingURL=config.js.map