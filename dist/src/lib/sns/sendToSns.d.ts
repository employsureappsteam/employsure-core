import { SNS } from 'aws-sdk';
import { SnsMessage, TopicArns } from '../../models/sns';
declare type SendToSnsOptions = {
    ignoreStageCheck: boolean;
};
export declare function supressNotifications(): void;
/**
 *
 * @param snsTopic arn of topic to send notification to. Use SnsTopics const.
 * @param subject title of the notification. Used as subject in email.
 * @param messages messages to be sent. Will be appended together in email.
 */
export declare function sendToSns(snsTopic: TopicArns | string, subject: string, messages: SnsMessage[], options?: SendToSnsOptions): Promise<"SNS Notifications Supressed" | import("aws-sdk/lib/request").PromiseResult<SNS.PublishResponse, import("aws-sdk").AWSError>>;
export {};
