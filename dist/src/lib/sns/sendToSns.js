"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aws_sdk_1 = require("aws-sdk");
const constants_1 = require("../../constants");
const config = {
    supressAllNotifications: false
};
function supressNotifications() {
    config.supressAllNotifications = true;
}
exports.supressNotifications = supressNotifications;
/**
 *
 * @param snsTopic arn of topic to send notification to. Use SnsTopics const.
 * @param subject title of the notification. Used as subject in email.
 * @param messages messages to be sent. Will be appended together in email.
 */
async function sendToSns(snsTopic, subject, messages, options) {
    var _a, _b;
    const client = new aws_sdk_1.SNS({ region: constants_1.AWS_REGION_DEFAULT });
    if (config.supressAllNotifications) {
        return "SNS Notifications Supressed";
    }
    if (messages.length === 0) {
        throw new Error("No messages. Cannot send empty notification.");
    }
    /* Send everything not in prod to dev channel. */
    let topicArn = snsTopic;
    if (((_a = options) === null || _a === void 0 ? void 0 : _a.ignoreStageCheck) !== true && process.env.stage !== "Prod") {
        topicArn = constants_1.SnsTopics.EngErrorsDev;
    }
    /* Append subject with Stage if it's not production. */
    let stagePrefix = '';
    if (((_b = options) === null || _b === void 0 ? void 0 : _b.ignoreStageCheck) !== true && process.env.stage !== "Prod") {
        // topicArn = SnsTopics.EngErrorsDev
        stagePrefix = `${process.env.stage} | `;
    }
    const defaultMessage = messages.map(e => {
        return `${e.title}\n${e.message}${e.details ? '\n\n' + e.details : ""}`;
    }).join('\n\n');
    const publishMessage = {
        default: defaultMessage,
        lambda: JSON.stringify({ Errors: [], Logs: messages }),
        email: `${stagePrefix}${subject}\n${defaultMessage}\n`
    };
    return client.publish({
        TopicArn: topicArn,
        Subject: `${stagePrefix}${subject}`,
        Message: JSON.stringify(publishMessage),
        MessageStructure: 'json'
    }).promise();
}
exports.sendToSns = sendToSns;
//# sourceMappingURL=sendToSns.js.map