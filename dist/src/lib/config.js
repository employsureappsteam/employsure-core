"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aws_sdk_1 = require("aws-sdk");
const constants_1 = require("../constants");
const models_1 = require("../models");
const dynamodb_1 = require("./dynamodb");
const ssm = new aws_sdk_1.SSM({
    region: constants_1.AWS_REGION_DEFAULT,
    apiVersion: constants_1.AWS_SSM_API_VERSION
});
function GetConfig(params) {
    if (params.length === 0) {
        throw new Error('params is empty');
    }
    const FieldToParam = process.env.stage === "Prod" ? ProdConfigFieldToParam : DevConfigFieldToParam;
    const Names = params.map(param => {
        if (FieldToParam[param]) {
            return FieldToParam[param];
        }
        else {
            throw new Error(`Field ${param} missing from mapping.`);
        }
    });
    let SsmParams = { Names, WithDecryption: true };
    return ssm.getParameters(SsmParams).promise()
        .then((data) => {
        const InvalidParameters = data.InvalidParameters ? data.InvalidParameters : [];
        const Parameters = data.Parameters ? data.Parameters : [];
        if (InvalidParameters.length > 0) {
            throw new models_1.InvalidParameterError(InvalidParameters);
        }
        const ParamToValue = Parameters.reduce((obj, param) => {
            obj[param.Name] = param.Value;
            return obj;
        }, {});
        return params.reduce((obj, field) => {
            obj[field] = ParamToValue[FieldToParam[field]];
            return obj;
        }, {});
    });
}
exports.GetConfig = GetConfig;
async function GetSlackChannelID(name) {
    return new models_1.SlackChannel((await dynamodb_1.getDynamoTableRow('SlackChannelsIDs', 'Name', name)));
}
exports.GetSlackChannelID = GetSlackChannelID;
async function GetSnsTopicSlackConfig(SnsTopicArn) {
    return new models_1.SnsTopicSlackConfig((await dynamodb_1.getDynamoTableRow('SnsTopicSlackMappings', 'SnsTopicArn', SnsTopicArn)));
}
exports.GetSnsTopicSlackConfig = GetSnsTopicSlackConfig;
const DevConfigFieldToParam = {
    WorkplaceAccessToken: '/FacebookWorkplace/Dev/AccessToken',
    FirestoreCertificate: "/Firestore/EmployeeProvisioning/Dev/Certificate",
    BigQueryClientEmail: "/BigQuery/Dev/ClientEmail",
    BigQueryPrivateKey: "/BigQuery/Dev/PrivateKey",
    FlareHrKey: "/FlareHR/Dev/PrimaryKey",
    PingboardClientID: "/Pingboard/Dev/ClientID",
    PingboardClientSecret: "/Pingboard/Dev/ClientSecret",
    FlareHRPrimaryKey: "/FlareHR/Dev/PrimaryKey",
    FacebookAppId: "/FacebookLeadApp/Dev/AppID",
    FacebookAppSecret: "/FacebookLeadApp/Dev/AppSecret"
};
const ProdConfigFieldToParam = {
    WorkplaceAccessToken: '/FacebookWorkplace/Prod/AccessToken',
    FirestoreCertificate: '/Firestore/EmployeeProvisioning/Prod/Certificate',
    BigQueryClientEmail: '/BigQuery/Prod/ClientEmail',
    BigQueryPrivateKey: '/BigQuery/Prod/PrivateKey',
    FlareHrKey: "/FlareHR/Prod/PrimaryKey",
    PingboardClientID: '/Pingboard/Prod/ClientID',
    PingboardClientSecret: '/Pingboard/Prod/ClientSecret',
    FlareHRPrimaryKey: '/FlareHR/Prod/PrimaryKey',
    FacebookAppId: "/FacebookLeadApp/Prod/AppID",
    FacebookAppSecret: "/FacebookLeadApp/Prod/AppSecret"
};
//# sourceMappingURL=config.js.map