"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aws_sdk_1 = require("aws-sdk");
const constants_1 = require("../constants");
const classLog_1 = require("../helpers/clients/classLog");
const models_1 = require("../models");
const MAX_RETRIES = 7;
const ssm = new aws_sdk_1.SSM({
    region: constants_1.AWS_REGION_DEFAULT,
    apiVersion: constants_1.AWS_SSM_API_VERSION,
    maxRetries: MAX_RETRIES,
    retryDelayOptions: {
        base: 250,
        customBackoff: (count, error) => {
            const backOff = 120 * (2 ** count) + Math.floor(Math.random() * 600);
            classLog_1.classLog('EmploysureSsm', 'GetSecretParams', `Retry ${count + 1}/${MAX_RETRIES}. Backoff: ${backOff}ms. Error: ${error}`, 'warn');
            return backOff;
        }
    }
});
/**
 * Simple wrapper over SSM's get-parameters to return the result in a key-value
 * pairing of the parameter's Name to the parameter's Value. For encrypted values.
 *
 * E.g.: GetSecretParams(['/Service/Dev']) would get you:
 * {
 *  "/Service/Dev": "SecretValue"
 * }
 *
 * @param Names
 */
async function GetSecretParams(Names) {
    try {
        if (Names.length === 0) {
            throw new Error('Names is empty');
        }
        let SsmParams = { Names, WithDecryption: true };
        const data = await ssm.getParameters(SsmParams).promise();
        const InvalidParameters = data.InvalidParameters ? data.InvalidParameters : [];
        const Parameters = data.Parameters ? data.Parameters : [];
        if (InvalidParameters.length > 0) {
            throw new models_1.InvalidParameterError(InvalidParameters);
        }
        return Parameters.reduce((obj, param) => {
            obj[param.Name] = param.Value;
            return obj;
        }, {});
    }
    catch (error) {
        classLog_1.classLog('EmploysureSsm', 'GetSecretParams', `${error}`, 'error');
        throw error;
    }
}
exports.GetSecretParams = GetSecretParams;
//# sourceMappingURL=ssm.js.map