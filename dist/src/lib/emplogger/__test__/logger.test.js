"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const __1 = require("..");
describe('Emplogger', () => {
    it('Log history should keep log entries', () => {
        const log = new __1.EmpLogger({ disablePrinting: true });
        log.log('hello');
        const hist = log.getLogHistory();
        chai_1.expect(hist).to.have.length(1);
    });
    it('false date option should disable timestamp', () => {
        const log = new __1.EmpLogger({ disablePrinting: true, date: false });
        log.log('hello');
        const hist = log.getLogHistory();
        chai_1.expect(hist[0]).to.equal("hello");
    });
    it('Should display prefix', () => {
        const log = new __1.EmpLogger({ disablePrinting: true, date: false, prefix: 'test: ' });
        log.log('hello');
        const hist = log.getLogHistory();
        chai_1.expect(hist[0]).to.equal("test: hello");
    });
    it('Should display prefix and timestamp', () => {
        const log = new __1.EmpLogger({ disablePrinting: true, prefix: 'test: ' });
        log.log('hello');
        const hist = log.getLogHistory();
        chai_1.expect(hist[0]).to.match(new RegExp(/\[\S+\] test: hello/));
    });
    it('Function option should overwrite global', () => {
        const log = new __1.EmpLogger({ disablePrinting: true, prefix: 'test: ' });
        log.log('hello', { prefix: 'world-' });
        const hist = log.getLogHistory();
        chai_1.expect(hist[0]).to.match(new RegExp(/\[\S+\] world-hello/));
    });
});
//# sourceMappingURL=logger.test.js.map