export interface IEmpLoggerOptions {
    date?: boolean;
    disablePrinting?: boolean;
    prefix?: string;
}
/**
 * Little logging class to help me out.
 */
export declare class EmpLogger {
    private history;
    private options;
    constructor(options?: IEmpLoggerOptions);
    log(s: string, options?: IEmpLoggerOptions): void;
    getLogHistory(): string[];
    clearLogHistory(): void;
}
