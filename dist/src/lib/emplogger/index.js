"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Little logging class to help me out.
 */
class EmpLogger {
    constructor(options) {
        this.history = [];
        this.options = {
            date: true,
            disablePrinting: false
        };
        /* Overwrite obj defaults with provided options. */
        this.options = { ...this.options, ...options };
    }
    log(s, options) {
        var _a;
        const dateStr = (new Date).toISOString();
        /* Function options overwrite global. */
        options = { ...this.options, ...options };
        let _s = s;
        if (options.prefix !== undefined) {
            _s = `${options.prefix}${_s}`;
        }
        if (_a = options.date, (_a !== null && _a !== void 0 ? _a : true)) {
            _s = `[${dateStr}] ${_s}`;
        }
        if (!options.disablePrinting) {
            console.log(_s);
        }
        this.history.push(_s);
    }
    getLogHistory() {
        return this.history;
    }
    clearLogHistory() {
        this.history = [];
    }
}
exports.EmpLogger = EmpLogger;
//# sourceMappingURL=index.js.map