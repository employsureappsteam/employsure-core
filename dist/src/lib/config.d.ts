import { ConfigFields, IConfig, SlackChannel, SnsTopicSlackConfig } from "../models";
export declare function GetConfig(params: Array<ConfigFields>): Promise<IConfig>;
export declare function GetSlackChannelID(name: string): Promise<SlackChannel>;
export declare function GetSnsTopicSlackConfig(SnsTopicArn: string): Promise<SnsTopicSlackConfig>;
