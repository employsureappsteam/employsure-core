import { DynamoDB } from "aws-sdk";
export declare function getDynamoTableRow(TableName: string, PrimaryKey: string, IndexValue: string): Promise<DynamoDB.AttributeMap | null>;
