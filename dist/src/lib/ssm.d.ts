import { ParametersMap } from "../models";
/**
 * Simple wrapper over SSM's get-parameters to return the result in a key-value
 * pairing of the parameter's Name to the parameter's Value. For encrypted values.
 *
 * E.g.: GetSecretParams(['/Service/Dev']) would get you:
 * {
 *  "/Service/Dev": "SecretValue"
 * }
 *
 * @param Names
 */
export declare function GetSecretParams(Names: Array<string>): Promise<ParametersMap>;
