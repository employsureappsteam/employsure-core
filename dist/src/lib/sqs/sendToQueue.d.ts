import { SQS } from "aws-sdk";
import { ISendToSqsOptions } from "./models";
/**
 *
 * @param QueueUrl QueueUrl to send messages to
 * @param messages Array to messages to send
 * @param options Employsure options.
 * @returns
 */
export declare const sendToQueue: (QueueUrl: string, messages: any[], options?: ISendToSqsOptions | undefined) => Promise<{
    Success: SQS.SendMessageBatchResultEntryList;
    Failed: SQS.BatchResultErrorEntryList;
}>;
