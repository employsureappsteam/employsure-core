"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const aws_sdk_1 = require("aws-sdk");
const lodash_1 = __importDefault(require("lodash"));
const constants_1 = require("../../constants");
const helpers_1 = require("../../helpers");
/**
 *
 * @param QueueUrl QueueUrl to send messages to
 * @param messages Array to messages to send
 * @param options Employsure options.
 * @returns
 */
exports.sendToQueue = async (QueueUrl, messages, options) => {
    var _a, _b, _c;
    /* Prepare data for SQS Queue */
    const sqsMessages = messages.map((message, index) => {
        var _a, _b;
        const MessageBody = JSON.stringify(message);
        let Id = `${index}`;
        try {
            if (((_a = options) === null || _a === void 0 ? void 0 : _a.messageIndexField) !== undefined && typeof message[options.messageIndexField] === "string") {
                Id = message[options.messageIndexField];
            }
        }
        catch (error) {
            helpers_1.classLog(`sqs`, `sendToQueue`, `Failed to set Id using value from ${(_b = options) === null || _b === void 0 ? void 0 : _b.messageIndexField}`, `warn`);
        }
        return { MessageBody, Id };
    });
    const chunkSize = (_b = (_a = options) === null || _a === void 0 ? void 0 : _a.chunkSize, (_b !== null && _b !== void 0 ? _b : 10));
    const messagesChunked = lodash_1.default.chunk(sqsMessages, chunkSize);
    const sqs = new aws_sdk_1.SQS({ region: constants_1.AWS_REGION_DEFAULT });
    const Success = [];
    const Failed = [];
    if (((_c = options) === null || _c === void 0 ? void 0 : _c.sendSequentially) === true) {
        let count = 0;
        for (const Entries of messagesChunked) {
            count = count + 1;
            const r = await sqs.sendMessageBatch({
                QueueUrl,
                Entries
            }).promise();
            Success.push(...r.Successful);
            Failed.push(...r.Failed);
        }
    }
    else {
        const promises = messagesChunked.map(Entries => {
            const param = { QueueUrl, Entries };
            return sqs.sendMessageBatch(param).promise();
        });
        const results = await Promise.all(promises);
        for (const r of results) {
            Success.push(...r.Successful);
            Failed.push(...r.Failed);
        }
    }
    return {
        Success,
        Failed
    };
};
//# sourceMappingURL=sendToQueue.js.map