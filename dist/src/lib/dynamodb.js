"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aws_sdk_1 = require("aws-sdk");
const constants_1 = require("../constants");
const dynamodb = new aws_sdk_1.DynamoDB({
    region: constants_1.AWS_REGION_DEFAULT
});
async function getDynamoTableRow(TableName, PrimaryKey, IndexValue) {
    const params = {
        TableName,
        KeyConditionExpression: `#primaryKey = :indexValue`,
        ExpressionAttributeValues: {
            ":indexValue": {
                S: IndexValue
            }
        },
        ExpressionAttributeNames: {
            "#primaryKey": PrimaryKey
        },
    };
    const table = await dynamodb.query(params).promise();
    if (table.Items && table.Items.length > 0) {
        return table.Items[0];
    }
    else {
        return null;
    }
}
exports.getDynamoTableRow = getDynamoTableRow;
//# sourceMappingURL=dynamodb.js.map