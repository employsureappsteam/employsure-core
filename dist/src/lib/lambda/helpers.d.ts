import { Context, SQSEvent, SQSRecord, SNSEvent, SNSEventRecord } from "aws-lambda";
import { GenericSnsHandlerOptions, GenericSqsHandlerOptions, MessageResultList, PostProcessingOptions } from "./models";
/**
 * Builds a log stream link from lambda context.
 *
 * AWS CloudWatch has a unique encoding. This doesn't try to understand it
 * only copy it. For better filtering further investigation is needed.
 *
 * Example: '$2522' is a double quote.
 *
 * @param context Lambda context
 * @returns a url to access the log stream for relevant context.
 */
export declare const buildLogStreamLink: (context: Context) => string;
export declare function parseSqsMessage<T = any>(event: SQSEvent, options?: GenericSqsHandlerOptions<T>): {
    parsed: {
        record: SQSRecord;
        parsedBody: T;
    }[];
    errors: {
        record: SQSRecord;
        message: string;
    }[];
};
export declare function parseSqsMessageV2<T>(event: SQSEvent, options?: GenericSqsHandlerOptions<T>): {
    validRecords: {
        record: SQSRecord;
        payload: T;
    }[];
    errors: {
        record: SQSRecord;
        message: string;
    }[];
};
export declare function parseSnsMessage<T>(event: SNSEvent, options?: GenericSnsHandlerOptions<T>): {
    validRecords: {
        record: SNSEventRecord;
        payload: T;
    }[];
    errors: {
        record: SNSEventRecord;
        message: string;
    }[];
};
export declare const postProcessMessageResults: (options: PostProcessingOptions | undefined, results: MessageResultList, context: Context) => Promise<void>;
export declare const parseAnyToString: (unknownVar: any) => string | undefined;
