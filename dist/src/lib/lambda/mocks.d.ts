import { Context } from 'aws-lambda';
/**
 * @returns mock object for AWS Lambda Context
 */
export declare const createMockLambdaContext: (userMock?: Context | undefined) => Context;
