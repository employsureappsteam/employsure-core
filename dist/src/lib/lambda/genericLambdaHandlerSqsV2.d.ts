import { SQSEvent, Context } from "aws-lambda";
import { GenericAppFunction, GenericSqsHandlerOptions } from "./models";
/** Parse and handle an SQS event. Returns status of function evaluation. */
export declare function genericSqsHandlerV2<T, G>(event: SQSEvent, context: Context, fn: GenericAppFunction<T, G>, options?: GenericSqsHandlerOptions<T>): Promise<string | void>;
