"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("uuid");
/**
 * @returns mock object for AWS Lambda Context
 */
exports.createMockLambdaContext = (userMock) => {
    const functionName = 'portalDocumentService-full-processNewDocument';
    const defaultMock = {
        "callbackWaitsForEmptyEventLoop": true,
        "functionVersion": "$LATEST",
        "functionName": functionName,
        "memoryLimitInMB": "256",
        "logGroupName": `/aws/lambda/${functionName}`,
        "logStreamName": "YYYY/MM/DD/[$LATEST]XXX",
        "invokedFunctionArn": `arn:aws:lambda:REGION:XXXXXXXXX:function:${functionName}`,
        "awsRequestId": `mock_${uuid_1.v4()}`,
        getRemainingTimeInMillis: () => 0,
        done: () => null,
        fail: () => null,
        succeed: () => null
    };
    return {
        ...userMock,
        ...defaultMock
    };
};
//# sourceMappingURL=mocks.js.map