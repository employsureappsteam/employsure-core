"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sendToSns_1 = require("../sns/sendToSns");
const helpers_1 = require("./helpers");
const typeguards_1 = require("./typeguards");
exports.runTimeErrorCodes = {
    InvalidEvent: "EXIT-ERROR-INVALID-EVENT",
    TooManyRecords: "EXIT-ERROR-TOO-MANY-RECORDS",
    InvalidMessage: "EXIT-ERROR-INVALID-MESSAGE",
    UnhandledError: "EXIT-ERROR-UNHANDLED",
};
exports.runTimeSuccessCode = {
    Ok: "EXIT-SUCCESS"
};
/** Parse and handle an SQS event. Returns status of function evaluation. */
async function genericSqsHandler(event, context, fn, options, recordLimit = 1) {
    var _a;
    if (!typeguards_1.instanceOfSqsEvent(event)) {
        const message = 'Invalid SQS Event';
        console.log(`${exports.runTimeErrorCodes.InvalidEvent} ${message}`);
        return exports.runTimeErrorCodes.InvalidEvent;
    }
    if (event.Records.length > recordLimit) {
        const message = `Did not process: received more than the record limit for this function (${recordLimit} record/s)`;
        console.log(`${exports.runTimeErrorCodes.TooManyRecords} ${message}`);
        return exports.runTimeErrorCodes.TooManyRecords;
    }
    const parsedEvent = helpers_1.parseSqsMessage(event, options);
    if (parsedEvent.errors.length !== 0) {
        const message = `Did not process due to parsing errors: ${parsedEvent.errors.map(e => `${e.message}`).join(", ")}`;
        console.log(`${exports.runTimeErrorCodes.InvalidMessage} ${message}`);
        return exports.runTimeErrorCodes.InvalidMessage;
    }
    /**
     * To avoid crashing and repeating. Do not allow function to throw
     * lambda. Instead log and report the issue.
     */
    try {
        // const p = parsedEvent.parsed[0].record.
        const { parsedBody, record } = parsedEvent.parsed[0];
        await fn(parsedBody, context, (_a = options) === null || _a === void 0 ? void 0 : _a.appFunctionOptions, record);
        console.log(`${exports.runTimeSuccessCode.Ok}`);
        return exports.runTimeSuccessCode.Ok;
    }
    catch (error) {
        console.error(`${exports.runTimeErrorCodes.UnhandledError} ${error}`);
        /* Log event in case of unhandled error. */
        try {
            console.log(`event: ${JSON.stringify(event, null, 4)}`);
        }
        catch (error) {
            console.error(`Failed to log event.`);
        }
        /* Notify SQS in case of unhandled error. */
        try {
            const logStreamLink = helpers_1.buildLogStreamLink(context);
            const attachments = [
                { title: 'Error', message: error.message, details: error.stack },
                { title: 'Context', message: logStreamLink, details: JSON.stringify(context, null, 4) }
            ];
            await sendToSns_1.sendToSns('arn:aws:sns:ap-southeast-2:711143483997:engineering-errors-prod', `[Lambda Server Error] ${context.functionName} crashed with ${error.name}`, attachments);
        }
        catch (error) {
            console.error(`Failed to send error to engineering.`);
        }
        return exports.runTimeErrorCodes.UnhandledError;
    }
}
exports.genericSqsHandler = genericSqsHandler;
//# sourceMappingURL=genericLambdaHandlerSqs.js.map