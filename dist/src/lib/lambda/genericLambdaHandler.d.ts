import { GenericLambdaHandlerFn } from "./models";
/**
 * Generic wrapper for business logic function to handle server errors,
 * logging, and responses consistently.
 *
 * @param event The event data received by lambda
 * @param businessFunction The function to call and pass the event to
 * @param lambdaContext The Context of the lambda that was invoked
 * @param packageVersion The version of the project/code
 * @param businessFunctionConfig (Optional) options to configure the businessFunction
 */
export declare const genericLambdaHandler: GenericLambdaHandlerFn;
