"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("./helpers");
const sendToSns_1 = require("../sns/sendToSns");
const models_1 = require("./models");
const models_2 = require("../../models");
const classLog_1 = require("../../helpers/clients/classLog");
/**
 * Generic wrapper for business logic function to handle server errors,
 * logging, and responses consistently.
 *
 * @param event The event data received by lambda
 * @param context The Context of the lambda that was invoked
 * @param appFunction The function to call and pass the event to
 * @param options Options for the appFunction
 */
exports.genericLambdaHandlerApigee = async (event, context, appFunction, options) => {
    var _a, _b, _c;
    try {
        if (((_a = options) === null || _a === void 0 ? void 0 : _a.isEventValid) !== undefined) {
            /* If invalid, this function should throw an InvalidPayload error with a detailed message. */
            const isValid = options.isEventValid(event);
            if (!isValid) {
                classLog_1.classLog('lib-lambda', 'genericLambdaHandlerApigee', `event is invalid`);
                throw new models_1.InvalidPayload();
            }
        }
        const response = await appFunction(event, context, (_b = options) === null || _b === void 0 ? void 0 : _b.appFunctionOptions);
        return new models_2.ApigeeResponse("OK", response);
    }
    catch (error) {
        /* Handle 4xx errors gracefully. */
        if (error.isBadRequestError) {
            const errorBadReq = error;
            classLog_1.classLog('lib-lambda', 'genericLambdaHandlerApigee', `BAD_REQUEST - ${error.name}: ${error.message}`, 'warn');
            return new models_2.ApigeeBadRequestResponse(errorBadReq, context);
        }
        /* Maybe send Server Errors to SNS. */
        const snsTopic = (_c = options) === null || _c === void 0 ? void 0 : _c.serverErrorSnsTopic;
        if (snsTopic !== undefined) {
            const logStreamLink = helpers_1.buildLogStreamLink(context);
            const attachments = [
                { title: 'Error', message: error.message, details: error.stack },
                { title: 'Context', message: logStreamLink, details: JSON.stringify(context, null, 4) }
            ];
            await sendToSns_1.sendToSns(snsTopic, `[Lambda Server Error] ${context.functionName} crashed with ${error.name}`, attachments);
        }
        classLog_1.classLog('lib-lambda', 'genericLambdaHandlerApigee', `SERVER_ERROR - ${error.name}: ${error.message}`, 'error');
        return new models_2.ApigeeServerErrorResponse(context);
    }
};
//# sourceMappingURL=genericLambdaHandlerApigee.js.map