import { genericLambdaHandlerApigeeFn } from "./models";
/**
 * Generic wrapper for business logic function to handle server errors,
 * logging, and responses consistently.
 *
 * @param event The event data received by lambda
 * @param context The Context of the lambda that was invoked
 * @param appFunction The function to call and pass the event to
 * @param options Options for the appFunction
 */
export declare const genericLambdaHandlerApigee: genericLambdaHandlerApigeeFn;
