import { SQSEvent, Context, SQSRecord } from "aws-lambda";
import { GenericSqsHandlerOptions } from "./models";
export declare const runTimeErrorCodes: {
    InvalidEvent: string;
    TooManyRecords: string;
    InvalidMessage: string;
    UnhandledError: string;
};
export declare const runTimeSuccessCode: {
    Ok: string;
};
/** Parse and handle an SQS event. Returns status of function evaluation. */
export declare function genericSqsHandler<T, G>(event: SQSEvent, context: Context, fn: (e: T, c: Context, options: any, r: SQSRecord) => Promise<G>, options?: GenericSqsHandlerOptions<T>, recordLimit?: number): Promise<string>;
