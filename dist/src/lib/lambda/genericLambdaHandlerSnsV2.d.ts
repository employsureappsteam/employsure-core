import { Context, SNSEvent } from "aws-lambda";
import { GenericAppFunction, GenericSnsHandlerOptions } from "./models";
export declare const wrapperErrorCodes: {
    InvalidEvent: string;
    TooManyRecords: string;
};
/** Parse and handle an SNS event. Returns status of function evaluation. */
export declare function genericSnsHanderV2<T, G>(event: SNSEvent, context: Context, fn: GenericAppFunction<T, G>, options?: GenericSnsHandlerOptions<T>): Promise<string | void>;
