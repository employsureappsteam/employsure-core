"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("./helpers");
const sendToSns_1 = require("../sns/sendToSns");
const models_1 = require("./models");
/**
 * Generic wrapper for business logic function to handle server errors,
 * logging, and responses consistently.
 *
 * @param event The event data received by lambda
 * @param businessFunction The function to call and pass the event to
 * @param lambdaContext The Context of the lambda that was invoked
 * @param packageVersion The version of the project/code
 * @param businessFunctionConfig (Optional) options to configure the businessFunction
 */
exports.genericLambdaHandler = async (event, context, appFunction, options) => {
    var _a, _b, _c, _d, _e;
    try {
        if (((_a = options) === null || _a === void 0 ? void 0 : _a.isEventValid) !== undefined) {
            /* Check if valid. This should throw, if not throw a generic error. */
            const isValid = options.isEventValid(event);
            if (!isValid) {
                throw new models_1.InvalidPayload();
            }
        }
        const response = await appFunction(event, context, (_b = options) === null || _b === void 0 ? void 0 : _b.appFunctionOptions);
        return response;
    }
    catch (error) {
        /* Handle 4xx errors gracefully. */
        if (error.isBadRequestError) {
            return {
                error: {
                    code: 400,
                    type: error.name,
                    message: error.message,
                    requestId: context.awsRequestId
                }
            };
        }
        /* Maybe send Server Errors to SNS. */
        const snsTopic = (_c = options) === null || _c === void 0 ? void 0 : _c.serverErrorSnsTopic;
        if (snsTopic !== undefined) {
            const logStreamLink = helpers_1.buildLogStreamLink(context);
            const attachments = [
                { title: 'Error', message: error.message, details: error.stack },
                { title: 'Context', message: logStreamLink, details: JSON.stringify(context, null, 4) }
            ];
            await sendToSns_1.sendToSns(snsTopic, `[Lambda Server Error] ${context.functionName} crashed with ${error.name}`, attachments);
        }
        /* Return error without details. */
        console.error(`${error.name}: ${error.message}`);
        if ((_e = (_d = options) === null || _d === void 0 ? void 0 : _d.serverErrorBehaviour, (_e !== null && _e !== void 0 ? _e : 'supress')) === 'supress') {
            return {
                error: {
                    code: 500,
                    type: 'ServerError',
                    message: 'Server Error',
                    requestId: context.awsRequestId
                }
            };
        }
        else {
            throw error;
        }
    }
};
//# sourceMappingURL=genericLambdaHandler.js.map