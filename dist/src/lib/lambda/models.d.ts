import { EmpLogger } from "../emplogger";
import { ApigeeResponse } from "../../models";
import { Context } from "aws-lambda";
/**
 * Function for housing business logic.
 */
export declare type RestApiFunction<Body, Options, Response> = (body: Body, logger: EmpLogger, options?: Options) => Promise<ApigeeResponse<Response>>;
/**
 * Light function wrapper giving consistency to AWS Lambda.
 *
 * Aims to reduce boilerplate.
 */
export declare type GenericLambdaHandlerFn<Event = {}, Options = null, AppResponse = any> = (event: Event, context: Context, appFunction: GenericLambdaFn<Event, Options, AppResponse>, options?: GenericLambdaOptions<Event>) => Promise<GenericLambdaResponse<AppResponse>>;
/**
 * Business function that will be called
 */
export declare type GenericLambdaFn<Event, Options, AppResponse> = (e: Event, c: Context, o?: Options) => Promise<AppResponse>;
/**
 * Options for the wrapper
 */
export declare type GenericLambdaOptions<Event> = {
    /**
     *  Options to pass to the business function.
     */
    appFunctionOptions?: any;
    /**
     *  If defined will be run before business function to validate event
     *
     * @throws InvalidLambdaEvent with details of why event is invalid.
     */
    isEventValid?: (e: Event) => e is Event;
    /**
     *  Send server errors to this SNS queue if defined.
     */
    serverErrorSnsTopic?: string;
    /** If throw, the handler will rethrow the error if not handled gracefully.
     * If supress, a typed 500 error will be returned and the lambda.
     * Default = supress
     */
    serverErrorBehaviour?: 'throw' | 'supress';
};
/**
 * Light function wrapper for functions connected to Apigee.
 *
 * Aims to reduce boilerplate.
 */
export declare type genericLambdaHandlerApigeeFn<Event = {}, Options = null, AppResponse = any> = (event: Event, context: Context, appFunction: GenericLambdaFn<Event, Options, AppResponse>, options?: GenericLambdaApigeeOptions<Event>) => Promise<ApigeeResponse<AppResponse>>;
/**
 * Same as generic but missing option to change how the wrapper handles server errors. They
 * will always be supressed and reply to Apigee without throwing.
 */
export declare type GenericLambdaApigeeOptions<Event> = Omit<GenericLambdaOptions<Event>, "serverErrorBehaviour">;
/**
 * Error should be thrown for any user error, wrapper will catch it and
 * handle gracefully with a 4xx code.
 */
export declare class BadRequestError extends Error {
    isBadRequestError: boolean;
    name: string;
    constructor(message: string);
}
export declare class InvalidPayload extends BadRequestError {
    name: string;
    constructor(message?: string);
}
export interface EmploysureError {
    code: string;
    name: string;
    message: string;
    requestId: string;
}
export declare type EmploysureErrorResponse = {
    error: EmploysureError;
};
/**
 * Lambda will return either:
 * - response from the appFunction
 * - error
 */
export declare type GenericLambdaResponse<AppResponse> = AppResponse | EmploysureErrorResponse;
export declare type AppFunctionOptions = any;
/** Parse an sqs event and optionally parse with a type guard */
export interface GenericSqsHandlerOptions<T> {
    toJson?: boolean;
    bodyTypeGuardFn?: (e: T) => e is T;
    postProcessing?: PostProcessingOptions;
    /**
     *  Options to pass to the business function.
     */
    appFunctionOptions?: AppFunctionOptions;
}
export interface GenericSnsHandlerOptions<T> {
    toJson?: boolean;
    bodyTypeGuardFn?: <T>(e: T) => e is T;
    processRecordsIn?: 'parallel' | 'serial';
    appFunctionOptions?: AppFunctionOptions;
    postProcessing?: PostProcessingOptions;
}
export declare type GenericAppFunction<T, G> = (e: T, options: AppFunctionOptions) => Promise<G>;
/**
 * pre-built post-processing for:
 * - sending to another SNS Topic, SQS Queue, or a service.
 */
export declare type PostProcessingOptions = {
    logToBigQuery?: boolean;
    sendErrorsToRollbar?: boolean;
    rollbarProjectName?: string;
    includeMessagePayload?: boolean;
};
export declare type HttpStatusCode = 200 | 400 | 500;
export declare type MessageResult = {
    source_arn: string;
    message_id: string;
    message_payload?: any;
    status: HttpStatusCode;
    status_message: string;
    response_payload?: string;
    error?: Error;
};
export declare type MessageResultList = Array<MessageResult>;
