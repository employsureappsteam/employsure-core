"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../../models");
const constants_1 = require("../../constants");
const sendToSns_1 = require("../sns/sendToSns");
const emplogger_1 = require("../emplogger");
/**
 * Generic wrapper for business logic function to handle server errors,
 * logging, and responses consistently.
 *
 * @param event The event data received by lambda
 * @param businessFunction The function to call and pass the event to
 * @param lambdaContext The Context of the lambda that was invoked
 * @param packageVersion The version of the project/code
 * @param businessFunctionConfig (Optional) options to configure the businessFunction
 */
async function genericRestLambda(event, businessFunction, lambdaContext, packageVersion, businessFunctionConfig) {
    const logger = new emplogger_1.EmpLogger();
    try {
        logger.log(`Stage: ${process.env.stage}, Version: ${packageVersion}`);
        logger.log(`Event received: ${JSON.stringify(event, null, 4)}`);
        const response = await businessFunction(event, logger, businessFunctionConfig);
        logger.log(`Status: ${response.StatusCode}`);
        logger.log(`Result: ${JSON.stringify(response.Data, null, 4)}`);
        return response;
    }
    catch (error) {
        /* Don't log entire error with JSON.stringify(), details will be in notification */
        logger.log(`${error}`);
        // TODO: the api error notifications should live in apigee
        await sendToSns_1.sendToSns(constants_1.SnsTopics.EngErrorsProd, `${lambdaContext.functionName} has failed`, [
            {
                title: `${error.name}`,
                message: `${error}`,
                details: `${error.stack}`,
            },
            {
                title: `Log`,
                message: ``,
                details: logger.getLogHistory().join("\n")
            }
        ]);
        throw new models_1.LambdaError({
            StatusCode: constants_1.HttpStatusCodes.SERVER_ERROR,
            Data: "Please try again and contact Employsure Engineering if issue persists."
        });
    }
}
exports.genericRestLambda = genericRestLambda;
//# sourceMappingURL=genericRestLambda.js.map