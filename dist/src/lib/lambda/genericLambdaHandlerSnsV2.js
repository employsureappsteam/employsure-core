"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sendToSns_1 = require("../sns/sendToSns");
const helpers_1 = require("./helpers");
const typeguards_1 = require("./typeguards");
exports.wrapperErrorCodes = {
    InvalidEvent: "CONFIG_ERROR-INVALID_EVENT",
    TooManyRecords: "CONFIG_ERROR-TOO_MANY_RECORDS"
};
/** Parse and handle an SNS event. Returns status of function evaluation. */
async function genericSnsHanderV2(event, context, fn, options) {
    var _a, _b, _c, _d;
    /**
     * To avoid crashing and repeating. Do not allow function to throw
     * lambda. Instead log and report the issue.
     */
    try {
        if (!typeguards_1.instanceOfSns(event)) {
            const message = 'Invalid SNS Event';
            console.warn(`${exports.wrapperErrorCodes.InvalidEvent} ${message}`);
            return exports.wrapperErrorCodes.InvalidEvent;
        }
        const { errors, validRecords } = helpers_1.parseSnsMessage(event, options);
        const processedRecordsResponses = [];
        /* Log any invalid messages as warnings. */
        console.info(`Processing ${validRecords.length} messages, skipping ${errors.length} invalid messages`);
        for (const err of errors) {
            const message = `INVALID_MESSAGE_PAYLOAD ${err.message}`;
            console.warn(`${err.record.Sns.MessageId} 400 ${message}`);
            processedRecordsResponses.push({
                source_arn: err.record.Sns.TopicArn,
                message_id: err.record.Sns.MessageId,
                status: 400,
                status_message: message
            });
        }
        const processRecordsIn = (_b = (_a = options) === null || _a === void 0 ? void 0 : _a.processRecordsIn, (_b !== null && _b !== void 0 ? _b : 'serial'));
        if (processRecordsIn === 'serial') {
            for (const valid of validRecords) {
                const result = await executeFunctionSns(fn, valid.payload, valid.record, (_c = options) === null || _c === void 0 ? void 0 : _c.appFunctionOptions);
                processedRecordsResponses.push(result);
            }
        }
        else if (processRecordsIn === 'parallel') {
            const results = await Promise.all(validRecords.map(async (valid) => {
                var _a;
                return executeFunctionSns(fn, valid.payload, valid.record, (_a = options) === null || _a === void 0 ? void 0 : _a.appFunctionOptions);
            }));
            processedRecordsResponses.push(...results);
        }
        /* Post Processing of messages. */
        await helpers_1.postProcessMessageResults((_d = options) === null || _d === void 0 ? void 0 : _d.postProcessing, processedRecordsResponses, context);
    }
    catch (error) {
        console.error(error);
        /* Log event in case of unhandled error. */
        try {
            console.log(`event: ${JSON.stringify(event, null, 4)}`);
        }
        catch (error) {
            console.error(`Failed to log event causing above error. ${error}`);
        }
        /* Notify SQS in case of unhandled error. */
        try {
            const logStreamLink = helpers_1.buildLogStreamLink(context);
            const attachments = [
                { title: 'Error', message: error.message, details: error.stack },
                { title: 'Context', message: logStreamLink, details: JSON.stringify(context, null, 4) }
            ];
            await sendToSns_1.sendToSns('arn:aws:sns:ap-southeast-2:711143483997:engineering-errors-prod', `[Lambda Server Error] ${context.functionName} crashed with ${error.name}`, attachments);
        }
        catch (error) {
            console.error(`Failed to send error to engineering. ${error}`);
        }
    }
}
exports.genericSnsHanderV2 = genericSnsHanderV2;
/* Execution function for a particular record. */
const executeFunctionSns = async (fn, payload, record, options) => {
    try {
        const response = await fn(payload, options);
        const response_payload = helpers_1.parseAnyToString(response);
        return {
            source_arn: record.Sns.TopicArn,
            message_id: record.Sns.MessageId,
            message_payload: payload,
            status: 200,
            status_message: 'Success',
            response_payload
        };
    }
    catch (error) {
        if (error.isBadRequestError) {
            return {
                source_arn: record.Sns.TopicArn,
                message_id: record.Sns.MessageId,
                message_payload: payload,
                status: 400,
                status_message: `${error}`,
                error
            };
        }
        else {
            return {
                source_arn: record.Sns.TopicArn,
                message_id: record.Sns.MessageId,
                message_payload: payload,
                status: 500,
                status_message: `${error}`,
                error
            };
        }
    }
};
//# sourceMappingURL=genericLambdaHandlerSnsV2.js.map