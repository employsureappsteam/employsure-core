"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Error should be thrown for any user error, wrapper will catch it and
 * handle gracefully with a 4xx code.
 */
class BadRequestError extends Error {
    constructor(message) {
        super(message);
        this.isBadRequestError = true;
        this.name = "BadRequestError";
    }
}
exports.BadRequestError = BadRequestError;
class InvalidPayload extends BadRequestError {
    constructor(message) {
        super((message !== null && message !== void 0 ? message : "Payload is invalid"));
        this.name = "InvalidPayload";
    }
}
exports.InvalidPayload = InvalidPayload;
//# sourceMappingURL=models.js.map