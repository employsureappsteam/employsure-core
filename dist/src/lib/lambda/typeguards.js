"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("../../helpers");
const objectHasValuesOf_1 = require("../../helpers/typeguards/objectHasValuesOf");
function instanceOfSqsEvent(event) {
    return (Array.isArray(event.Records) &&
        event.Records
            .map(r => instanceOfSqsEventRecord(r))
            .reduce((p, c) => p && c, true));
}
exports.instanceOfSqsEvent = instanceOfSqsEvent;
function instanceOfSqsEventRecord(record) {
    return (typeof record.messageId === "string" &&
        typeof record.receiptHandle === "string" &&
        typeof record.body === "string" &&
        typeof record.md5OfBody === "string" &&
        typeof record.eventSource === "string" &&
        typeof record.eventSourceARN === "string" &&
        typeof record.awsRegion === "string" &&
        instanceOfSqsRecordAttributes(record.attributes));
}
exports.instanceOfSqsEventRecord = instanceOfSqsEventRecord;
function instanceOfSqsRecordAttributes(att) {
    return (typeof att.ApproximateFirstReceiveTimestamp === "string" &&
        typeof att.SentTimestamp === "string" &&
        typeof att.SenderId === "string" &&
        typeof att.ApproximateFirstReceiveTimestamp === "string");
}
exports.instanceOfSqsRecordAttributes = instanceOfSqsRecordAttributes;
function instanceOfSns(event) {
    return (Array.isArray(event.Records) &&
        event.Records
            .map(r => instanceOfSnsEventRecord(r))
            .reduce((p, c) => p && c, true));
}
exports.instanceOfSns = instanceOfSns;
function instanceOfSnsEventRecord(r) {
    return typeof r.EventVersion === 'string'
        && typeof r.EventSubscriptionArn === 'string'
        && typeof r.EventSource === 'string'
        && instanceOfSnsMessage(r.Sns);
}
exports.instanceOfSnsEventRecord = instanceOfSnsEventRecord;
function instanceOfSnsMessage(m) {
    return typeof m.SignatureVersion === 'string'
        && typeof m.Timestamp === 'string'
        && typeof m.Signature === 'string'
        && typeof m.SigningCertUrl === 'string'
        && typeof m.MessageId === 'string'
        && typeof m.Message === 'string'
        && instanceOfSNSMessageAttributes(m.MessageAttributes)
        && typeof m.Type === 'string'
        && typeof m.UnsubscribeUrl === 'string'
        && typeof m.TopicArn === 'string'
        && typeof m.Subject === 'string';
}
exports.instanceOfSnsMessage = instanceOfSnsMessage;
function instanceOfSNSMessageAttributes(a) {
    return helpers_1.isJsonObject(a)
        && objectHasValuesOf_1.objectHasValuesOf(a, instanceOfSnsMessageAttribute);
}
exports.instanceOfSNSMessageAttributes = instanceOfSNSMessageAttributes;
function instanceOfSnsMessageAttribute(a) {
    return helpers_1.isJsonObject(a)
        && typeof a.Type === 'string'
        && typeof a.Value === 'string';
}
exports.instanceOfSnsMessageAttribute = instanceOfSnsMessageAttribute;
//# sourceMappingURL=typeguards.js.map