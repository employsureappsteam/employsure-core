"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rollbar_1 = require("../../clients/rollbar");
const helpers_1 = require("../../helpers");
/**
 * Builds a log stream link from lambda context.
 *
 * AWS CloudWatch has a unique encoding. This doesn't try to understand it
 * only copy it. For better filtering further investigation is needed.
 *
 * Example: '$2522' is a double quote.
 *
 * @param context Lambda context
 * @returns a url to access the log stream for relevant context.
 */
exports.buildLogStreamLink = (context) => {
    var _a, _b, _c;
    const region = (_c = (_b = (_a = process) === null || _a === void 0 ? void 0 : _a.env) === null || _b === void 0 ? void 0 : _b.AWS_REGION, (_c !== null && _c !== void 0 ? _c : 'local_debug'));
    const logGroupEnc = context.logGroupName.replace(/\//g, '$252F');
    const logStreamEnc = context.logStreamName.replace('$', '$2524').replace('[', '$255B').replace(']', '$255D').replace(/\//g, '$252F');
    const awsRequestId = context.awsRequestId;
    const filterPattern = `$3FfilterPattern$3D` + `$2522` + awsRequestId + `$2522`;
    const link = `https://${region}.console.aws.amazon.com/cloudwatch/home?region=${region}#logsV2:log-groups/log-group/${logGroupEnc}/log-events/${logStreamEnc}${filterPattern}`;
    return link;
};
function parseSqsMessage(event, options) {
    var _a;
    const errors = [];
    const parsed = [];
    for (const record of event.Records) {
        try {
            const parsedBody = JSON.parse(record.body);
            /* Use the type guard function if it is provided. */
            if (((_a = options) === null || _a === void 0 ? void 0 : _a.bodyTypeGuardFn) && !options.bodyTypeGuardFn(parsedBody)) {
                try {
                    errors.push({ record, message: `Event is typed incorrectly` });
                }
                catch (error) {
                    if (error.isBadRequestError) {
                        errors.push({ record, message: `${error}` });
                    }
                    else {
                        throw error;
                    }
                    ;
                }
            }
            else {
                parsed.push({ record, parsedBody });
            }
        }
        catch (error) {
            errors.push({ record, message: error.message });
        }
    }
    return { parsed, errors };
}
exports.parseSqsMessage = parseSqsMessage;
function parseSqsMessageV2(event, options) {
    var _a;
    const errors = [];
    const validRecords = [];
    for (const record of event.Records) {
        try {
            const payload = JSON.parse(record.body);
            /* Use the type guard function if it is provided. */
            if (((_a = options) === null || _a === void 0 ? void 0 : _a.bodyTypeGuardFn) && !options.bodyTypeGuardFn(payload)) {
                try {
                    errors.push({ record, message: `Event is typed incorrectly` });
                }
                catch (error) {
                    if (error.isBadRequestError) {
                        errors.push({ record, message: `${error}` });
                    }
                    else {
                        throw error;
                    }
                    ;
                }
            }
            else {
                validRecords.push({ record, payload });
            }
        }
        catch (error) {
            errors.push({ record, message: error.message });
        }
    }
    return { validRecords, errors };
}
exports.parseSqsMessageV2 = parseSqsMessageV2;
function parseSnsMessage(event, options) {
    var _a, _b;
    const errors = [];
    const validRecords = [];
    for (const record of event.Records) {
        try {
            let payload;
            if ((_a = options) === null || _a === void 0 ? void 0 : _a.toJson) {
                payload = JSON.parse(record.Sns.Message);
            }
            else {
                /* T should be specified as string in this case. */
                payload = record.Sns.Message;
            }
            /* Use the type guard function if it is provided. */
            if (((_b = options) === null || _b === void 0 ? void 0 : _b.bodyTypeGuardFn) && !options.bodyTypeGuardFn(payload)) {
                try {
                    errors.push({ record, message: `Event is typed incorrectly` });
                }
                catch (error) {
                    if (error.isBadRequestError) {
                        errors.push({ record, message: `${error}` });
                    }
                    else {
                        throw error;
                    }
                    ;
                }
            }
            else {
                validRecords.push({ record, payload });
            }
        }
        catch (error) {
            errors.push({ record, message: error.message });
        }
    }
    return { validRecords, errors };
}
exports.parseSnsMessage = parseSnsMessage;
exports.postProcessMessageResults = async (options, results, context) => {
    var _a, _b, _c, _d;
    const timestamp = (new Date).toISOString();
    if ((_a = options) === null || _a === void 0 ? void 0 : _a.logToBigQuery) {
        try {
            const bqClient = await helpers_1.loadBigQueryClient('lead-scoring-model');
            const tableId = `message_log`;
            let datasetId = `aws_employsure_dev`;
            if (process.env.stage === "Prod") {
                datasetId = `aws_employsure_prod`;
            }
            console.debug(`Logging to '${datasetId}.${tableId}'`);
            await bqClient.insertToTable(datasetId, tableId, results.map(r => ({
                timestamp,
                source_arn: r.source_arn,
                message_id: r.message_id,
                message_payload: exports.parseAnyToString(r.message_payload),
                status: r.status,
                status_message: r.status_message,
                response_payload: r.response_payload,
                function_name: context.functionName,
                aws_request_id: context.awsRequestId,
                log_group_name: context.logGroupName,
                log_stream_name: context.logStreamName,
            })), [
                /* Generated while inserting. */
                { "name": "timestamp", "type": "TIMESTAMP" },
                /* From message results. */
                { "name": "source_arn", "type": "STRING" },
                { "name": "message_id", "type": "STRING" },
                { "name": "message_payload", "type": "STRING" },
                { "name": "status", "type": "INTEGER" },
                { "name": "status_message", "type": "STRING" },
                { "name": "response_payload", "type": "STRING" },
                /* AWS Lambda context. */
                { "name": "function_name", "type": "STRING" },
                { "name": "aws_request_id", "type": "STRING" },
                { "name": "log_group_name", "type": "STRING" },
                { "name": "log_stream_name", "type": "STRING" }
            ]);
        }
        catch (error) {
            console.error(`lib-lambda`, `genericLambdaHandlerSns`, `Failed to log to BigQuery ${error}`, 'error');
        }
    }
    if ((_b = options) === null || _b === void 0 ? void 0 : _b.sendErrorsToRollbar) {
        try {
            const toSend = results.filter(r => [400, 500].includes(r.status));
            if (toSend.length > 0) {
                const projectName = (_d = (_c = options) === null || _c === void 0 ? void 0 : _c.rollbarProjectName, (_d !== null && _d !== void 0 ? _d : "AwsEmploysure"));
                console.debug(`Sending ${toSend.length} to rollbar project '${projectName}'`);
                const rollbar = new rollbar_1.RollbarClient({ projectName });
                await Promise.all(toSend.map(async (r) => {
                    const custom = { context };
                    if (options.includeMessagePayload) {
                        custom['message_payload'] = exports.parseAnyToString(r.message_payload);
                    }
                    await rollbar.log({
                        message: r.status_message,
                        error: r.error,
                        severity: r.status === 400 ? 'error' : 'critical',
                        custom
                    });
                }));
            }
        }
        catch (error) {
            console.error(`lib-lambda`, `genericLambdaHandlerSns`, `Failed to log to send to Sqs ${error}`, 'error');
        }
    }
};
exports.parseAnyToString = (unknownVar) => {
    try {
        if (typeof unknownVar === "string") {
            return unknownVar;
        }
        else if (unknownVar !== null && typeof unknownVar === 'object') {
            return JSON.stringify(unknownVar);
        }
        else {
            /* Fall back. */
            return `${unknownVar}`;
        }
    }
    catch (error) {
        console.error(`Failed to parse unknownVar to a string. ${error}`);
    }
    return undefined;
};
//# sourceMappingURL=helpers.js.map