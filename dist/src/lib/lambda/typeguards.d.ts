import { SNSEvent, SNSEventRecord, SNSMessage, SNSMessageAttribute, SNSMessageAttributes, SQSEvent, SQSRecord, SQSRecordAttributes } from 'aws-lambda';
export declare function instanceOfSqsEvent(event: SQSEvent): event is SQSEvent;
export declare function instanceOfSqsEventRecord(record: SQSRecord): record is SQSRecord;
export declare function instanceOfSqsRecordAttributes(att: SQSRecordAttributes): att is SQSRecordAttributes;
export declare function instanceOfSns(event: SNSEvent): event is SNSEvent;
export declare function instanceOfSnsEventRecord(r: SNSEventRecord): r is SNSEventRecord;
export declare function instanceOfSnsMessage(m: SNSMessage): m is SNSMessage;
export declare function instanceOfSNSMessageAttributes(a: SNSMessageAttributes): a is SNSMessageAttributes;
export declare function instanceOfSnsMessageAttribute(a: SNSMessageAttribute): a is SNSMessageAttribute;
