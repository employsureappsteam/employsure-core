"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("../../models/errors");
const sendToSns_1 = require("../sns/sendToSns");
const helpers_1 = require("./helpers");
const typeguards_1 = require("./typeguards");
/** Parse and handle an SQS event. Returns status of function evaluation. */
async function genericSqsHandlerV2(event, context, fn, options) {
    var _a, _b;
    /**
     * To avoid crashing and repeating. Do not allow function to throw
     * lambda. Instead log and report the issue.
     */
    try {
        if (!typeguards_1.instanceOfSqsEvent(event)) {
            throw new errors_1.EmploysureError(`InvalidSqsEvent`, `Function only handles SQS Messages`);
        }
        /* TODO: update this to handle more. */
        if (event.Records.length > 1) {
            throw new errors_1.EmploysureError(`TooManyRecords`, `Function only handles 1 message`);
        }
        const { errors, validRecords } = helpers_1.parseSqsMessageV2(event, options);
        const processedRecordsResponses = [];
        console.info(`Processing ${validRecords.length} messages, skipping ${errors.length} invalid messages`);
        for (const err of errors) {
            const message = `INVALID_MESSAGE_PAYLOAD ${err.message}`;
            console.warn(`${err.record.messageId} 400 ${message}`);
            processedRecordsResponses.push({
                source_arn: err.record.eventSourceARN,
                message_id: err.record.messageId,
                status: 400,
                status_message: message
            });
        }
        for (const validRecord of validRecords) {
            const { payload, record } = validRecord;
            processedRecordsResponses.push(await executeFunctionSqs(fn, payload, record, (_a = options) === null || _a === void 0 ? void 0 : _a.appFunctionOptions));
        }
        /* Post Processing of messages. */
        await helpers_1.postProcessMessageResults((_b = options) === null || _b === void 0 ? void 0 : _b.postProcessing, processedRecordsResponses, context);
    }
    catch (error) {
        console.error(error);
        /* Log event in case of unhandled error. */
        try {
            console.log(`event: ${JSON.stringify(event, null, 4)}`);
        }
        catch (error) {
            console.error(`Failed to log event. ${error}`);
        }
        /* Notify SQS in case of unhandled error. */
        try {
            const logStreamLink = helpers_1.buildLogStreamLink(context);
            const attachments = [
                { title: 'Error', message: error.message, details: error.stack },
                { title: 'Context', message: logStreamLink, details: JSON.stringify(context, null, 4) }
            ];
            await sendToSns_1.sendToSns('arn:aws:sns:ap-southeast-2:711143483997:engineering-errors-prod', `[Lambda Server Error] ${context.functionName} crashed with ${error.name}`, attachments);
        }
        catch (error) {
            console.error(`Failed to send error to engineering. ${error}`);
        }
    }
}
exports.genericSqsHandlerV2 = genericSqsHandlerV2;
/* Execution function for a particular record. */
const executeFunctionSqs = async (fn, payload, record, options) => {
    try {
        const response = await fn(payload, options);
        const response_payload = helpers_1.parseAnyToString(response);
        return {
            source_arn: record.eventSourceARN,
            message_id: record.messageId,
            message_payload: payload,
            status: 200,
            status_message: 'Success',
            response_payload
        };
    }
    catch (error) {
        if (error.isBadRequestError) {
            return {
                source_arn: record.eventSourceARN,
                message_id: record.messageId,
                message_payload: payload,
                status: 400,
                status_message: `${error}`,
                error
            };
        }
        else {
            return {
                source_arn: record.eventSourceARN,
                message_id: record.messageId,
                message_payload: payload,
                status: 500,
                status_message: `${error}`,
                error
            };
        }
    }
};
//# sourceMappingURL=genericLambdaHandlerSqsV2.js.map