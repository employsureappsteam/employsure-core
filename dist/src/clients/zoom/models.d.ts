export interface IClientParams {
    apiSecret: string;
    apiKey: string;
}
export interface IGetUsers {
    page_count: number;
    page_number: number;
    page_size: number;
    total_records: number;
    users: IZoomUser[];
}
export interface IZoomUser {
    id: string;
    first_name: string;
    last_name: string;
    email: string;
    type: number;
    pmi: number;
    timezone?: Timezone;
    verified: number;
    dept?: string;
    created_at: Date;
    last_login_time?: Date;
    last_client_version?: string;
    pic_url?: string;
    language?: string;
    phone_number?: string;
    status: Status;
    group_ids?: string[];
}
export declare enum Status {
    Active = "active",
    Inactive = "inactive",
    Pending = "pending"
}
export declare enum Timezone {
    AustraliaAdelaide = "Australia/Adelaide",
    AustraliaBrisbane = "Australia/Brisbane",
    AustraliaDarwin = "Australia/Darwin",
    AustraliaHobart = "Australia/Hobart",
    AustraliaPerth = "Australia/Perth",
    AustraliaSydney = "Australia/Sydney",
    Empty = "",
    PacificAuckland = "Pacific/Auckland"
}
export interface IZoomUserDetail {
    id: string;
    first_name: string;
    last_name: string;
    email: string;
    type: number;
    role_name: string;
    pmi: number;
    use_pmi: boolean;
    vanity_url: string;
    personal_meeting_url: string;
    timezone: string;
    verified: number;
    dept: string;
    created_at: Date;
    last_login_time: Date;
    last_client_version: string;
    host_key: string;
    jid: string;
    group_ids: any[];
    im_group_ids: any[];
    account_id: string;
    language: string;
    phone_country: string;
    phone_number: string;
    status: string;
}
export interface IGetGroups {
    total_records: number;
    groups: IZoomGroup[];
}
export interface IZoomGroup {
    id: string;
    name: string;
    total_members: number;
}
export interface IAddGroupMemberResponse {
    "ids": string;
    "added_at": string;
}
export interface IZoomUpdateUserPayload {
    [x: string]: any;
}
export interface IUserMeetingsReport {
    from: string;
    to: string;
    page_count: number;
    page_size: number;
    total_records: number;
    next_page_token: string;
    meetings: IUserMeeting[];
}
export interface IUserMeeting {
    uuid: string;
    id: number;
    host_id: string;
    type: number;
    topic: string;
    user_name: string;
    user_email: string;
    start_time: string;
    end_time: string;
    duration: number;
    total_minutes: number;
    participants_count: number;
}
export interface ICreateMeetingOptions {
    topic?: string;
    type?: number;
    start_time?: string;
    duration?: number;
    schedule_for?: string;
    timezone?: string;
    password?: string;
    agenda?: string;
    recurrence?: {
        type?: number;
        repeat_interval?: number;
        weekly_days?: string;
        monthly_day?: number;
        monthly_week?: number;
        monthly_week_day?: number;
        end_times?: number;
        end_date_time?: string;
    };
    settings?: {
        host_video?: boolean;
        participant_video?: boolean;
        cn_meeting?: boolean;
        in_meeting?: boolean;
        join_before_host?: boolean;
        mute_upon_entry?: boolean;
        watermark?: boolean;
        use_pmi?: boolean;
        approval_type?: number;
        registration_type?: number;
        audio?: string;
        auto_recording?: string;
        enforce_login?: boolean;
        enforce_login_domains?: string;
        alternative_hosts?: string;
        global_dial_in_countries?: Array<string>;
        registrants_email_notification?: boolean;
    };
}
export interface ICreateMeetingResponse {
    uuid: string;
    id: number;
    host_id: string;
    host_email: string;
    topic: string;
    type: number;
    status: string;
    start_time: Date;
    duration: number;
    timezone: string;
    created_at: Date;
    start_url: string;
    join_url: string;
    password: string;
    h323_password: string;
    pstn_password: string;
    encrypted_password: string;
    pmi?: number;
    settings: Settings;
}
export interface Settings {
    host_video: boolean;
    participant_video: boolean;
    cn_meeting: boolean;
    in_meeting: boolean;
    join_before_host: boolean;
    mute_upon_entry: boolean;
    watermark: boolean;
    use_pmi: boolean;
    approval_type: number;
    audio: string;
    auto_recording: string;
    enforce_login: boolean;
    enforce_login_domains: string;
    alternative_hosts: string;
    close_registration: boolean;
    show_share_button: boolean;
    allow_multiple_devices: boolean;
    registrants_confirmation_email: boolean;
    waiting_room: boolean;
    request_permission_to_unmute_participants: boolean;
    global_dial_in_countries: Array<Country>;
    global_dial_in_numbers: Array<GlobalDialInNumber>;
    registrants_email_notification: boolean;
    meeting_authentication: boolean;
    encryption_type: string;
    approved_or_denied_countries_or_regions: ApprovedOrDeniedCountriesOrRegions;
}
export interface ApprovedOrDeniedCountriesOrRegions {
    enable: boolean;
}
export declare enum Country {
    Au = "AU",
    GB = "GB",
    Nz = "NZ"
}
export interface GlobalDialInNumber {
    country_name: CountryName;
    number: string;
    type: string;
    country: Country;
}
export declare enum CountryName {
    Australia = "Australia",
    NewZealand = "New Zealand",
    UnitedKingdom = "United Kingdom"
}
