"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Status;
(function (Status) {
    Status["Active"] = "active";
    Status["Inactive"] = "inactive";
    Status["Pending"] = "pending";
})(Status = exports.Status || (exports.Status = {}));
var Timezone;
(function (Timezone) {
    Timezone["AustraliaAdelaide"] = "Australia/Adelaide";
    Timezone["AustraliaBrisbane"] = "Australia/Brisbane";
    Timezone["AustraliaDarwin"] = "Australia/Darwin";
    Timezone["AustraliaHobart"] = "Australia/Hobart";
    Timezone["AustraliaPerth"] = "Australia/Perth";
    Timezone["AustraliaSydney"] = "Australia/Sydney";
    Timezone["Empty"] = "";
    Timezone["PacificAuckland"] = "Pacific/Auckland";
})(Timezone = exports.Timezone || (exports.Timezone = {}));
var Country;
(function (Country) {
    Country["Au"] = "AU";
    Country["GB"] = "GB";
    Country["Nz"] = "NZ";
})(Country = exports.Country || (exports.Country = {}));
var CountryName;
(function (CountryName) {
    CountryName["Australia"] = "Australia";
    CountryName["NewZealand"] = "New Zealand";
    CountryName["UnitedKingdom"] = "United Kingdom";
})(CountryName = exports.CountryName || (exports.CountryName = {}));
//# sourceMappingURL=models.js.map