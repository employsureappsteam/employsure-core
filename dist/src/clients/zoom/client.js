"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const axios_rate_limit_1 = __importDefault(require("axios-rate-limit"));
const const_1 = require("./const");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
class ZoomClient {
    constructor(params) {
        this._params = params;
        const token = this._generateToken();
        this._httpClient = axios_rate_limit_1.default(axios_1.default.create({
            baseURL: const_1.ZOOM_API_BASE_URL,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }), {
            maxRequests: 2,
            perMilliseconds: 2000
        });
    }
    _refreshToken() {
        const token = this._generateToken();
        this._httpClient.defaults.headers['Authorization'] = `Bearer ${token}`;
    }
    _generateToken() {
        /* Zoom docs: https://marketplace.zoom.us/docs/guides/authorization/jwt/jwt-with-zoom */
        const header = {
            "alg": "HS256",
            "typ": "JWT"
        };
        const payload = {
            "iss": this._params.apiKey
        };
        return jsonwebtoken_1.default.sign(payload, this._params.apiSecret, {
            header,
            expiresIn: const_1.ZOOM_JWT_EXPIRY
        });
    }
    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meetingcreate
     * This API has a daily rate limit of 100 requests per day. Therefore, only 100 Create a Meeting API requests are permitted within a 24 hour window for a user.
     *
     * @param userId
     */
    async createMeeting(userId, options) {
        const url = `/users/${userId}/meetings`;
        const response = await this._httpClient.post(url, options);
        return response.data;
    }
    async getGroups() {
        return this._httpClient.get(`/groups/`).then(d => d.data.groups);
    }
    async getUsers() {
        const pageSize = 300; // maximum
        let pageNumber = 1;
        const query = (size, number) => `/users?page_size=${size}&page_number=${number}`;
        const firstResponse = await this._httpClient.get(query(pageSize, pageNumber)).then(d => d.data);
        const pageCount = firstResponse.page_count;
        pageNumber = firstResponse.page_number + 1;
        const users = firstResponse.users;
        while (pageNumber <= pageCount) {
            const response = await this._httpClient
                .get(query(pageSize, pageNumber))
                .then(d => d.data);
            pageNumber = response.page_number + 1;
            users.push(...response.users);
        }
        return users;
    }
    async getUserDetail(id) {
        return this._httpClient.get(`/users/${id}`).then(d => d.data);
    }
    async getUserMeetings(id, params) {
        var _a;
        const urlParam = [`page_size=300`];
        if ((_a = params) === null || _a === void 0 ? void 0 : _a.from) {
            if (params.from.match(/\d{4}-\d{2}-\d{2}/)) {
                urlParam.push(`from=${params.from}`);
            }
            else {
                throw new Error(`${params.from} is not a valid date. Must be YYYY-MM-DD`);
            }
        }
        const url = `/report/users/${id}/meetings?${urlParam.join("&")}`;
        const responseData = await this
            ._httpClient
            .get(url)
            .then(d => d.data);
        if (responseData.page_count > 1) {
            // TODO: rewrite to support this
            console.log(`WARNING: page count is greater than 1 and some data is missing.`);
        }
        return responseData.meetings;
    }
    async updateUser(id, updatePayload) {
        const payload = await this._httpClient.patch(`/users/${id}`, updatePayload).then(r => r.data);
        /* Returns an empty string when successful. Change it to a null */
        if (payload === '') {
            return null;
        }
        else {
            throw new Error(`PATCH /users/${id} retuturned unexpected ${JSON.stringify(payload, null, 4)}`);
        }
    }
    async addGroupMembers(groupId, memberIds) {
        const data = await this._httpClient
            .post(`/groups/${groupId}/members`, { members: memberIds.map(id => ({ id })) })
            .then(d => d.data);
        const ids = data.ids.split(",");
        if (ids.length !== memberIds.length) {
            const missing = ids.map(id => !memberIds.includes(id));
            throw new Error(`Failed to add members to group. Either already added or invalid: ${missing.join(", ")}`);
        }
        return data;
    }
}
exports.ZoomClient = ZoomClient;
//# sourceMappingURL=client.js.map