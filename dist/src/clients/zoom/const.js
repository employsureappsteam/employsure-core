"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ZOOM_API_BASE_URL = 'https://api.zoom.us/v2';
/* https://github.com/zeit/ms */
exports.ZOOM_JWT_EXPIRY = '1h';
//# sourceMappingURL=const.js.map