import { IClientParams, IZoomUserDetail, IAddGroupMemberResponse, IZoomUpdateUserPayload, ICreateMeetingOptions, ICreateMeetingResponse } from "./models";
export declare class ZoomClient {
    private _params;
    private _httpClient;
    constructor(params: IClientParams);
    private _refreshToken;
    private _generateToken;
    /**
     * https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meetingcreate
     * This API has a daily rate limit of 100 requests per day. Therefore, only 100 Create a Meeting API requests are permitted within a 24 hour window for a user.
     *
     * @param userId
     */
    createMeeting(userId: string, options?: ICreateMeetingOptions): Promise<ICreateMeetingResponse>;
    getGroups(): Promise<import("./models").IZoomGroup[]>;
    getUsers(): Promise<import("./models").IZoomUser[]>;
    getUserDetail(id: string): Promise<IZoomUserDetail>;
    getUserMeetings(id: string, params?: {
        from?: string;
    }): Promise<import("./models").IUserMeeting[]>;
    updateUser(id: string, updatePayload: IZoomUpdateUserPayload): Promise<null>;
    addGroupMembers(groupId: string, memberIds: Array<string>): Promise<IAddGroupMemberResponse>;
}
