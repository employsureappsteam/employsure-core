export declare const ZOOM_API_BASE_URL = "https://api.zoom.us/v2";
export declare const ZOOM_JWT_EXPIRY = "1h";
