"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const rollbar_1 = __importDefault(require("rollbar"));
const lib_1 = require("../../lib");
const errors_1 = require("../../models/errors");
class RollbarClient {
    constructor(options) {
        var _a, _b;
        /* Prod = 'production', any other valid string = 'development', not set = 'unknown'*/
        let envStage = 'unknown';
        if (process.env.stage === "Prod") {
            envStage = "production";
        }
        else if (process.env.stage) {
            envStage = "development";
        }
        const defaultOptions = {
            projectName: "AwsEmploysure",
            /* Assume 'unknown' if it's not in the stage and not specified. */
            environment: (_b = (_a = options) === null || _a === void 0 ? void 0 : _a.environment, (_b !== null && _b !== void 0 ? _b : envStage)),
        };
        this.options = {
            ...defaultOptions,
            ...options,
        };
        if (!this.options.projectName && !this.options.access_token) {
            throw new errors_1.EmploysureError(`InvalidRollbarOptions`, `Must give project name or access token.`);
        }
        else if (this.options.access_token) {
            const accessToken = this.options.access_token;
            this.client = new rollbar_1.default({ accessToken });
        }
    }
    async initClient() {
        if (!this.client && this.options.projectName) {
            const accessToken = await this.getRollbarAccessToken(this.options.projectName);
            this.client = new rollbar_1.default({
                accessToken,
                environment: this.options.environment
            });
        }
    }
    async getRollbarAccessToken(projectName) {
        /* Fetch from SSM. */
        const accessTokenPath = `/Rollbar${projectName}/Prod/AccessToken`;
        const params = await lib_1.GetSecretParams([accessTokenPath]);
        return params[accessTokenPath];
    }
    async log(event) {
        await this.initClient();
        /* Wrap in promise as log function only has callback. */
        return new Promise((resolve, reject) => {
            var _a, _b;
            /* Assume severity of info */
            const level = (_b = (_a = event) === null || _a === void 0 ? void 0 : _a.severity, (_b !== null && _b !== void 0 ? _b : 'info'));
            /* Note: order does not matter, however the first Object that contains at least one key from the list under request will be considered a request object. */
            const { uuid } = this.client.log(event.message, event.error, {
                level,
                emp: event.custom
            }, (err) => err ? reject(err) : resolve(uuid));
        });
    }
}
exports.RollbarClient = RollbarClient;
//# sourceMappingURL=index.js.map