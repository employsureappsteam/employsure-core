export declare type RollbarProjectName = 'ApiIntegrations';
export declare type RollbarClientOptions = {
    projectName?: string;
    access_token?: string;
    environment?: string;
};
export declare type RollbarSeverity = 'info' | 'debug' | 'warning' | 'error' | 'critical';
export declare type LogToRollbarEvent = {
    projectName?: string;
    message: string;
    severity?: RollbarSeverity;
    environment?: string;
    error?: Error;
    custom?: {};
};
