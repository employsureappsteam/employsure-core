import { LogToRollbarEvent, RollbarClientOptions } from './models';
import Rollbar from 'rollbar';
export declare class RollbarClient {
    options: RollbarClientOptions;
    client: Rollbar;
    constructor(options?: RollbarClientOptions);
    private initClient;
    private getRollbarAccessToken;
    log(event: LogToRollbarEvent): Promise<string>;
}
