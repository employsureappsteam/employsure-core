import { Table, TableMetadata } from "@google-cloud/bigquery";
/**
 * Replace Google's interface to credentials details.
 */
export interface BigQueryCredentials {
    client_email: string;
    private_key: string;
}
export interface IEmploysureInsertOpts {
    useLogProfile?: boolean;
    disableTimestampPatching?: boolean;
    chunkSize?: number;
}
export declare type IBigQuerySchemaType = 'STRING' | 'INTEGER' | 'FLOAT' | 'BOOLEAN' | 'TIMESTAMP' | 'DATE' | 'TIME' | 'RECORD';
export interface IBigQuerySchema {
    name: string;
    type: IBigQuerySchemaType;
    mode?: "NULLABLE" | "REPEATED";
    fields?: Array<IBigQuerySchema>;
}
export declare type TableCustomEmp = Omit<Table, 'metadata'> & {
    metadata: TableMetadataSummary;
};
export declare type TableMetadataSummary = {
    creationTime: string;
    id: string;
    kind: 'bigquery#table' | string;
    tableReference: TableReference;
    type: 'VIEW' | 'TABLE' | string;
};
export declare type TableReference = {
    projectId: string;
    datasetId: string;
    tableId: string;
};
export interface IGetDatasetTablesOpt {
    fetchOnlyViews: boolean;
}
export declare type TableEnriched = {
    table: TableCustomEmp;
    tableMetadata: TableMetadata;
};
export declare type CreateViewOptions = {
    createDataset?: boolean;
    ignoreIfViewExists?: boolean;
};
export declare type GetDatasetOptions = {
    createDataset?: boolean;
};
