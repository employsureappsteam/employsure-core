"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Errors that occur in Employsure's BigQueryClient
 */
class BigQueryClientError extends Error {
    constructor(message) {
        super(message);
        this.name = "BigQueryClientError";
    }
}
exports.BigQueryClientError = BigQueryClientError;
//# sourceMappingURL=bigQueryClientError.js.map