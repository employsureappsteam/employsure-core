import { ViewDefinition } from "@google-cloud/bigquery";
import { CreateViewOptions } from "../models";
export declare class CreateViewError extends Error {
    name: string;
    bigQueryError: any;
    datasetId: string;
    viewId: string;
    viewDef: ViewDefinition;
    options?: CreateViewOptions;
    constructor(error: Error, datasetId: string, viewId: string, viewDef: ViewDefinition, options?: CreateViewOptions);
}
