"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CreateViewError extends Error {
    constructor(error, datasetId, viewId, viewDef, options) {
        super(error.message);
        this.name = "CreateViewError";
        this.datasetId = datasetId;
        this.viewId = viewId;
        this.viewDef = viewDef;
        this.options = options;
    }
}
exports.CreateViewError = CreateViewError;
//# sourceMappingURL=createViewError.js.map