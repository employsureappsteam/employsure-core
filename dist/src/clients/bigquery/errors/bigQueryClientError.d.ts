/**
 * Errors that occur in Employsure's BigQueryClient
 */
export declare class BigQueryClientError extends Error {
    name: string;
    constructor(message: string);
}
