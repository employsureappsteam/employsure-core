import { BigQuery, DatasetDeleteOptions, DatasetResource, GetDatasetsOptions, ViewDefinition } from '@google-cloud/bigquery';
import { BigQueryCredentials, IBigQuerySchema, IEmploysureInsertOpts, IGetDatasetTablesOpt, TableEnriched, CreateViewOptions, GetDatasetOptions } from './models';
import { IClientOptions } from '../../helpers/clients/models';
/**
 * Lightweight wrapper around Google's BigQuery SDK.
 *
 * Many of the methods can and should be access directly by using Google's SDK.
 * Access these with: bigQueryClient.client.method()
 */
export declare class BigQueryClient {
    client: BigQuery;
    private options?;
    constructor(projectId: string, credentials: BigQueryCredentials, options?: IClientOptions);
    /**
     *
     * @param datasetId
     * Name of the dataset. Must exist already.
     *
     * @param tableId
     * Name of the table. If the table doesn't exist it will be created. This can take longer than 2 minutes.
     *
     * @param rows
     * Array of data to insert into table. Assumed all items have the same keys.
     *
     * @param options
     * Options to enable other behaviour
     *
     * For examples see: https://googleapis.dev/nodejs/bigquery/latest/Table.html#insert-examples
     */
    insertToTable(datasetId: string, tableId: string, rows: Array<{}>, schema?: Array<IBigQuerySchema>, options?: IEmploysureInsertOpts): Promise<{
        insertJobsDone: (import("@google-cloud/bigquery/build/src/types").default.ITable | import("@google-cloud/bigquery/build/src/types").default.ITableDataInsertAllResponse)[];
        employsureJob: {
            timestamp: Date;
            jobId: string;
        };
    }>;
    query<T>(query: string): Promise<T[]>;
    createDataset(id: string, options?: DatasetResource): Promise<import("@google-cloud/bigquery/build/src/dataset").Dataset>;
    doesDatasetExist(id: string): Promise<boolean>;
    getDatasets(options: GetDatasetsOptions): Promise<import("@google-cloud/bigquery").PagedResponse<import("@google-cloud/bigquery/build/src/dataset").Dataset, import("@google-cloud/bigquery").PagedRequest<import("@google-cloud/bigquery/build/src/types").default.datasets.IListParams>, import("@google-cloud/bigquery/build/src/types").default.IDatasetList>>;
    getAllDatasets(): Promise<import("@google-cloud/bigquery").PagedResponse<import("@google-cloud/bigquery/build/src/dataset").Dataset, import("@google-cloud/bigquery").PagedRequest<import("@google-cloud/bigquery/build/src/types").default.datasets.IListParams>, import("@google-cloud/bigquery/build/src/types").default.IDatasetList>>;
    getDataset(datasetId: string, options?: GetDatasetOptions): Promise<import("@google-cloud/bigquery/build/src/dataset").Dataset>;
    getDatasetTables(id: string, options?: IGetDatasetTablesOpt): Promise<TableEnriched[]>;
    /**
     * Creates a new view and returns view.
     *
     * If view exists and `options.ignoreIfViewExists` is set to true, the existing
     * view will be returned. If false, an error is thrown.
     *
     * @param datasetId
     * @param viewId
     * @param view
     * @param options
     * @returns
     */
    createView(datasetId: string, viewId: string, view: ViewDefinition, options?: CreateViewOptions): Promise<import("@google-cloud/bigquery/build/src/table").Table>;
    /**
     * Deletes a table/view from a dataset.
     *
     * @param datasetId
     * @param tableId
     */
    deleteTable(datasetId: string, tableId: string): Promise<import("teeny-request").Response<any>>;
    /**
     * Deletes a dataset
     *
     * @param datasetId the dataset name
     * @param options Use `force: true` to delete all tables and views as well in the dataset
     */
    deleteDataset(datasetId: string, options?: DatasetDeleteOptions): Promise<void>;
    /**
     * Helper function to validate schema before sending to BigQuery.
     *
     * @param rows rows that will be inserted
     * @param schema the schema for the rows that will be inserted
     */
    private validateSchema;
    /**
     * Log if verbose logging is set to true
     *
     * @param message string message to log.
     */
    private log;
}
