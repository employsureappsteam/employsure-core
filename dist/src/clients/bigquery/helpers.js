"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const const_1 = require("./const");
/**
 * Takes any value and patches to ISO string if possible. Otherwise returns original value.
 */
exports.patchToIsoString = (value) => {
    /* Convert Date/Time data types. */
    if (typeof value === "string" && value.match(const_1.salesforceTimestamp)) {
        return new Date(value).toISOString();
    }
    else {
        return value;
    }
};
/**
 * Takes any value and patches to ISO string if possible. Otherwise returns original value.
 */
exports.patchStringToTime = (value) => {
    /* Convert Date/Time data types. */
    if (typeof value === "string") {
        const matchObj = value.match(const_1.salesforceTime);
        if (matchObj) {
            return `${matchObj[1]}:${matchObj[2]}:${matchObj[3]}.${matchObj[4]}`;
        }
    }
    return value;
};
//# sourceMappingURL=helpers.js.map