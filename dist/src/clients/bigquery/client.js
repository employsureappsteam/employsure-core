"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bigquery_1 = require("@google-cloud/bigquery");
const big_query_1 = require("../../constants/big-query");
const array_1 = require("../../helpers/array");
const uuid_1 = require("uuid");
const helpers_1 = require("./helpers");
const bigQueryClientError_1 = require("./errors/bigQueryClientError");
const classLog_1 = require("../../helpers/clients/classLog");
const const_1 = require("./const");
const createViewError_1 = require("./errors/createViewError");
/**
 * Lightweight wrapper around Google's BigQuery SDK.
 *
 * Many of the methods can and should be access directly by using Google's SDK.
 * Access these with: bigQueryClient.client.method()
 */
class BigQueryClient {
    constructor(projectId, credentials, options) {
        /* Internal helper functions. */
        /**
         * Helper function to validate schema before sending to BigQuery.
         *
         * @param rows rows that will be inserted
         * @param schema the schema for the rows that will be inserted
         */
        this.validateSchema = (rows, schema) => {
            const dataKeys = [
                ...new Set(rows.map((m) => Object.keys(m))
                    .reduce((acc, keys) => [...acc, ...keys], []))
            ];
            dataKeys.forEach((d) => {
                let isFieldDefined = false;
                for (const s of schema) {
                    if (s.name === d) {
                        isFieldDefined = true;
                    }
                }
                if (!isFieldDefined) {
                    throw Error(`Missing schema for field ${d}`);
                }
            });
        };
        /**
         * Log if verbose logging is set to true
         *
         * @param message string message to log.
         */
        this.log = (method, message, logLevel) => {
            classLog_1.classLog("BigQueryClient", method, message, logLevel, this.options);
        };
        this.client = new bigquery_1.BigQuery({
            projectId,
            credentials,
        });
        this.options = options;
    }
    /**
     *
     * @param datasetId
     * Name of the dataset. Must exist already.
     *
     * @param tableId
     * Name of the table. If the table doesn't exist it will be created. This can take longer than 2 minutes.
     *
     * @param rows
     * Array of data to insert into table. Assumed all items have the same keys.
     *
     * @param options
     * Options to enable other behaviour
     *
     * For examples see: https://googleapis.dev/nodejs/bigquery/latest/Table.html#insert-examples
     */
    async insertToTable(datasetId, tableId, rows, schema, options) {
        var _a, _b;
        this.log("insertToTable", `datasetId: ${datasetId}, tableId: ${tableId}, data.length: ${rows.length}`);
        this.log("insertToTable", `options: ${JSON.stringify(options)}`);
        /* Get the keys for each of the */
        if (schema !== undefined) {
            this.validateSchema(rows, schema);
        }
        /** Convert booleans to string as BigQuery doesn't support it. (TODO: what does this mean?)
         *
         *  Error: "Conversion from bool to string is unsupported"
         */
        if (schema !== undefined) {
            const booleanFields = schema.filter((s) => s.type === 'BOOLEAN');
            for (const field of booleanFields) {
                for (const _d of rows) {
                    _d[field.name] = _d[field.name] ? 'true' : 'false';
                }
            }
        }
        /* Convert unsupported timestamp strings to expected format. */
        /* e.g. 'Could not parse '2020-04-29T02:25:16.000+0000' as a timestamp. Required format is YYYY-MM-DD HH:MM[:SS[.SSSSSS]]' */
        if (!options || !options.disableTimestampPatching) {
            rows = rows.map(d => array_1.mapValuesDeep(d, helpers_1.patchToIsoString));
        }
        rows = rows.map(d => array_1.mapValuesDeep(d, helpers_1.patchStringToTime));
        const timestamp = new Date();
        const jobId = uuid_1.v4();
        if (options && options.useLogProfile) {
            /* TODO: deep copy this array instead (lodash) */
            rows = rows.map((d) => {
                d[big_query_1.EMPLOYSURE_LOG_TIMESTAMP_FIELD_NAME] = timestamp.toISOString();
                d[big_query_1.EMPLOYSURE_LOG_JOB_ID_FIELD_NAME] = jobId;
                return d;
            });
            if (schema !== undefined) {
                schema.push({
                    name: big_query_1.EMPLOYSURE_LOG_TIMESTAMP_FIELD_NAME,
                    type: 'TIMESTAMP',
                });
                schema.push({
                    name: big_query_1.EMPLOYSURE_LOG_JOB_ID_FIELD_NAME,
                    type: 'STRING',
                });
            }
        }
        const da = this.getDataset(datasetId);
        const dataset = this.client.dataset(datasetId);
        const table = dataset.table(tableId);
        /* Chunk the rows into updates, send to bigquery in parallel, and wait. */
        const chunkSize = (_b = (_a = options) === null || _a === void 0 ? void 0 : _a.chunkSize, (_b !== null && _b !== void 0 ? _b : const_1.DEFAULT_INSERT_CHUNK_SIZE));
        this.log("insertToTable", `chunkSize: ${chunkSize}`);
        const insertJobsPending = array_1.chunkArray(rows, chunkSize)
            .map(async (row) => {
            const [insertRowResponse] = await table.insert(row, { schema });
            return insertRowResponse;
        });
        const insertJobsDone = (await Promise.all(insertJobsPending));
        this.log("insertToTable", `finished ${insertJobsDone.length} insert jobs`);
        return {
            insertJobsDone,
            employsureJob: {
                timestamp,
                jobId,
            }
        };
    }
    async query(query) {
        this.log("query", `running`);
        const options = {
            query,
            useLegacySql: false,
        };
        const [job] = await this.client.createQueryJob(options);
        await job.promise();
        const [result] = await job.getQueryResults();
        this.log("query", `done. results.length: ${result.length}`);
        return result;
    }
    async createDataset(id, options) {
        const [dataset, apiresponse] = await this.client.createDataset(id, options);
        return dataset;
    }
    async doesDatasetExist(id) {
        const dataset = await this.getDataset(id);
        const [result] = await dataset.exists();
        return result;
    }
    async getDatasets(options) {
        return this.client.getDatasets(options);
    }
    async getAllDatasets() {
        const params = {
            autoPaginate: true,
            all: true
        };
        return this.client.getDatasets(params);
    }
    async getDataset(datasetId, options) {
        var _a;
        const dataset = this.client.dataset(datasetId);
        const [exists] = await dataset.exists();
        if (!exists && ((_a = options) === null || _a === void 0 ? void 0 : _a.createDataset) === true) {
            this.log(`getDataset`, `dataset already exists, creating`);
            await this.createDataset(datasetId);
        }
        else if (!exists) {
            this.log(`getDataset`, `dataset already exists, throwing error`);
            throw new bigQueryClientError_1.BigQueryClientError(`Dataset ${datasetId} does not exist.`);
        }
        return dataset;
    }
    async getDatasetTables(id, options) {
        var _a;
        const dataset = this.client.dataset(id);
        const [tables] = await dataset.getTables();
        let tablesMd = tables;
        if ((_a = options) === null || _a === void 0 ? void 0 : _a.fetchOnlyViews) {
            tablesMd = tablesMd.filter(t => t.metadata.type === "VIEW");
        }
        const tableEnr = await Promise.all(tablesMd.map(async (table) => ({
            table,
            tableMetadata: (await table.getMetadata())[0]
        })));
        return tableEnr;
    }
    /**
     * Creates a new view and returns view.
     *
     * If view exists and `options.ignoreIfViewExists` is set to true, the existing
     * view will be returned. If false, an error is thrown.
     *
     * @param datasetId
     * @param viewId
     * @param view
     * @param options
     * @returns
     */
    async createView(datasetId, viewId, view, options) {
        var _a;
        /* Fetch the dataset referece. Create if doesn't exist. */
        const dataset = await this.getDataset(datasetId, options);
        /* Create the new view. */
        try {
            const [newView] = await dataset.createTable(viewId, { view });
            this.log(`createView`, `done`);
            return newView;
        }
        catch (error) {
            if (error.errors[0].reason === "duplicate" && ((_a = options) === null || _a === void 0 ? void 0 : _a.ignoreIfViewExists)) {
                this.log(`createView`, `view ${viewId} already exists in dataset ${datasetId}`);
                return dataset.table(viewId);
            }
            throw new createViewError_1.CreateViewError(error, datasetId, viewId, view, options);
        }
    }
    /**
     * Deletes a table/view from a dataset.
     *
     * @param datasetId
     * @param tableId
     */
    async deleteTable(datasetId, tableId) {
        this.log(`deleteTable`, `datasetId: ${datasetId}, tableId: ${tableId}`);
        const dataset = this.client.dataset(datasetId);
        const table = dataset.table(tableId);
        const [res] = await table.delete();
        this.log(`deleteTable`, `done`);
        return res;
    }
    /**
     * Deletes a dataset
     *
     * @param datasetId the dataset name
     * @param options Use `force: true` to delete all tables and views as well in the dataset
     */
    async deleteDataset(datasetId, options) {
        this.log("deleteDataset", `datasetId: ${datasetId}`);
        const dataset = this.client.dataset(datasetId);
        await dataset.delete(options);
        this.log(`deleteDataset`, `done`);
    }
}
exports.BigQueryClient = BigQueryClient;
//# sourceMappingURL=client.js.map