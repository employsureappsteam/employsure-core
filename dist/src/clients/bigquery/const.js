"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.salesforceTimestamp = new RegExp(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}\+\d+$/);
exports.salesforceTime = new RegExp(/^(\d{2}):(\d{2}):(\d{2})\.(\d+)Z$/);
/* How many rows to send to bigquery at a time if not specified. */
exports.DEFAULT_INSERT_CHUNK_SIZE = 50;
//# sourceMappingURL=const.js.map