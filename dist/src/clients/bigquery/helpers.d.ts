/**
 * Takes any value and patches to ISO string if possible. Otherwise returns original value.
 */
export declare const patchToIsoString: (value: any) => any;
/**
 * Takes any value and patches to ISO string if possible. Otherwise returns original value.
 */
export declare const patchStringToTime: (value: any) => any;
