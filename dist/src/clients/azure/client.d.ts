import { IAzureClientConfig, IFieldValueSet, IGetDriveParameters, IGetOptions, ISharepointListItem, ISharepointListItemUpdateResponse, IUploadFileParamaters } from "./models";
import { IAzureUser, IAzureGroup, IAzureGroupMember, DriveItem } from "./models-rest";
export declare class AzureClient {
    private _api;
    private _config;
    constructor(config: IAzureClientConfig);
    private checkClientIsInitialised;
    init(): Promise<void>;
    /** Fetch all users on Azure AD */
    getUsers(): Promise<Array<IAzureUser>>;
    /**
     * This is a wrapped around endpoints that return lists. See here: https://docs.microsoft.com/en-us/rest/api/azure/#async-operations-throttling-and-paging
     * From the docs: "Some list operations return a property called nextLink in the response body. You see this property when the results are too large to return in one response. Typically, the response includes the nextLink property when the list operation returns more than 1,000 items. When nextLink isn't present in the results, the returned results are complete. When nextLink contains a URL, the returned results are just part of the total result set."
     *
     * "OData query parameters" can be used to operate on the data before it is returned. Read more here: https://docs.microsoft.com/en-us/graph/query-parameters. Note not all endpoints support all OData query parameters. Check endpoint documentation for what specifically is supported.
     *
     * @param endpoint endpoint to access the list (not including base url, e.g. auditLogs/signIns)
     * @param options use this to limit the number of requests made as there is no way of telling how many items there might be in the list. And thus no way to tell how many requests will be made. Use $top=999 to return the maximum number of items in one request.
     */
    getList<T = any>(endpoint: string, options?: IGetOptions): Promise<Array<T>>;
    getGroups(): Promise<Array<IAzureGroup>>;
    getGroupMembers(groupId: string): Promise<Array<IAzureGroupMember>>;
    addGroupMember(groupId: string, memberId: string): Promise<null>;
    removeGroupMember(groupId: string, memberId: string): Promise<null>;
    updateSharepointListItem<T = {
        [key: string]: any;
    }>(item: ISharepointListItem, fieldValueSet: IFieldValueSet): Promise<ISharepointListItemUpdateResponse & T>;
    /**
     * Uploads a file to a Sharepoint Drive. Will overwrite file if exists.
     *
     * @param param site, drive, and file details
     *
     * https://docs.microsoft.com/en-us/graph/api/driveitem-put-content?view=graph-rest-1.0
     */
    uploadFile(param: IUploadFileParamaters): Promise<DriveItem>;
    getDrive(param: IGetDriveParameters): Promise<import("./models-rest").Drive>;
    private _handleError;
}
