"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const qs = __importStar(require("querystring"));
const models_errors_1 = require("./models-errors");
class AzureClient {
    constructor(config) {
        this._config = config;
    }
    /* Internal helper function to initialise the client first time a method is used. Add to all methods. */
    async checkClientIsInitialised() {
        if (!this._api) {
            await this.init();
        }
    }
    async init() {
        const tenantId = this._config.tenantId;
        const client_secret = this._config.clientSecret;
        const client_id = this._config.clientId;
        /* Note: The scope “https://graph.microsoft.com/.default” tells the Microsoft Graph to return a token with the application permissions already configured for the Azure AD app. */
        const scope = 'https://graph.microsoft.com/.default';
        const grant_type = 'client_credentials';
        const bodyEncoded = qs.stringify({
            grant_type,
            client_id,
            client_secret,
            scope
        });
        const loginData = await axios_1.default
            .post(`https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`, bodyEncoded, {
            headers: {
                "Content-Type": 'application/x-www-form-urlencoded'
            }
        }).then(response => response.data);
        this._api = axios_1.default.create({
            baseURL: 'https://graph.microsoft.com/v1.0/',
            headers: {
                'Authorization': `Bearer ${loginData.access_token}`,
                'Content-Type': 'application/json'
            }
        });
    }
    /** Fetch all users on Azure AD */
    async getUsers() {
        const graphQlFields = [
            'accountEnabled',
            'DisplayName',
            'mail',
            'id',
            'userPrincipalName'
        ];
        let query = `users?$select=${graphQlFields.join(",")}&$top=999`;
        return this.getList(query);
    }
    /**
     * This is a wrapped around endpoints that return lists. See here: https://docs.microsoft.com/en-us/rest/api/azure/#async-operations-throttling-and-paging
     * From the docs: "Some list operations return a property called nextLink in the response body. You see this property when the results are too large to return in one response. Typically, the response includes the nextLink property when the list operation returns more than 1,000 items. When nextLink isn't present in the results, the returned results are complete. When nextLink contains a URL, the returned results are just part of the total result set."
     *
     * "OData query parameters" can be used to operate on the data before it is returned. Read more here: https://docs.microsoft.com/en-us/graph/query-parameters. Note not all endpoints support all OData query parameters. Check endpoint documentation for what specifically is supported.
     *
     * @param endpoint endpoint to access the list (not including base url, e.g. auditLogs/signIns)
     * @param options use this to limit the number of requests made as there is no way of telling how many items there might be in the list. And thus no way to tell how many requests will be made. Use $top=999 to return the maximum number of items in one request.
     */
    async getList(endpoint, options) {
        var _a;
        await this.checkClientIsInitialised();
        const maxRequests = (_a = options) === null || _a === void 0 ? void 0 : _a.maxRequests;
        const data = [];
        let requestsMade = 0;
        while (endpoint) {
            const response = await this._api
                .get(endpoint)
                .then(response => response.data)
                .catch(error => { throw this._handleError(error); });
            if (response.value === undefined) {
                throw new Error(`Invalid response from ${endpoint} ($.value is missing).`);
            }
            data.push(...response.value);
            endpoint = response["@odata.nextLink"];
            requestsMade = requestsMade + 1;
            if (maxRequests && requestsMade >= maxRequests) {
                break;
            }
        }
        return data;
    }
    async getGroups() {
        await this.checkClientIsInitialised();
        const endpoint = `groups/?$top=999`;
        return this.getList(endpoint);
    }
    async getGroupMembers(groupId) {
        await this.checkClientIsInitialised();
        const endpoint = `groups/${groupId}/members?$top=999`;
        return this.getList(endpoint);
    }
    async addGroupMember(groupId, memberId) {
        await this.checkClientIsInitialised();
        const endpoint = `groups/${groupId}/members/$ref`;
        const body = {
            "@odata.id": `https://graph.microsoft.com/v1.0/directoryObjects/${memberId}`
        };
        try {
            await this._api.post(endpoint, body);
            /* 204 returned on success */
            return null;
        }
        catch (rtError) {
            throw this._handleError(rtError);
        }
    }
    async removeGroupMember(groupId, memberId) {
        await this.checkClientIsInitialised();
        const endpoint = `groups/${groupId}/members/${memberId}/$ref`;
        try {
            await this._api.delete(endpoint);
            /* 204 returned on success */
            return null;
        }
        catch (rtError) {
            throw this._handleError(rtError);
        }
    }
    async updateSharepointListItem(item, fieldValueSet) {
        await this.checkClientIsInitialised();
        const endpoint = `sites/${item.siteId}/lists/${item.listId}/items/${item.itemId}/fields`;
        try {
            const d = await this._api.patch(endpoint, fieldValueSet);
            return d.data;
        }
        catch (e) {
            throw this._handleError(e);
        }
    }
    /**
     * Uploads a file to a Sharepoint Drive. Will overwrite file if exists.
     *
     * @param param site, drive, and file details
     *
     * https://docs.microsoft.com/en-us/graph/api/driveitem-put-content?view=graph-rest-1.0
     */
    async uploadFile(param) {
        var _a;
        await this.checkClientIsInitialised();
        const { siteId, drive, file } = param;
        if (drive.name === undefined && drive.id === undefined) {
            throw new models_errors_1.AzureUserError(`Must provide either drive name or id`);
        }
        const driveId = (_a = drive.id, (_a !== null && _a !== void 0 ? _a : (await this.getDrive({ siteId, driveName: drive.name })).id));
        const endpoint = `sites/${param.siteId}/drives/${driveId}/items/root:/${file.filename}:/content`;
        try {
            const axiosConfig = { headers: { "Content-Type": file.type } };
            const d = await this._api.put(endpoint, file.body, axiosConfig);
            return d.data;
        }
        catch (e) {
            throw this._handleError(e);
        }
    }
    async getDrive(param) {
        await this.checkClientIsInitialised();
        const endpoint = `sites/${param.siteId}/drives`;
        try {
            const response = await this._api.get(endpoint);
            for (const drive of response.data.value) {
                if (drive.name === param.driveName) {
                    return drive;
                }
            }
            throw new models_errors_1.AzureUserError(`Cannot find drive '${param.driveName}'`);
        }
        catch (e) {
            throw this._handleError(e);
        }
    }
    /* Throw and wrap error if from axios, otherwise rethrow unknown error. */
    _handleError(rtError) {
        var _a, _b, _c, _d, _e, _f, _g, _h;
        if (rtError.isAxiosError) {
            const respData = (_b = (_a = rtError) === null || _a === void 0 ? void 0 : _a.response) === null || _b === void 0 ? void 0 : _b.data;
            const message = (_f = (_e = (_d = (_c = rtError) === null || _c === void 0 ? void 0 : _c.response) === null || _d === void 0 ? void 0 : _d.data) === null || _e === void 0 ? void 0 : _e.error) === null || _f === void 0 ? void 0 : _f.message;
            const status = (_h = (_g = rtError) === null || _g === void 0 ? void 0 : _g.response) === null || _h === void 0 ? void 0 : _h.status;
            return new models_errors_1.AzureResponseError((status !== null && status !== void 0 ? status : 500), (respData !== null && respData !== void 0 ? respData : {}), `${message}`);
        }
        else {
            return rtError;
        }
    }
}
exports.AzureClient = AzureClient;
//# sourceMappingURL=client.js.map