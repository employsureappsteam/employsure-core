"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AzureUserError extends Error {
    constructor(message) {
        super(message);
        this.name = "AzureBadRequestError";
    }
}
exports.AzureUserError = AzureUserError;
class AzureResponseError extends Error {
    constructor(status, data, message) {
        super(message);
        this.name = "AzureResponseError";
        this.status = status;
        this.data = data;
    }
}
exports.AzureResponseError = AzureResponseError;
//# sourceMappingURL=models-errors.js.map