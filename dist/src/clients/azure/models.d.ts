export interface IAzureClientConfig {
    clientId: string;
    clientSecret: string;
    tenantId: string;
}
export interface IGetOptions {
    maxRequests?: number;
}
export interface ISharepointListItem {
    siteId: string;
    listId: string;
    itemId: string;
}
export declare type ISharepointFieldValue = string | number | boolean;
export interface IFieldValueSet {
    [key: string]: ISharepointFieldValue;
}
export interface ISharepointListItemUpdateResponse {
    "@odata.context": string;
    "@odata.etag": string;
    id: string;
    ContentType: string;
    Modified: Date;
    Created: Date;
    AuthorLookupId: string;
    EditorLookupId: string;
    _UIVersionString: string;
    Attachments: boolean;
    Edit: string;
    LinkTitleNoMenu: string;
    LinkTitle: string;
    ItemChildCount: string;
    FolderChildCount: string;
    _ComplianceFlags: string;
    _ComplianceTag: string;
    _ComplianceTagWrittenTime: string;
    _ComplianceTagUserId: string;
    AppEditorLookupId: string;
}
export interface IUploadFileParamaters {
    siteId: string;
    drive: {
        name?: string;
        id?: string;
    };
    file: {
        body: any;
        type: string;
        filename: string;
    };
}
export interface IGetDriveParameters {
    siteId: string;
    driveName: string;
}
