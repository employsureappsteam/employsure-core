export declare class AzureUserError extends Error {
    name: string;
    constructor(message: string);
}
export declare class AzureResponseError<T = any> extends Error {
    name: string;
    status: number;
    data: T;
    constructor(status: number, data: T, message: string);
}
