export interface IAzureLoginResponse {
    "token_type": string;
    "expires_in": number;
    "ext_expires_in": number;
    "access_token": string;
}
export interface IGraphQlResponse<T> {
    "@odata.context": string;
    "@odata.nextLink": string;
    value: Array<T>;
}
export interface IAzureUser {
    "accountEnabled": string;
    "displayName": string;
    "mail": string;
    "id": string;
    "userPrincipalName": string;
}
export interface IAzureGroup {
    id: string;
    deletedDateTime: null;
    classification: null;
    createdDateTime: Date;
    creationOptions: any[];
    description: null;
    displayName: string;
    expirationDateTime: null;
    groupTypes: any[];
    isAssignableToRole: null;
    mail: null;
    mailEnabled: boolean;
    mailNickname: string;
    membershipRule: null;
    membershipRuleProcessingState: null;
    onPremisesDomainName: string;
    onPremisesLastSyncDateTime: Date;
    onPremisesNetBiosName: string;
    onPremisesSamAccountName: string;
    onPremisesSecurityIdentifier: string;
    onPremisesSyncEnabled: boolean;
    preferredDataLocation: null;
    preferredLanguage: null;
    proxyAddresses: any[];
    renewedDateTime: Date;
    resourceBehaviorOptions: any[];
    resourceProvisioningOptions: any[];
    securityEnabled: boolean;
    securityIdentifier: string;
    theme: null;
    visibility: null;
    onPremisesProvisioningErrors: any[];
}
export interface IHttpBodyError {
    error: Error;
}
export interface Error {
    code: string;
    message: string;
    innerError: InnerError;
}
export interface InnerError {
    "request-id": string;
    date: Date;
}
export interface IAzureGroupMember {
    "@odata.type": string;
    id: string;
    businessPhones: any[];
    displayName: string;
    givenName: string;
    jobTitle: string;
    mail: string;
    mobilePhone: null;
    officeLocation: string;
    preferredLanguage: null;
    surname: string;
    userPrincipalName: string;
}
export interface GetDriveResponse {
    "@odata.context": string;
    value: Drive[];
}
export interface Drive {
    createdDateTime: Date;
    description: string;
    id: string;
    lastModifiedDateTime: Date;
    name: string;
    webUrl: string;
    driveType: string;
    createdBy: EditedBy;
    lastModifiedBy: EditedBy;
    owner: EditedBy;
    quota: Quota;
}
export interface EditedBy {
    user?: User;
    application?: Application;
}
export interface User {
    displayName: string;
    email?: string;
    id?: string;
}
export interface Quota {
    deleted: number;
    remaining: number;
    state: string;
    total: number;
    used: number;
}
export interface DriveItem {
    "@odata.context": string;
    "@microsoft.graph.downloadUrl": string;
    createdDateTime: Date;
    eTag: string;
    id: string;
    lastModifiedDateTime: Date;
    name: string;
    webUrl: string;
    cTag: string;
    size: number;
    createdBy: EditedBy;
    lastModifiedBy: EditedBy;
    parentReference: ParentReference;
    file: File;
    fileSystemInfo: FileSystemInfo;
}
export interface Application {
    id: string;
    displayName: string;
}
export interface File {
    mimeType: string;
    hashes: Hashes;
}
export interface Hashes {
    quickXorHash: string;
}
export interface FileSystemInfo {
    createdDateTime: Date;
    lastModifiedDateTime: Date;
}
export interface ParentReference {
    driveId: string;
    driveType: string;
    id: string;
    path: string;
}
