import { ApexOptions, SObjectType, sObject, CompositeRecordCrudOptions, CompositeRecordList, CompositeResponseBody } from './models/salesforce';
import { SobjectDescribe } from './models/data/describe';
import { SalesforceConfig, ISfQueryOptions } from './models';
/**
 * REST API client for Salesforce.
 *
 * See official REST API Developer guide here:
 * https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/quickstart.htm
 *
 * @remarks
 * Development notes:
 * This client originally used nforce (salesforce wrapper) only. To accomodate
 * for missing features, there is an internal axios client also used. The nforce
 * client is kept for backwards compatability. The alternative methods using
 * axios are suffixed with a 2.
 *
 * e.g. `query()` uses nforce client while `query2()` uses axios client.
 *
 * The nforce client is the private field `connection`.
 * The axios client is the private field `axios`.
 */
export declare class SalesforceClient {
    private userConfig;
    private connection;
    private axios;
    constructor(config: SalesforceConfig);
    /**
     * Authenticates with salesforce
     */
    authenticateClient(): Promise<void>;
    /**
     * Confirm that the connection has been made already
     */
    checkClientIsAuthenticated(): Promise<void>;
    getInstanceUrl(): Promise<any>;
    /**
     * Gets the picklist labels and values for a partocular sObject field
     * @param {string} sObjectName Salesforce Object name
     * @param {string} fieldName Salesforce Object field name
     */
    getPicklistValues(sObjectName: string, fieldName: string): Promise<any>;
    /**
     * Running into bugs (seemingly) in nforce.
     */
    get2: <T>(url: string) => Promise<T>;
    query2: <T = {}>(queryStr: string) => Promise<T[]>;
    private pageQueryResult;
    /**
     * Executes a SOQL query against the Salesforce connection
     * @param {string} soqlQuery SOQL Query string
     * @param {Object} options { toJson: }
     */
    query(soqlQuery: string, options?: ISfQueryOptions): Promise<Array<sObject>>;
    /**
     * Gets a single record from Salesforce given the object type, id and fields
     * @param {string} type Salesforce SObject name
     * @param {string} id Salesforce record id
     * @param {Array<string>} fields Fields to return
     */
    getRecord(type: string, id: string, fields: string[]): Promise<any>;
    /**
     * Creates a Salesforce sobject
     *
     * POST /services/data/v44.0/sobjects/<sobjectType>/
     *
     * @param object
     * @param sobjectType
     * @returns {Promise.<string>} id or null if unsuccessful
     */
    createRecord(object: {}, sobjectType: SObjectType): Promise<{
        success: boolean;
        status: string;
        message: string;
        id?: string;
    }>;
    /**
     * Update a Salesforce sobject
     * @param {Object} sobject object to update
     */
    updateRecord(object: {}, sobjectType: SObjectType): Promise<string>;
    deleteRecords(ids: Array<string>, options?: {
        maxParallel?: number;
        chunkSize?: number;
    }): Promise<CompositeResponseBody>;
    /**
     * Simple wrapper around composite rest API for inserting records.
     */
    insertRecords(records: CompositeRecordList, options?: {
        maxParallel?: number;
        chunkSize?: number;
    }): Promise<CompositeResponseBody>;
    /**
     * Simple wrapper around composite rest API for inserting records.
     */
    updateRecords(records: CompositeRecordList, options?: CompositeRecordCrudOptions): Promise<CompositeResponseBody>;
    /**
     * Scans an object for any subquery objects and flattens them
     * @param {Object} object Object to scan
     * @param {Object} aliases Object that contains aliases as key value pairs
     */
    cleanSubqueries(object: any, aliases?: {}): any;
    /**
     * Flattens a subquery object into its field values.
     * Lower cases keys
     * @param {Object} object Subquery Object to flatten
     */
    flattenSubquery(object: any): any;
    /**
     * Lower cases an object's keys and removes attributes field
     * @param {Object} object Subquery Object to flatten
     */
    lowerCaseObjectKeys(object: any): {} | null;
    /**
     * Typed wrapper around nforce.
     **/
    apexRest<T>(opts: ApexOptions): Promise<T>;
    rest<Req, Resp>(method: string, url: string, data?: Req): Promise<Resp>;
    sObjectDescribe(sObject: string): Promise<SobjectDescribe>;
    /**
     * Checks whether or not an input is sanitised to be an id
     * @param {String} input id string
     */
    isIdSanitised(input: any): boolean;
    /**
     * Checks whether or not an input is sanitised to be an email
     * @param {*} input email string
     */
    isEmailSanitised(input: any): boolean;
    getRecordTypeId(recordType: string, sObjectType: SObjectType): Promise<any>;
    private cleanSoqlQueryResults;
    /**
     *
     * @param method name of method this is called from
     * @param message the message
     * @param logLevel the severity of message
     */
    private log;
    private logWarning;
    private logError;
}
