import { SalesforceQueryResult } from "../models";
export declare function IsSalesforceQueryResult(value: SalesforceQueryResult): value is SalesforceQueryResult;
