"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function IsSalesforceQueryResult(value) {
    return value !== undefined
        && value !== null
        && typeof value.done === "boolean"
        && typeof value.totalSize === "number"
        && Array.isArray(value.records);
}
exports.IsSalesforceQueryResult = IsSalesforceQueryResult;
//# sourceMappingURL=IsSalesforceQueryResult.js.map