import { SalesforceClientOauth } from '../models/index';
import { SalesforceConfig } from '../models';
export interface IDistributedSalesforceOauthRecord {
    oauth?: SalesforceClientOauth;
    updateOauthCache?: (oauth: SalesforceClientOauth) => Promise<void>;
}
/**
 * Fetches the Salesforce Oauth token for a given connection (user + app).
 *
 * If there is no existing oauth or the oauth has expired, a function is returned
 * update the distributed oauth once it is generated by the callee.
 *
 * If there is an existing oauth, it is return.
 *
 * @param config the config for the Salesforce connection
 * @returns In an object, either the oauth JSON or a callback to update the oauth
 */
export declare const fetchDistributedSalesforceOauth: (config: SalesforceConfig) => Promise<IDistributedSalesforceOauthRecord>;
