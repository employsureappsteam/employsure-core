export declare type SObjectType = string;
export interface ApexOptions {
    uri: string;
    method: 'GET' | 'POST' | 'PUT' | 'PATCH';
    urlParams?: {} | string;
    body?: {};
}
/**
 * Response from calling /composite/sobject
 */
export declare type CompositeRequestBody = {
    allOrNone: boolean;
    records: sObject[];
};
export declare type CompositeResponseBody = Array<CompositeResponseRecord>;
export declare type CompositeResponseRecord = {
    id: string;
    success: boolean;
    errors: CompositeResponseErrors;
};
export declare type CompositeResponseErrors = Array<CompositeResponseError>;
export declare type CompositeResponseError = {
    statusCode?: string;
    message?: string;
    fields?: Array<string>;
};
export declare type sObjectValue = any;
export declare class sObject {
    constructor(type: SObjectType);
    attributes: Attributes;
    id?: string;
    [field: string]: sObjectValue;
}
export interface Attributes {
    type: SObjectType;
}
export declare type CompositeRecordList = Array<CompositeRecord>;
export declare type CompositeRecord = {
    attributes: Attributes;
    [field: string]: sObjectValue;
};
export declare type CompositeRecordCrudOptions = {
    maxParallel?: number;
    chunkSize?: number;
};
