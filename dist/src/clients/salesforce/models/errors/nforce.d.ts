export declare class NforceError extends Error {
    name: string;
    errorCode: string;
    isOperational: boolean;
    message: string;
    stack: string;
    statusCode: number;
    constructor(name: string);
}
