"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* An error raised by enforce. */
class NforceError extends Error {
    constructor(name) {
        super();
        this.name = name;
    }
}
exports.NforceError = NforceError;
//# sourceMappingURL=nforce.js.map