"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaults = {
    query: {
        TO_JSON: true,
        FETCH_ALL: true,
        FETCH_ALL_CHILDREN: true,
    }
};
//# sourceMappingURL=defaults.js.map