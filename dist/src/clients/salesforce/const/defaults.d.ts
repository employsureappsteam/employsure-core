export declare const defaults: {
    query: {
        TO_JSON: boolean;
        FETCH_ALL: boolean;
        FETCH_ALL_CHILDREN: boolean;
    };
};
