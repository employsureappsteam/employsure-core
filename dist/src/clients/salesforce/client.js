"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const IsSalesforceQueryResult_1 = require("./helpers/IsSalesforceQueryResult");
const salesforce_1 = require("../../constants/salesforce");
const defaults_1 = require("./const/defaults");
const nforce_1 = require("./models/errors/nforce");
const axios_1 = __importDefault(require("axios"));
const distributedOauth_1 = require("./helpers/distributedOauth");
const classLog_1 = require("../../helpers/clients/classLog");
const sleep_1 = require("../../helpers/sleep");
const promiseMapLimit = require("promise-map-limit");
const lodash_1 = __importDefault(require("lodash"));
var nforce = require('nforce');
const RETRY_ATTEMPTS = 2;
const INVALID_DATA_ERROR_CODES = [
    'STRING_TOO_LONG',
    'INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST',
    'REQUIRED_FIELD_MISSING',
    'INVALID_EMAIL_ADDRESS',
    'FIELD_CUSTOM_VALIDATION_EXCEPTION',
];
/**
 * REST API client for Salesforce.
 *
 * See official REST API Developer guide here:
 * https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/quickstart.htm
 *
 * @remarks
 * Development notes:
 * This client originally used nforce (salesforce wrapper) only. To accomodate
 * for missing features, there is an internal axios client also used. The nforce
 * client is kept for backwards compatability. The alternative methods using
 * axios are suffixed with a 2.
 *
 * e.g. `query()` uses nforce client while `query2()` uses axios client.
 *
 * The nforce client is the private field `connection`.
 * The axios client is the private field `axios`.
 */
class SalesforceClient {
    constructor(config) {
        /**
         * Running into bugs (seemingly) in nforce.
         */
        this.get2 = async (url) => {
            if (!url.startsWith('https')) {
                url = `${this.connection.oauth.instance_url}/${url}`;
            }
            const response = await this.axios.get(url);
            return response.data;
        };
        /* Improved query function. */
        this.query2 = async (queryStr) => {
            await this.checkClientIsAuthenticated();
            this.log('query2', queryStr);
            const queryStrEnc = encodeURIComponent(queryStr);
            const { instance_url } = this.connection.oauth;
            const { apiVersion } = this.connection;
            const url = `${instance_url}/services/data/${apiVersion}/query/?q=${queryStrEnc}`;
            /* Fetch results*/
            const data = await this.get2(url);
            await this.pageQueryResult(data);
            /* Fetch all other pages. */
            while (data.nextRecordsUrl !== undefined) {
                this.log(`query2`, `${data.records.length} / ${data.totalSize}`);
                const nextResponse = await this.get2(data.nextRecordsUrl);
                await this.pageQueryResult(nextResponse);
                data.nextRecordsUrl = nextResponse.nextRecordsUrl;
                data.done = nextResponse.done;
                data.records.push(...nextResponse.records);
            }
            const d2 = this.cleanSoqlQueryResults(data);
            return d2;
        };
        this.pageQueryResult = async (data) => {
            for (const r of data.records) {
                for (const [att, val] of Object.entries(r)) {
                    if (IsSalesforceQueryResult_1.IsSalesforceQueryResult(val)) {
                        while (val.nextRecordsUrl !== undefined) {
                            this.log(`pageQueryResult`, `${att} - ${val.records.length} / ${val.totalSize}`);
                            const nextResponse = await this.get2(val.nextRecordsUrl);
                            val.nextRecordsUrl = nextResponse.nextRecordsUrl;
                            val.done = nextResponse.done;
                            val.records.push(...nextResponse.records);
                        }
                    }
                }
            }
        };
        this.cleanSoqlQueryResults = (result) => {
            return result.records
                .map(rec => {
                const childRecordKeys = [];
                const removeKeys = [];
                for (const [key, val] of Object.entries(rec)) {
                    if (IsSalesforceQueryResult_1.IsSalesforceQueryResult(val)) {
                        childRecordKeys.push(key);
                    }
                    if (key === "attributes") {
                        removeKeys.push(key);
                    }
                }
                for (const key of childRecordKeys) {
                    rec[key] = this.cleanSoqlQueryResults(rec[key]);
                }
                for (const key of removeKeys) {
                    delete rec[key];
                }
                return rec;
            });
        };
        /**
         *
         * @param method name of method this is called from
         * @param message the message
         * @param logLevel the severity of message
         */
        this.log = (method, message, logLevel) => {
            classLog_1.classLog('SalesforceClient', method, message, logLevel, this.userConfig);
        };
        this.logWarning = (method, message) => this.log(method, message, 'warn');
        this.logError = (method, message) => this.log(method, message, 'error');
        this.userConfig = config;
    }
    /**
     * Authenticates with salesforce
     */
    async authenticateClient() {
        var _a, _b, _c, _d, _e, _f;
        /* Try and fetch cached oauth from SSM. */
        let distributedOauth = undefined;
        this.log(`authenticateClient`, `username: ${this.userConfig.username}, clientId: ${this.userConfig.clientId}`);
        if (this.userConfig.disableDistributedAuth !== true) {
            this.log(`authenticateClient`, `using distributed auth`);
            distributedOauth = await distributedOauth_1.fetchDistributedSalesforceOauth(this.userConfig);
        }
        /* Use settings from user config and fallback on environment stage if necessary. */
        const environment = (_b = (_a = this.userConfig.environment) === null || _a === void 0 ? void 0 : _a.toLowerCase(), (_b !== null && _b !== void 0 ? _b : (process.env.stage === 'Prod' ? 'production' : 'sandbox')));
        const oAuthHost = (_c = this.userConfig.oAuthHost, (_c !== null && _c !== void 0 ? _c : (process.env.stage === 'Prod' ? 'https://login.salesforce.com' : 'https://test.salesforce.com')));
        /* Create the nforce client. */
        this.connection = nforce.createConnection({
            oAuthHost,
            environment,
            clientId: this.userConfig.clientId,
            clientSecret: this.userConfig.clientSecret,
            redirectUri: this.userConfig.redirectUri || 'http://localhost:3000/oauth/_callback',
            apiVersion: this.userConfig.apiVersion || 'v51.0',
            mode: 'single',
            oauth: (_d = distributedOauth) === null || _d === void 0 ? void 0 : _d.oauth
        });
        /* Authenticate if the oauth token is not set. */
        if (!this.connection.oauth) {
            this.log(`authenticateClient`, `sending authenticate request to salesforce`);
            try {
                /* Returns oauth object, */
                const oauth = await this.connection.authenticate({
                    username: this.userConfig.username,
                    password: this.userConfig.password,
                });
                /* Note: authenticate _sometimes_ does not save this, hence doing manually outside of nforce library */
                this.connection.oauth = oauth;
                this.log(`authenticateClient`, `authenticate done`);
            }
            catch (error) {
                this.logError(`authenticateClient`, `authenticate failed: ${error}`);
                throw error;
            }
        }
        else {
            this.log(`authenticateClient`, `skipping authenticate request to salesforce`);
        }
        /* If the distributed salesforce oauth service requested an update, run the callback */
        const newOauth = this.connection.oauth;
        if (((_e = distributedOauth) === null || _e === void 0 ? void 0 : _e.updateOauthCache) && newOauth !== undefined) {
            this.log(`authenticateClient`, `updating distrbuted oauth`);
            (_f = distributedOauth) === null || _f === void 0 ? void 0 : _f.updateOauthCache(newOauth);
        }
        /* Create the axios client using acces_token from oauth. */
        this.axios = axios_1.default.create({
            headers: {
                'Authorization': `Bearer ${this.connection.oauth.access_token}`
            },
            baseURL: this.connection.oauth.instance_url
        });
    }
    /**
     * Confirm that the connection has been made already
     */
    async checkClientIsAuthenticated() {
        var _a;
        if (!((_a = this.connection) === null || _a === void 0 ? void 0 : _a.oauth)) {
            await this.authenticateClient();
        }
    }
    async getInstanceUrl() {
        await this.checkClientIsAuthenticated();
        return this.connection.oauth.instance_url;
    }
    /**
     * Gets the picklist labels and values for a partocular sObject field
     * @param {string} sObjectName Salesforce Object name
     * @param {string} fieldName Salesforce Object field name
     */
    async getPicklistValues(sObjectName, fieldName) {
        if (sObjectName && fieldName && this.userConfig) {
            const apiVersion = this.userConfig.apiVersion || 'v42.0';
            const recordTypeId = (this.userConfig.defaultRecordTypeIds || {})[sObjectName.toLowerCase()] || '012000000000000AAA';
            if (recordTypeId) {
                try {
                    const res = await this.connection.getUrl(`/services/data/${apiVersion}/ui-api/object-info/${sObjectName}/picklist-values/${recordTypeId}/${fieldName}`);
                    if (res && res.values && res.values.length) {
                        return res.values;
                    }
                }
                catch (error) {
                    this.logError('getPicklistValues', `${error}`);
                    return null;
                }
            }
        }
        return null;
    }
    /**
     * Executes a SOQL query against the Salesforce connection
     * @param {string} soqlQuery SOQL Query string
     * @param {Object} options { toJson: }
     */
    async query(soqlQuery, options) {
        var _a, _b, _c, _d;
        await this.checkClientIsAuthenticated();
        this.log('query', soqlQuery);
        let retries = RETRY_ATTEMPTS;
        let error;
        while (retries > 0) {
            retries -= 1;
            error = null;
            try {
                const result = await this.connection.query({
                    query: soqlQuery,
                    fetchAll: (_b = (_a = options) === null || _a === void 0 ? void 0 : _a.fetchAll, (_b !== null && _b !== void 0 ? _b : defaults_1.defaults.query.FETCH_ALL))
                });
                if (!result || !result.records) {
                    throw new Error('Error querying Salesforce');
                }
                if (!result.records.length) {
                    return [];
                }
                if (_d = (_c = options) === null || _c === void 0 ? void 0 : _c.toJson, (_d !== null && _d !== void 0 ? _d : defaults_1.defaults.query.TO_JSON)) {
                    return result.records.map(record => record.toJSON());
                }
                else {
                    return result.records;
                }
            }
            catch (err) {
                this.logError('query', `${err}`);
                error = Object.assign(err, new nforce_1.NforceError(`SalesforceQueryError`));
                await this.authenticateClient();
            }
        }
        throw error;
    }
    /**
     * Gets a single record from Salesforce given the object type, id and fields
     * @param {string} type Salesforce SObject name
     * @param {string} id Salesforce record id
     * @param {Array<string>} fields Fields to return
     */
    async getRecord(type, id, fields) {
        await this.checkClientIsAuthenticated();
        let retries = RETRY_ATTEMPTS;
        let error;
        while (retries > 0) {
            retries -= 1;
            try {
                return await this.connection.getRecord({ type, id, fields });
            }
            catch (err) {
                this.logError('getRecord', `${err}`);
                error = Object.assign(err, new nforce_1.NforceError(`SalesforceGet${type}RecordError`));
                await this.authenticateClient();
            }
        }
        throw error;
    }
    /**
     * Creates a Salesforce sobject
     *
     * POST /services/data/v44.0/sobjects/<sobjectType>/
     *
     * @param object
     * @param sobjectType
     * @returns {Promise.<string>} id or null if unsuccessful
     */
    async createRecord(object, sobjectType) {
        await this.checkClientIsAuthenticated();
        let retries = RETRY_ATTEMPTS;
        let error;
        while (retries > 0) {
            retries -= 1;
            error = null;
            try {
                const sobject = nforce.createSObject(sobjectType);
                Object.keys(object).forEach((key) => {
                    sobject.set(key, object[key]);
                });
                const result = await this.connection.insert({ sobject });
                return {
                    success: true,
                    status: salesforce_1.ResponseStatus.CREATED,
                    message: `Created ${sobjectType} record: ${result.id}`,
                    id: result.id,
                };
            }
            catch (err) {
                this.logError('createRecord', `${err}`);
                if (INVALID_DATA_ERROR_CODES.includes(err.errorCode)) {
                    return {
                        success: false,
                        status: salesforce_1.ResponseStatus.INVALID,
                        message: err.message,
                    };
                }
                else {
                    error = Object.assign(err, new nforce_1.NforceError(`SalesforceCreate${sobjectType}RecordError`));
                    await this.authenticateClient();
                }
            }
            this.log('createRecord', `retries left ${retries}`);
            sleep_1.sleep(1000);
        }
        throw error;
    }
    /**
     * Update a Salesforce sobject
     * @param {Object} sobject object to update
     */
    async updateRecord(object, sobjectType) {
        await this.checkClientIsAuthenticated();
        let retries = RETRY_ATTEMPTS;
        let error;
        while (retries > 0) {
            retries -= 1;
            error = null;
            try {
                const sobject = nforce.createSObject(sobjectType);
                Object.keys(object).forEach((key) => {
                    sobject.set(key, object[key]);
                });
                const record = await this.connection.update({ sobject });
                return record === '' ? 'Update successful' : 'Updated failed';
            }
            catch (err) {
                this.logError('updateRecord', `${err}`);
                error = Object.assign(err, new nforce_1.NforceError(`SalesforceUpdateRecordError`));
                await this.authenticateClient();
            }
        }
        throw error;
    }
    async deleteRecords(ids, options) {
        var _a, _b;
        await this.checkClientIsAuthenticated();
        const chunkSize = Math.max((_b = (_a = options) === null || _a === void 0 ? void 0 : _a.chunkSize, (_b !== null && _b !== void 0 ? _b : 5)), 200);
        const idChunks = lodash_1.default.chunk(ids, chunkSize);
        const allResults = [];
        for (const idsChunk of idChunks) {
            const uriParam = `ids=${idsChunk.join(',')}&allOrNone=false`;
            const uri = `/services/data/v46.0/composite/sobjects?${uriParam}`;
            const response = await this.axios.delete(uri);
            allResults.push(...response.data);
        }
        return allResults;
    }
    /**
     * Simple wrapper around composite rest API for inserting records.
     */
    async insertRecords(records, options) {
        var _a, _b, _c, _d;
        await this.checkClientIsAuthenticated();
        const maxParallel = (_b = (_a = options) === null || _a === void 0 ? void 0 : _a.maxParallel, (_b !== null && _b !== void 0 ? _b : 3));
        const chunkSize = Math.max((_d = (_c = options) === null || _c === void 0 ? void 0 : _c.chunkSize, (_d !== null && _d !== void 0 ? _d : 5)), 200);
        const recordChunks = lodash_1.default.chunk(records, chunkSize);
        const results = await promiseMapLimit(recordChunks, maxParallel, (records) => {
            return this.rest("POST", "/services/data/v46.0/composite/sobjects", {
                allOrNone: false,
                records
            });
        });
        return results.reduce((acc, res) => [...acc, ...res], []);
    }
    /**
     * Simple wrapper around composite rest API for inserting records.
     */
    async updateRecords(records, options) {
        var _a, _b, _c, _d;
        await this.checkClientIsAuthenticated();
        const maxParallel = (_b = (_a = options) === null || _a === void 0 ? void 0 : _a.maxParallel, (_b !== null && _b !== void 0 ? _b : 3));
        const chunkSize = Math.max((_d = (_c = options) === null || _c === void 0 ? void 0 : _c.chunkSize, (_d !== null && _d !== void 0 ? _d : 5)), 200);
        const recordChunks = lodash_1.default.chunk(records, chunkSize);
        const results = await promiseMapLimit(recordChunks, maxParallel, (records) => {
            return this.rest("PATCH", "/services/data/v46.0/composite/sobjects", {
                allOrNone: false,
                records
            });
        });
        return results.reduce((acc, res) => [...acc, ...res], []);
    }
    /**
     * Scans an object for any subquery objects and flattens them
     * @param {Object} object Object to scan
     * @param {Object} aliases Object that contains aliases as key value pairs
     */
    cleanSubqueries(object, aliases = {}) {
        const newObject = JSON.parse(JSON.stringify(object));
        const keys = Object.keys(object);
        if (keys && keys.length) {
            keys.forEach((key) => {
                const newKey = aliases[key] || key;
                if (object[key] && object[key].totalSize) {
                    newObject[newKey] = this.flattenSubquery(object[key]);
                }
                else {
                    newObject[newKey] = object[key];
                }
                if (newKey !== key) {
                    delete newObject[key];
                }
            });
        }
        return newObject;
    }
    /**
     * Flattens a subquery object into its field values.
     * Lower cases keys
     * @param {Object} object Subquery Object to flatten
     */
    flattenSubquery(object) {
        let keys;
        let lowerKeys;
        if (!object || !object.records || !object.records.length) {
            return [];
        }
        return object.records.map((record) => {
            const newRecord = {};
            if (!keys) {
                keys = Object.keys(record).filter((key) => key !== 'attributes');
                lowerKeys = keys.map((key) => key.toLowerCase());
            }
            if (keys && keys.length) {
                for (let i = 0; i < keys.length; i += 1) {
                    newRecord[lowerKeys[i]] = record[keys[i]];
                }
            }
            return newRecord;
        });
    }
    /**
     * Lower cases an object's keys and removes attributes field
     * @param {Object} object Subquery Object to flatten
     */
    lowerCaseObjectKeys(object) {
        if (!object) {
            return null;
        }
        const newObject = {};
        const keys = Object.keys(object).filter((key) => key !== 'attributes');
        const lowerKeys = keys.map((key) => key.toLowerCase());
        if (keys && keys.length) {
            for (let i = 0; i < keys.length; i += 1) {
                newObject[lowerKeys[i]] = object[keys[i]];
            }
        }
        return newObject;
    }
    /**
     * Typed wrapper around nforce.
     **/
    async apexRest(opts) {
        await this.checkClientIsAuthenticated();
        return this.connection.apexRest({
            oauth: this.connection.oauth,
            uri: opts.uri,
            method: opts.method,
            // urlParams: opts.urlParams,
            body: JSON.stringify(opts.body),
        });
    }
    /* Extending nforce private functions. YMMV : ) */
    async rest(method, url, data) {
        await this.checkClientIsAuthenticated();
        const opt = {
            oauth: this.connection.oauth,
            method,
            uri: this.connection.oauth.instance_url + url
        };
        if (data !== undefined) {
            opt["body"] = JSON.stringify(data);
        }
        return this.connection._apiRequest(opt);
    }
    async sObjectDescribe(sObject) {
        await this.checkClientIsAuthenticated();
        const data = await this.rest("GET", `/services/data/v51.0/sobjects/${sObject}/describe`);
        return data;
    }
    /**
     * Checks whether or not an input is sanitised to be an id
     * @param {String} input id string
     */
    isIdSanitised(input) {
        return input && input.search(/[^A-z0-9]/) === -1;
    }
    /**
     * Checks whether or not an input is sanitised to be an email
     * @param {*} input email string
     */
    isEmailSanitised(input) {
        return input && input.search(/[^A-z0-9.@!#$%&'*+\-/=?^_`{|}~]/) === -1;
    }
    /* Returns the ID of a recordType for a given sObject. */
    async getRecordTypeId(recordType, sObjectType) {
        await this.checkClientIsAuthenticated();
        const query = `SELECT Id FROM RecordType WHERE Name = '${recordType}' AND SobjectType = '${sObjectType}'`;
        const query_res = await this.query(query);
        const recordTypeId = query_res[0].get('id');
        return recordTypeId;
    }
}
exports.SalesforceClient = SalesforceClient;
//# sourceMappingURL=client.js.map