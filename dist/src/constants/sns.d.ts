import { TopicArns, SnsTopicNames } from "../models/sns";
export declare const SnsTopics: {
    [topic in SnsTopicNames]: TopicArns;
};
