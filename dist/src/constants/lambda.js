"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LambdaNamesMap = {
    postSlackMessage: "slack-utilities-dev-postSlackMessage",
    updateSlackMessage: "slack-utilities-dev-updateSlackMessage",
    FWCLeadScrapper: "fwc-lead-scrapper-dev-run",
    NZLeadSrapper: "nz-utils-dev-leadScraper",
    ProvisionWorkplaceDev: "provisioning-dev-provision-workplace",
    ProvisionWorkplaceProd: "provisioning-prod-provision-workplace"
};
exports.HttpStatusCodes = {
    "OK": 200,
    "MULTI_STATUS": 207,
    "BAD_REQUEST": 400,
    "SERVER_ERROR": 500,
};
exports.HttpStatusMessages = {
    "OK": "Success",
    "BAD_REQUEST": "Bad Request",
    "SERVER_ERROR": "Server Error",
    "MULTI_STATUS": "Multi-Status"
};
exports.unknownBadRequestSuffix = "02";
exports.badRequestSuffixMap = {
    "BadRequestError": "03",
    "InvalidPayload": "04",
};
//# sourceMappingURL=lambda.js.map