"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResponseStatus = {
    RECEIVED: 'received',
    CREATED: 'created',
    INVALID: 'invalid',
    ERROR: 'error',
};
//# sourceMappingURL=salesforce.js.map