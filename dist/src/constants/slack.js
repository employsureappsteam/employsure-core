"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChannelIDs = {
    Provisioning: "GCPUFAE0P",
    PureCloudSync: "CBYVBL4CX",
    EngAlertsDev: "GCH7GC5EC",
    EngAlertsProd: "CDNU1LH7Z",
    Engineering: "GCJ6Y1P3K"
};
exports.AppConfigPaths = {
    Provisioning: "/Slack/Provisioning",
    AWSNotificationService: "/Slack/AWSNotification",
    Development: "/Slack/Development",
};
//# sourceMappingURL=slack.js.map