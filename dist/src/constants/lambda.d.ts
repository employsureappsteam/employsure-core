import { LambdaNameMap, HttpStatusAll } from "../models";
export declare const LambdaNamesMap: LambdaNameMap;
export declare const HttpStatusCodes: {
    [type in HttpStatusAll]: number;
};
export declare const HttpStatusMessages: {
    [type in HttpStatusAll]: string;
};
export declare const unknownBadRequestSuffix = "02";
export declare const badRequestSuffixMap: {
    [x: string]: string;
};
