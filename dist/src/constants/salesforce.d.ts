export declare const ResponseStatus: {
    RECEIVED: string;
    CREATED: string;
    INVALID: string;
    ERROR: string;
};
