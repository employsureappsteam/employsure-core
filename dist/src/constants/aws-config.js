"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AWS_LAMBDA_API_VERSION = "2015-03-31";
exports.AWS_STEP_FUNCTIONS_API_VERSION = "2016-11-23";
exports.AWS_SSM_API_VERSION = "2014-11-06";
exports.AWS_REGION_DEFAULT = "ap-southeast-2";
exports.AWS_REGION_SES = "us-west-2";
//# sourceMappingURL=aws-config.js.map