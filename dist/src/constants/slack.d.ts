import { ChannelIDMap } from "../models/slack";
export declare const ChannelIDs: ChannelIDMap;
export declare const AppConfigPaths: {
    Provisioning: string;
    AWSNotificationService: string;
    Development: string;
};
