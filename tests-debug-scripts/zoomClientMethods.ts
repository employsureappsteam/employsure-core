import { ZoomClient } from "../src/clients/zoom";
import { GetSecretParams } from "../src/lib";
import moment from 'moment';

async function loadClients() {
    const apiKeySsmPath = "/ZoomProvisioning/Prod/ApiKey"
    const apiSecretSsmPath = "/ZoomProvisioning/Prod/ApiSecret"
    const secrets = await GetSecretParams([
        apiKeySsmPath,
        apiSecretSsmPath
    ]);
    const zoomClient = new ZoomClient({
        apiKey: secrets[apiKeySsmPath],
        apiSecret: secrets[apiSecretSsmPath],
    });

    return { zoomClient };
}

(async () => {
    console.log()
    const date_from = moment().subtract(5, 'days').format("YYYY-MM-DD");
    debugger;
    const { zoomClient } = await loadClients();
    const u = await zoomClient.getUsers();

    const d = new Date();

    for (const user of u.splice(0, 5)) {
        const userMeetings = await zoomClient.getUserMeetings(user.id, { from: date_from });
        console.log(`${user.email}: ${userMeetings.length}`);
    }
})();