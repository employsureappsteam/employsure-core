process.env.EMPLOYSURE_VERBOSE_LOGGING = 'true';
process.env.EMPLOYSURE_VERBOSE_LOGGING_LEVEL = 'info';
process.env.stage = "Dev";

import { loadSalesforceClient } from '../src';
import { range } from 'lodash';
import { IClientOptions } from '../src/helpers/clients/models';

(async () => {
    try {
        console.log('starting');
        const client = await loadSalesforceClient('aus', 'Full');

        const r = await client.insertRecords([{
            attributes: {
                type: "Lead"
            },
            FirstName: 'test',
            LastName: 'test',
            Company: 'test',
        }]);

        debugger;

        const r2 = await client.deleteRecords([r[0].id]);

        debugger;
    } catch (error) {
        console.error(error);
        debugger;
    }

    debugger;
})();


