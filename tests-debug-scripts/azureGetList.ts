import { AzureClient, GetSecretParams } from '../src';
import { BigQueryClient } from '../src/clients';
import * as g from 'generate-schema';

process.env.stage = "Dev";

(async () => {

    const clientEmail = `/BigQuery/${process.env.stage}/ClientEmail`;
    const privateKey = `/BigQuery/${process.env.stage}/PrivateKeyWithoutNewline`;

    const secrets = await GetSecretParams([
        '/AzureEmployeeProvisioning/Dev/ClientId',
        '/AzureEmployeeProvisioning/Dev/ClientSecret',
        '/AzureEmployeeProvisioning/Dev/TenantId',
        clientEmail,
        privateKey
    ]);

    const azureClient = new AzureClient({
        clientSecret: secrets['/AzureEmployeeProvisioning/Dev/ClientSecret'],
        clientId: secrets['/AzureEmployeeProvisioning/Dev/ClientId'],
        tenantId: secrets['/AzureEmployeeProvisioning/Dev/TenantId']
    });

    await azureClient.init();

    const bq = new BigQueryClient('lead-scoring-model', {
        client_email: secrets[clientEmail],
        private_key: secrets[privateKey]
    });

    const a = await azureClient.getUsers();


    debugger;
    /* Fetch the max createddatetime from the synced records. */
    const queryRows = await bq.query<{ max_time: string }>(
        'select FORMAT_TIMESTAMP("%Y-%m-%dT%H:%M:%E*SZ", max(createddatetime)) as max_time from `lead-scoring-model.azure_prod.sign_ins_export_*`'
    );
    const maxTime = queryRows[0].max_time;

    const logins = await azureClient.getList(
        `auditLogs/signIns?$filter=createdDateTime ge ${maxTime}`,
        { maxRequests: 1 }
    );

    debugger;
})();


