import { loadClients, loadSalesforceClient } from "../src";
import { RollbarClient } from "../src/clients/rollbar";

(async () => {
    const sf = await loadSalesforceClient("aus", "Full");
    const t = await sf.updateRecords([{
        attributes: { type: "Lead" }
        , Id: '00Q1s000002kPfbEAE'
        , FirstName: "John 2"
        , "LastName": "Test"
        , "Company": "Test"
    }]);

    debugger;
})();