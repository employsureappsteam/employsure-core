import { sendToSns, supressNotifications } from "../src/lib/sns/sendToSns";
import { SnsTopics } from "../src/constants";

/* Only test in dev environment. */
process.env.stage = "Dev"; // Or "Prod"

const response = sendToSns(
    SnsTopics.EmploysureApiError,
    "Test Notification",
    [
        { title: "Test", message: "Test" }
    ])

response.then(data => {
    console.log(data)
}).catch(err => {
    console.log(err)
})